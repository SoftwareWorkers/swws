#+LANGUAGE: en
#+TITLE: Community
#+DESCRIPTION: communities in which we are involved
#+KEYWORDS: softwareworkers meup!
#+AUTHOR: Giovanni Biscuolo
#+EMAIL: g@xelera.eu
#+META_TYPE: website

* Communities we manage

- mailing list: bla bla bla

- IRC:  **sometimes we work** so don't expect to find us always online, on IRC we are here...

* Communities we participare in

- Guix: ...

- ILS: ...

