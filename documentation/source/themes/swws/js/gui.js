/**
 * This script extends the functionality of some GUI components like
 * the main menu and the table of contents so that they are easier to
 * access in small screens.
 */

// Make sure the document content has loaded before manipulating it.
if (document.readyState === "loading") {
    document.addEventListener("DOMContentLoaded", onDomContentLoaded);
} else {
    onDomContentLoaded();
}


// STATE

let isModalMenuOpen = false;
let isWideScreen = false;


// EVENT HANDLERS

/**
 * Install scripted functionality and helper GUI components.
 */
function onDomContentLoaded() {
    // Add new components to the DOM and extend the functionality of
    // existing ones.
    makeTocModal();
    makeMenuModal();
    // Initialize visibility of GUI components.
    const cssQueryString = "screen and (min-width: 1500px)";
    const mediaQueryList = window.matchMedia(cssQueryString);
    onScreenWidthChanged(mediaQueryList);
    mediaQueryList.addListener(onScreenWidthChanged);
    // Make IBANs human-readable.
    const ibanList = document.querySelectorAll(".IBAN");
    ibanList.forEach(structureIBAN);
}

/**
 * Show/hide GUI components depending on screen width.
 *
 * @param {MediaQueryList} screenWidened
 *   An object to match changes on screen width.
 */
function onScreenWidthChanged(screenWidened) {
    if (screenWidened.matches) {
        isWideScreen = true;
	hideModalMenu();
	if (tocExists()) {
            showTableOfContents();
	}
    } else {
        isWideScreen = false;
	if (tocExists()) {
            hideTableOfContents();
	}
    }
}

/**
 * Hide the table of contents when you click on any of its links
 * (only on narrow screens).
 */
function onTocLinkClicked() {
    if (!isWideScreen) {
        hideTableOfContents();
    }
}


// HELPER FUNCTIONS

/**
 * Script the main menu so that the user can display/hide it on demand
 * when using a narrow screen.
 */
function makeMenuModal() {
    insertMenuButton();
    insertCloseMenuButton();
}

/**
 * If the document has a Table of Contents, script it so that the user
 * can display/hide it on demand when using a narrow screen.
 */
function makeTocModal() {
    if (tocExists()) {
	insertTocButton();
	insertCloseTocButton();
	connectTocLinksToClickHandler();
    }
}

function connectTocLinksToClickHandler() {
    const tocLinks = document.querySelectorAll("#text-table-of-contents a");
    for (const link of tocLinks) {
        link.addEventListener("click", onTocLinkClicked);
    }
}

function insertMenuButton() {
    // Define button and its container.
    const menuButton = newButton(
        "Show main menu",
        "menu-button",
        showModalMenu
    );
    const menuButtonBox = document.createElement("div");
    const classAttribute = document.createAttribute("class");
    classAttribute.value = "flex-initial";
    menuButtonBox.setAttributeNode(classAttribute);
    // Insert button.
    menuButtonBox.append(menuButton);
    const preamble = document.querySelector("#preamble");
    preamble.append(menuButtonBox);
}

function insertCloseMenuButton() {
    // Define button.
    const closeButton = newButton(
        "Close main menu",
        "menu-close-button",
        hideModalMenu
    );
    // Insert button.
    const menuTitle = document.querySelector("#menu-title");
    menuTitle.append(closeButton);
}

function insertTocButton() {
    // Define button.
    const tocButton = newButton(
        "Show table of contents",
        "toc-button",
        showTableOfContents
    );
    // Insert button.
    const content = document.querySelector("#content");
    content.prepend(tocButton);
}

function insertCloseTocButton() {
    // Define button.
    const closeButton = newButton(
        "Close table of contents",
        "toc-close-button",
        hideTableOfContents
    );
    // Insert button.
    const toc = document.querySelector("#table-of-contents");
    toc.prepend(closeButton);
}

function showModalMenu() {
    if (!isModalMenuOpen) {
	const menu = document.querySelector(".menu");
	const menuTitle = document.querySelector("#menu-title");
	menu.classList.remove("menu");
	menu.classList.add("menu-modal");
	menuTitle.classList.remove("a11y-offset");
	isModalMenuOpen = true;
    }
}

function hideModalMenu() {
    if (isModalMenuOpen) {
	const menu = document.querySelector(".menu-modal");
	const menuTitle = document.querySelector("#menu-title");
	menu.classList.remove("menu-modal");
	menu.classList.add("menu");
	menuTitle.classList.add("a11y-offset");
	isModalMenuOpen = false;
    }
}

function showTableOfContents() {
    const toc = document.querySelector("#table-of-contents");
    toc.style.setProperty("display", "block");
    toc.classList.add("toc-modal");
}

function hideTableOfContents() {
    const toc = document.querySelector("#table-of-contents");
    toc.style.setProperty("display", "none");
    toc.classList.remove("toc-modal");
}

/**
 * Return true if the document has a table of contents, false otherwise.
 */
function tocExists() {
    const toc = document.querySelector("#table-of-contents");
    return (toc !== null);
}

/**
 * Return a new button element with the given attributes.
 *
 * @param {string} text
 *   Text to use as the label of the button. The text is also used as
 *   the title of the button (visible when hovering over it).
 *
 * @param {string} id
 *   The value to use for the button's id attribute.
 *
 * @param {function} clickHandler
 *   An event listener to handle the click event of the button.
 *
 * @returns {Element}
 *   An HTML "button" element.
 */
function newButton(text, id, clickHandler) {
    const button = document.createElement("button");
    const label = document.createElement("span");
    const labelText = document.createTextNode(text);
    const classAttribute = document.createAttribute("class");
    const idAttribute = document.createAttribute("id");
    const titleAttribute = document.createAttribute("title");
    classAttribute.value = "a11y-offset";
    idAttribute.value = id;
    titleAttribute.value = text;
    label.setAttributeNode(classAttribute);
    label.appendChild(labelText);
    button.setAttributeNode(idAttribute);
    button.setAttributeNode(titleAttribute);
    button.addEventListener("click", clickHandler);
    button.append(label);

    return button;
}

/**
 * Structure IBANs (International Bank Account Numbers) in groups of
 * four characters per span so that the ".IBAN > span" CSS rule set
 * takes effect and makes them easier to read.
 *
 * @param {Node} iban
 *   An HTML node representing a span element of class ".IBAN".
 */
// TODO: Structure IBANs in Org documents instead and remove this function.
// Maybe there is a way to extend Org's markup to structure IBANs in
// the source document itself in a way that allows them to be exported
// with the appropriate HTML structure (groups wrapped in spans). Also,
// add IBAN validation in the document editing stage.
function structureIBAN(iban) {
    const text = iban.innerText;
    const groupRegex = new RegExp(/.{1,4}/, "g");
    const humanizedText = text.replace(groupRegex, "<span>$&</span>");
    iban.innerHTML = humanizedText;
}
