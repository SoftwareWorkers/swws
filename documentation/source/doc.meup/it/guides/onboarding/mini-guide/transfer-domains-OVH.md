# Trasferimento di domini da OVH

Informazioni aggiornate al 18 Marzo 2020.

## Accedere al dominio

1. accedere alla pagina di [configurazione servizi Web](https://www.ovh.com/manager/web/index.html#/configuration)

1. assicurarsi di utilizzare la lingua italiana per l'interfaccia, indicata con "IT" alla sinistra dell'opzione "Le tue notifiche" nel menù principale dell'interfaccia web

1. dal menù di sinistra "Domini" selezionare il dominio che intendete trasferire

1. nella scheda "Informazioni generali" c'è il riquadro "Sicurezza" con le informazioni necessarie.

## Sblocco del trasferimento

1. disattivare l'opzione chiamata "Richiedi Auth info" (è un errore di traduzione: in inglese è "Protection against domain name transfer" che significa "Protezione contro il trasferimento del dominio" sottointeso trasferimento abusivo).  
La protezione - attiva per impostazione predefinita - consiste nella disabilitazione del pulsante di richiesta del codice di trasferimento.

1. nel caso fosse stato necessario disattivarla, l'operazione richiede qualche minuto, durante il quale verrà visualizzata l'informazione "Disattivazione in corso".  
Nel frattempo ci si può portare avanti passando alla configurazione DNS

## Codice di trasferimento

1. di fianco a "Richiedi Auth info" è presente il link "AUTH/INFO", seguendo quel link viene visualizzata una finestra che riporta il "Codice AUTH/INFO"

1. prendere nota, possibilmente con un copia e incolla su un documento di testo o direttamente nella email, del codice fornito

## Attuale configurazione DNS

1. entrare nella scheda "Zona DNS" e selezionare la voce "Utilizza editor di testo", compare una finestra intitolata "Utilizza l'editor di testo"

1. copiare tutto il contenuto del riquadro "Inserisci il testo delle zone DNS..." (fare un click dentro il riquadro, CTRL+A e infine CTRL+C)

1. incollare il contenuto appena copiato in un file di testo o direttamente nell'email da inviare a noi
