---
title: Il Single sign-on su CloudMeUp
description: Introduzione al Single sign-on
langs: it-it
robots: noindex, nofollow
tags: SSO, LemonLDAP::NG, onboarding
---

###### tags: `SSO` `LemonLDAP::NG` `onboarding`


# Fare merge con il file "Account SSO..."


# Il Single sign-on

Questa è un'introduzione al *Single sign-on*, il sistema di autenticazione adottato per accedere alla piattaforma online CloudMeUp.


## 1. Le applicazioni utilizzate

Nell'ambito del PCTO Narratron verranno utilizzati diverse applicazioni online; le prime ad essere attivate saranno:
* **Nextcloud** per condividere file, calendario, contatti, link ai siti rilevanti
* **CodiMD** per la scrittura collaborativa in markdown
* **Discourse** per il forum di discussione

Nonostante queste applicazioni siano utilizzabili anche in forma anonima, per i nostri scopi è necessario che gli utenti accedano con un account individuale, che quindi presuppone un nome utente e la relativa password. Per evitare il disagio di dover creare un account per ogni applicazione abbiamo attivato il [Single sign-on](https://it.wikipedia.org/wiki/Single_sign-on).

## 2. L'account

L'account è lo strumento con quale vengono gestiti due diversi aspetti riguardanti gli utenti: l'autenticazione e l'autorizzazione.

### Autenticazione

L'**autenticazione** consiste nel verificare che l'utente sia ++veramente++ chi dice di essere.
Tale verifica viene fatta attraverso:
* qualcosa che l'utente **conosce** (per esempio la password)
* qualcosa che l'utente **possiede** (per esempio un codice [OTP - One-time password](https://it.wikipedia.org/wiki/One-time_password))
* qualcosa che l'utente **è**, nel senso di caratteristiche biometriche (per esempio le impronte digitali)

L'autenticazione si definisce "forte" quando è basata sulla combinazione di più fattori, possibilmente di tipo diverso.


### Autorizzazione

L'**autorizzazione** consiste nell'attribuire a un utente determinati diritti su programmi e dati.
Tali diritti possono ad esempio riguardare l'accesso a un servizio, l'esecuzione di un programma, la lettura, modifica, cancellazione o creazione di file e cartelle.
Il tipico processo di autorizzazione coinvolge, nella sua versione più semplice, un utente in possesso di un account (user) che chiede determinati diritti a un amministratore di sistema (admin). Ricevuta la richiesta, l'admin assegna all'account i diritti necessari. Nei casi reali spesso il processo di autorizzazione include uno o più soggetti che confermano la legittimità della richiesta fatta dall'utente: per esempio se un utente acquista un servizio a pagamento (film in streaming) l'autorizzazione è subordinata alla conferma del pagamento da parte del gestore delle carte di credito.
:::info
Per la piattaforma Narratron gli user sono i venti studenti della 3C, l'admin è il sistemista della SOFTWARE WORKERS, e i due tutor scolastico e aziendale sono i soggetti che confermano all'admin la legittimità delle richieste.
:::


## 3. Come è realizzato il SSO per il PCTO Narratron

### Il nome di dominio

Per accedere alla piattaforma Narratron viene utilizzato il SSO che SOFTWARE WORKERS ha attivato per *tutti* i servizi online gestiti dall'azienda: 
* quelli a uso interno (per esempio https://pad.softwareworkers.it/), 
* quelli per i clienti (per esempio https://mail.meup.it/) 
* e quelli dei progetti speciali, come per esempio NARRATRON.

In questo modo a ogni utente che si autentica sul servizio di SSO verrà presentato un pannello con i link ai servizi abilitati per il suo account.

Poiché viene usato lo stesso sistema di SSO anche il nome del dominio è uguale per tutti, ed è stato scelto il dominio auth.meup.io


### Il software usato per il SSO

Dopo aver valutato alcune alternative (tra cui Keycloak e Shibboleth) è stato scelto [LemonLDAP::NG](https://lemonldap-ng.org/), la cui schermata di accesso compare quindi appena si accede alla pagina di autenticazione.