---
title: Browser
description: Appunti e considerazioni sui browser
langs: it-it
robots: noindex, nofollow
tags: browser, Chrome, Chromium, ungoogled-chromium

---

# Browser

:::info
Questa è una bozza: il pad è nato per pubblicare il riferimento a **ungoogled-chromium** che serviva in uno specifico contesto, ma sarà progressivamente espanso per catturare gli spunti sui vari browser .
:::

## Chromium

[Chromium](https://www.chromium.org/Home) è il browser con licenza libera dal cui codice sorgente sono stati derivati vari browser tra cui Google Chrome.
Per la storia del progetto, le caratteristiche e le differenze con in browser derivato da Chromium si rimanda all'articolo di Wikipedia: https://it.wikipedia.org/wiki/Chromium.

## Chrome

Chrome è il browser di Google derivato da Chromium. Ha una licenza proprietaria che consente l'uso gratuito. Rispetto a Chromium le aggiunte riguardano l'integrazione con i servizi Google e funzioni di tracciamento degli utenti e della loro attività online[^1] (in aggiunta a quelle già presenti in Chromium).

[^1]: https://en.wikipedia.org/wiki/Google_Chrome#User_tracking_concerns

## ungoogled-chromium

Il browser ungoogled-chromium è una versione di Chromium dalla quale sono state eliminate le funzioni di tracciamento dell'attività degli utenti.

Sul sito ufficiale [ungoogled-chromium](https://ungoogled-software.github.io/) si trovano informazioni dettagliate sul progetto e la pagina dei download.


## Browser raccomandati per le videoconferenze

### Videoconferenze con Jitsi

Questo [thread](https://community.jitsi.org/t/jitsi-meet-google-chrome-vs-firefox/17093) sul forum di supporto di Jitsi chiarisce i motivi per cui - almeno alla data dei vari post - i browser della *famiglia Chromium* danno migliori risultati sia come prestazioni sia come interfaccia utente.
