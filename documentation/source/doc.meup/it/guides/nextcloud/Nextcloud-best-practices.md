# Nextcloud best practices

********* BOZZA *********

## Contatti
La rubrica dei contatti di Nextcloud può essere sincronizzata con gli applicativi locali su desktop (Thunderbird) o smartphone (Contatti di Android).

Fino alla versione 16 Nextcloud ha problemi nel gestire correttamente il formato **vCard 4.0** (come documentato in questa [issue]( https://github.com/nextcloud/server/issues/12878#issuecomment-552981236)). 

Per tale motivo in Thunderbird [CardBook](https://gitlab.com/CardBook/CardBook) si consiglia di usare la **vCard 3.0**.
