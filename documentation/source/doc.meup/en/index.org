#+LANGUAGE: en
#+TITLE: Doc.MeUp!
#+DESCRIPTION: MeUp! documentation system.
#+KEYWORDS: documentation meup meup! doc.meup softwareworkers
#+AUTHOR: Giovanni Biscuolo
#+EMAIL: g@xelera.eu
#+DATE: [2021-03-23 Tue]
#+META_TYPE: website

* About

  Wellcome the *MeUp!* project documentation system.
  
* *MeUp!* user guides

  #+INCLUDE: "guides/sitemap.org" :lines "2-"

