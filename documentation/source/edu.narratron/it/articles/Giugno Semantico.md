---
title: Giugno Semantico
description: elevator pitch per Editoria scolastica
langs: it-it
robots: noindex, nofollow
tags: Narratron, PCTO, AIE, Formaper, Novello
type: slide

---

# Riorganizzare il documento

Deve diventare un articolo autonomo da cui **derivare** la presentazione.



# GIUGNO SEMANTICO
edizione 2021

---

## Cos'è
* uno stage di alternanza scuola-lavoro (PCTO)
* marcatura semantica **del testo** delle opere letterarie

----

## Quando e dove
* online
* 14 - 28 giugno 2021

----

## Con chi

* SOFTWARE WORKERS - partner tecnico
* Liceo Novello (Codogno) - supporto online
* Licei classici italiani (600+) - studenti in stage
* Case editrici scolastiche - partner per i contenuti


---

## Cosa otterremo

* La marcatura semantica di:
    * **Decameron** di Giovanni Boccaccio
    * **un testo** per ciascun Editore partecipante

---

## Vantaggi per gli Editori partner
1 di 2

* Marcatura semantica di una loro opera
* Relazione con i docenti di italiano
* R&D in modalità **Open Innovation**

----

## Vantaggi per gli Editori partner
2 di 2

* Possibilità di acquisire la tecnologia (tutta la piattaforma Narratron è open source)
* Visibilità associata ad alcuni temi chiave:
    * innovazione
    * crowdsourcing
    * didattica digitale integrata
    * open source

----

## Diventare partner: cosa occorre

* un testo su cui far lavorare gli studenti
* promuovere il **giugno semantico**  
(reclutare 5 docenti o 10 studenti)
* sponsorizzare l'iniziativa
(vedere piano di sponsorizzazioni)

---

## Il Giugno Semantico dopo giugno 2021
1 di 2

* Copertura a tappeto delle scuole italiane
    * altri licei
    * tecnici e professionali
* Estensione ad altre letterature
    * greca e latina
    * lingue straniere
    * lingue minori (per esempio Ladino)

----

## Il Giugno Semantico dopo giugno 2021
2 di 2

* Internazionalizzazione
    * Inglese, Tedesco (scuole in Alto Adige)
* Communities (tecnica e semantica globali + linguistiche)
* Spin-off commerciali e sviluppo ecosistema di *semantic services*

---

## Grazie!

andrea.rossi@softwareworkers.it

---

