#+LANGUAGE: it
#+TITLE: Sviluppatore CSS semantico per il nostro CMS
#+DESCRIPTION: Sviluppatpore CSS per implementare la nostra nuova visual identity secondo i nostri principi di design.
#+KEYWORDS: SoftwareWorkers jobs
#+AUTHOR: Andrea Rossi
#+EMAIL: andrea.rossi@softwareworkers.it

#+NAME: abstract
#+BEGIN_ABSTRACT
Sviluppatpore CSS per implementare la nostra nuova visual identity secondo [[https://doc.meup.io/colophon/#cms-design-principles][i nostri principi di design (in Inglese)]].
#+END_ABSTRACT

Stiamo cercando uno sviluppatore CSS che implementi la nostra nuova identità visuale, da applicare ai nostri siti [fn:1].  L'incarico sarà regolato dal nostro [[https://gitlab.com/softwareworkers/swws/-/blob/develop/documentation/source/doc.swws/en/legal/sprint-agreement/index.org][Open Contract for Agile development (in Inglese)]] [fn:2].

Coloro che desiderano sottoporre la propria candidatura sono invitati a verificare la propria volontà di aderire ai [[https://doc.meup.io/colophon/#cms-design-principles][principi di design del nostro CMS (in Inglese)]].

La cnooscenza del sistema di gestione distribuita delle versioni Git, del literate programming e di Emacs org-mode sono valutate come un plus.

Se interessati, per favore scrivete a jobs@softwareworkers.it.

* Footnotes
[fn:2] [[https://gitlab.com/softwareworkers/swws/-/raw/develop/documentation/source/doc.swws/en/legal/sprint-agreement/index.org][versione org-mode del Open Contract for Agile development]]

[fn:1] The versions under development are:
- https://beta.softwareworkers.it/ (institutional website)
- https://beta.meup.io/ (operational portal)
- https://doc.meup.io/ (documentation)
