#+LANGUAGE: it
#+TITLE: Una nuvola tutta mia
#+SUBTITLE: FIXME
#+DESCRIPTION: Pagina sul cloud privato riproducibile
#+KEYWORDS: SoftwareWorkers cloud privato riproducibilità
#+AUTHOR: Andrea Rossi
#+EMAIL: andrea.rossi@softwareworkers.it

#+NAME: abstract
#+BEGIN_ABSTRACT
Il cloud privato aggiunge ai vantaggi del cloud (accedere ai propri dati e applicazioni da qualunque posto tramite qualsiasi dispositivo) la garanzia del controllo sull'infrastruttura. La complessità della gestione è a carico di Swws; l'indipendenza dal fornitore è assicurata dalla totale riproducibilità.
#+END_ABSTRACT

* Una nuvola tutta mia

Nel cloud privato l’infrastruttura (a partire dai server fisici) è ad uso esclusivo dell’utilizzatore, la puoi gestire in totale autonomia. 
La situazione più comune è rappresentata da uno o più server presi a noleggio presso il datacenter di un provider.

Il nostro aiuto cambia a seconda di quello che ti serve:

- minimo: possiamo essere semplicemente dei consulenti che chiami ogni tanto, 
- completo: possiamo gestire noi direttamente la tua infrastruttura per tuo conto,
- intermedio: possiamo aiutarti in tutti i gradi intermedi tra questi due estremi.


La riproducibilità che contraddistingue la nostra offerta consente di installare nel cloud privato qualunque applicazione presente nel nostro catalogo Software as a Service (SaaS):

- email
- videoconferenza
- file sharing
- calendario, agenda e gestione attività (personali e di gruppo)
- rubrica contatti (privata e condivisa)
- gestione ticket di assistenza tecnica (help desk)
- CRM
- forum
- mailing list
- invio newsletter
- project management
- learning management system

#+begin_comment
Stessi link alla pagina del servizio sul sito Meup – B2C come per la pagina sull’hosting applicativo
#+end_comment

#+begin_aside
I nostri sistemi interni sono /riproducibili/.

Significa che i nostri clienti possono liberamente e gratuitamente ricreare presso il loro cloud privato l’intera infrastruttura di Software Workers.

Puoi farlo *anche in totale autonomia e senza bisogno di dirci nulla*: basta scaricare il software e la documentazione dal nostro repository.
#+end_aside
