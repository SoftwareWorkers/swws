#
# proj002 tasks
#
macro organization  [company01]

macro tasks_id	    [proj002]
macro tasks_title   [Good Software for Customer 2]
macro tasks_start   [2021-07-05-0:00-+0200]
macro task_maxend   [2021-10-15-0:00-+0200]

task ${tasks_id} "${tasks_title}" {
	note "Normal software, as usual."
	chargeset dev
	charge 50 perday
	responsible gm
	
	task spec "Specification" {
		note "Write the software specification, including (human) tests"
		effort 14d
		allocate dev04, dev06
		depends !mstones.start
	}

	task software "Software Development" {
		note "Develop the software according to specification, including unit testing"
		priority 1000
		depends !spec
		responsible dev01
		
		task database "Database coupling" {
			note "Create database schema and use it in software (unit testing)"
			effort 7d
			allocate dev03, dev04
		}

		task backend "Back-End Functions" {
			note "Develop back-end fuctions and expose them via APIs (unit testing)"
			effort 20d
			depends !database
			allocate dev01, dev02
		}

		task gui "Graphical User Interface" {
			note "Develop the GUI using the back-end APIs (unit testing)"
			effort 15d
			depends !database, !backend
			allocate dev02, dev03
			# Resource dev02 should only work 4 hours per day on this task.
			limits {
				dailymax 4h {
					resources dev02
				}
			}
		}
	}

	task test "Software testing" {
		note "Software tests according to tests specifications"

		task alpha "Alpha Test" {
			note "Hopefully most bugs will be found and fixed here."
			effort 1w
			depends !!software
			allocate dev01, dev03
		}

		task beta "Beta Test" {
			effort 1w
			depends !alpha
			allocate dev04, dev05
		}
	}

	task manual "Manual" {
		note "Write user manual according to specifications and software development"
		effort 6w
		depends !mstones.start
		allocate dev05, dev01
		purge chargeset
		chargeset doc
	}

	#
	# Milestones

	task mstones "Milestones" {

		# We credit the charges to the 'rev' account.
		purge chargeset
		chargeset rev

		flags mstone

		task start "Project start" {
			start ${tasks_start}
			charge 2000 onstart
		}

		task prev "Technology Preview" {
			note "All '''major''' features should be usable via APIs."
			depends !!software.backend
			charge 3000 onstart
		}

		task beta "Beta version" {
			note "Fully functional GUI, may contain bugs."
			depends !!test.alpha
			charge 5000 onstart
		}

		task done "Ship Product to Customer" {
			note "All priority 1 and 2 bugs must be fixed."
			maxend 2021-09-30
			depends !!test.beta, !!manual
			charge 8000 onstart
		}
	}
}
