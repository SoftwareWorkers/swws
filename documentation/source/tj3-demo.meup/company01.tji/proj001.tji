#
# proj001 tasks
#
macro organization  [company01]

macro tasks_id	    [proj001]
macro tasks_title   [Great Software for Customer 1]
macro tasks_start   [2021-06-07-0:00-+0200]
macro task_maxend   [2021-09-15-0:00-+0200]

task ${tasks_id} "${tasks_title}" {
	note "An interesting software, pratically vaporware."
	chargeset dev
	charge 50 perday
	responsible gm
	
	task spec "Specification" {
		note "Write the software specification, including (human) tests"
		effort 14d
		allocate dev01, dev03
		depends !mstones.start
	}

	task software "Software Development" {
		note "Develop the software according to specification, including unit testing"
		priority 1000
		depends !spec
		responsible dev01
		
		task database "Database coupling" {
			note "Create database schema and use it in software (unit testing)"
			effort 7d
			allocate dev01, dev02
		}

		task backend "Back-End Functions" {
			note "Develop back-end fuctions and expose them via APIs (unit testing)"
			effort 20d
			depends !database
			allocate dev01, dev02
		}

		task gui "Graphical User Interface" {
			note "Develop the GUI using the back-end APIs (unit testing)"
			effort 15d
			depends !database, !backend
			allocate dev02, dev03
			# Resource dev02 should only work 6 hours per day on this task.
			limits {
				dailymax 6h {
					resources dev02
				}
			}
		}
	}

	task test "Software testing" {
		note "Software tests according to tests specifications"

		task alpha "Alpha Test" {
			note "Hopefully most bugs will be found and fixed here."
			effort 1w
			depends !!software
			allocate dev04, dev02
		}

		task beta "Beta Test" {
			effort 1w
			depends !alpha
			allocate dev04, dev01
		}
	}

	task manual "Manual" {
		note "Write user manual according to specifications and software development"
		effort 6w
		depends !mstones.start
		allocate dev05, dev03
		purge chargeset
		chargeset doc
	}

	#
	# Milestones

	task mstones "Milestones" {

		# We credit the charges to the 'rev' account.
		purge chargeset
		chargeset rev

		flags mstone

		task start "Project start" {
			start ${tasks_start}
			charge 5000 onstart
		}

		task prev "Technology Preview" {
			note "All '''major''' features should be usable via APIs."
			depends !!software.backend
			charge 5000 onstart
		}

		task beta "Beta version" {
			note "Fully functional GUI, may contain bugs."
			depends !!test.alpha
			charge 10000 onstart
		}

		task done "Ship Product to Customer" {
			note "All priority 1 and 2 bugs must be fixed."
			maxend 2021-09-30
			depends !!test.beta, !!manual
			charge 10000 onstart
		}
	}
}
