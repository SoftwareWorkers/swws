* TaskJuggler Workflow
** User workflows

   Interesting workflows and general tricks from the [[https://groups.google.com/g/taskjuggler-users][users mailing list]].

*** [[https://groups.google.com/g/taskjuggler-users/c/kaMKfFESMAw/m/4vYenG0rBAAJ][Uros Platise timesheet workflow]].

   #+begin_verse
For my project management I created a python script that fetches from svn (irreversible proof of work) excel sheets from all collaborators and imports their "progress report" lines as bookings inserted into the project files themselves, via a unique reference which is in our case a ticketing system (open source trac, but any ticketing system could be used as gitlab, jira, ...); before I used aux files to supplement this information; That's how I control actual plan which is then also the actual P&L used to do progress reports, critical path analysis, invoicing, ..

why I decided so:

- long term project plans often needs to be re-factored, even though I am using three level projection mode, I need to refactor the structure,
- in this case all the bookings when I am moving "text" around the files is attached to the task and no information is lost;
- aux files depend on task reference which would during refactoring become lost
   #+end_verse

*** [[https://groups.google.com/g/taskjuggler-users/c/AnZc3bAr0rw/m/sGv9Xwy1AwAJ][Jakob Schöttl on projects (not tj3) i18n]].

   #+begin_verse
With a custom setup, it could look like this:

Makefile
tasks.csv  (three columns: task_id, language1, language2)
project.tjp

and make generates
project.en.tjp
project.de.tjp

project.tjp needs some kind of placeholders for the task description which are replaced by the corresponding translation from the csv file. This can be done by sed, or you use a template engine like m4. sed is easier.
   #+end_verse

*** [[https://groups.google.com/g/taskjuggler-users/c/PT4rXjsPoDw/m/nAe_x8ktAAAJ][Jakob Schöttl on multiple output types (e.g Graphviz) with org-mode and ox-taskjuggler]].

    #+begin_verse
In conclusion: The org file my single source file, from which tj3 source and reports, PDF documents and graphviz dependency graphs are generated.

I wrote the program org2dot in Haskell, its here:
https://gist.github.com/schoettl/ca1f729d5472dcff43e2139def941b40

You would have to change it slightly because in tj3, the property names "blocks" and "depends" have different names. You may also have to use awk or so, to pre-process the org file to contain the task ids in the header lines. This should not be too hard.
    #+end_verse

*** [[https://groups.google.com/g/taskjuggler-users/c/xFmd9e8lLF0/m/gFPbg7NEAQAJ][David Elze on reports for each resource in one single page]].

    #+begin_verse
here is how I "solved" it in the meantime: (1) I created flags for every resource and (2) added those to the resources. Then, I created (3) resourcereports for every resource that include "hideresource ~(individual resource flagname)" and finally built an overall (4) textreport which includes all reports together (<[report id="individual resource flagname1"]> <[report id="individual resource flagname2"]> and so on). This way, at least by scolling the individual headers are visible for each and every resource which basically solves the initial problem for me
    #+end_verse

*** 
