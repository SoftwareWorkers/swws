# SW - Documentazione per gli utenti

Documentazione pubblica e guide per gli utenti di Software Workers.

Ove non diversamente specificato tutto il contenuto pubblicato in questo repository è rilasciato con la licenza [Creative Commons Attribution-ShareAlike 4.0 International Public License](./LICENSE).
