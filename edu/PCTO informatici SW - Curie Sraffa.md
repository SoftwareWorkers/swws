---
title: PCTO informatici SW - Curie Sraffa
description: Progetti di PCTO per l'indirizzo informatico dell'IIS Curie Sraffa proposti da SOFTWARE WORKERS srl (bozze)
langs: it-it
robots: noindex, nofollow
tags: PCTO, Curie Sraffa
---

###### tags: `PCTO` `Curie Sraffa`

> [name=Andrea Rossi][color=red] Bozza da completare in modo collaborativo: il testo è scritto e pubblicato sull'istanza aziendale di [CodiMD](https://github.com/codimd/server), a cui verrà dato accesso in scrittura agli stakeholder che ne facciano richiesta.

# PCTO SW - Curie Sraffa

#### Abstract
Questo pad contiene sei sezioni: 
* nella sezione **==1. Il contesto==** vengono presentati i due enti - scuola e azienda - impegnati in modo paritetico nella co-progettazione dei PCTO;
* la sezione **==2. Full stack web development==** illustra i PCTO destinati alla specializzazione Sviluppo Software;
* la sezione **==3. Sysadmin==** presenta i PCTO per la specializzazione Sistemi e Reti informatiche;
* la sezione **==4. Embedded==** presenta i PCTO per la specializzazione Manutenzione Hardware
* la sezione **==5. Gestione dei progetti==** descrive i criteri che scuola e azienda hanno concordato per assicurare la massima qualità dei PCTO in modo sostenibile per le reciproche strutture organizzative
* la sezione **==6. Risorse==** raccoglie testi e siti per eventuali approfondimenti

> [name=Andrea Rossi][color=red] Il primo anno è comune a tutti; dal secondo i ragazzi scelgono una specializzazione.
> Approfondire le specializzazioni citate sul sito: riguardano solo i PCTO?
> Approfondire anche il contenuto del TPS - Tecnologi Programmazione di Sistemi.


## 1. Il contesto

### L'IIS Curie Sraffa

http://www.iiscuriesraffa.it/i-nostri-corsi/informatica/

#### Le ore settimanali delle materie tecnico scientifiche

| **MATERIA** | **I** | **II** | **III** | **IV** | **V** |
| ------- | - | -- | --- | -- | - |
| Matematica | 4 | 4 | 4 | 4 | 3 |
| Tecnologie e tecniche di rappresentazione grafica | 3 | 3 | | | |
| Tecnologie informatiche | 3 | | | | |
| Scienze e tecnologie applicate (in base alla specializzazione) | |  3| | | |
| Informatica | | | 6 | 6 | 6 |
| Telecomunicazioni | | | 3 | 3 | |
| Sistemi e reti | | | 4 | 4 | 4 |
| Tecnologie e progettazione di sistemi informatici e di telecomunicazioni | | | 3 | 3 | 4 |
| Gestione progetto, organizzazione d’impresa | | | | | 3 |
| **Totale** | **10** | **10** | **20** | **20** | **20**  |


#### I programmi di Informatica, Sistemi e TPS

##### Primo anno
* Pacchetto Office
* Programmazione in linguaggio Scratch

> [name=Andrea Rossi][color=green] ==da formattare come la riga sopra==
SECONDA
    • PROGRAMMAZIONE IN LINGUAGGIO PYTHON
TERZA
    • PROGRAMMAZIONE IN LINGUAGGIO VISUAL BASIC EXPRESS 2010
    • PROGRAMMAZIONE IN LINGUAGGIO C e  C++
    • HTML 5 
    • CSS
    • DISPOSITIVI FISICI E HOST LINUX
    •  SIMULAZIONE CON PACKET TRACER 
QUARTA
    • PROGRAMMAZIONE IN LINGUAGGIO JAVA
    • PROGRAMMAZIONE IN LINGUAGGIO JAVASCRIPT
    • cablaggio rete LAN, con switch e host.
    • Configurazione delle interfacce host Linux con ip addr
    • configurazione minicom per porta console switch e router CISCO del lab (alcuni studenti)
    • Routing tra LAN : switch, router, PC – configurazione rotte statiche e gateway 
    • ARP - ARP SPOOFING - Flooding MAC SIMULAZIONE CON PACKET TRACER 
    • configurazione base di host e switch 
    • configurazione base di host, switch e router 
    • creazione di un cluster 
    • ACL standard 
    • intraVLAN routing 
    • NAT 
    • strumento "inspect" 
    • ARP poisoning e/o esercizi 
    • Instradamento con RIP, OSPF, EIGRP ANALISI DEI PACCHETTI DI RETE: Wireshark 






### La SOFTWARE WORKERS

https://softwareworkers.it/

## 2. Full stack web development

TDB

## 3. Sysadmin

TDB

## 4. Embedded

TDB

## 5. Gestione dei progetti

TDB

## 6. Risorse

### Testi

Dario Nicoli e Arduino Salatin
*[L'alternanza scuola-lavoro. 
Esempi di progetti tra classe, scuola e territorio](https://www.erickson.it/it/l-alternanza-scuolalavoro)*
Edizioni Centro Studi Erickson, 2018
ISBN 978-88-590-1487-4

Vanessa Kamkhagi
*[L'alternanza scuola-lavoro in pratica](http://www.utetuniversita.it/catalogo/scienze-umane-e-sociali/l-alternanza-scuola-lavoro-in-pratica-3597)*
De Agostini Scuola, 2017
ISBN 978-88-6008-493-4

Marco Gui
*[Il digitale a scuola](https://www.mulino.it/isbn/9788815283207)*
De Agostini Scuola, 2017
ISBN 978-88-15-28320-7


### Siti 

Formazione docenti sui PCTO presso [Formaper](
https://www.formaper.it/alternanza-scuola-lavoro/formazione-docenti)
