# List of Emacs lisp module to compile (from Org files with the same basename)
el-modules = \
Software_Environment.el

all: $(el-modules)

clean:
	rm -fv *.el
	rm -fv *.elc

%.el: %.org
	 # guix shell --container emacs-no-x -- emacs --batch -l org --eval '(org-babel-tangle-file "$(<)")'
	@echo "Please use 'M-x org-babel-tangle' inside Emacs, instead"
