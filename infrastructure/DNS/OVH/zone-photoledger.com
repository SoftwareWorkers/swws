$TTL 3600
@	IN SOA dns108.ovh.net. tech.ovh.net. (2020032500 86400 3600 3600000 300)
                          IN NS     dns108.ovh.net.
                          IN NS     ns108.ovh.net.
                          IN MX     1 redirect.ovh.net.
                          IN A      213.186.33.5
                      600 IN TXT    "v=spf1 include:mx.ovh.com ~all"
                       60 IN TXT    "4|https://github.com/SoftwareWorkersSrl/photoledger.com/blob/master/README.md"
_autodiscover._tcp        IN SRV    0 0 443 mailconfig.ovh.net.
_imaps._tcp               IN SRV    0 0 993 ssl0.ovh.net.
_submission._tcp          IN SRV    0 0 465 ssl0.ovh.net.
autoconfig                IN CNAME  mailconfig.ovh.net.
autodiscover              IN CNAME  mailconfig.ovh.net.
ftp                       IN CNAME  photoledger.com.
imap                      IN CNAME  ssl0.ovh.net.
mail                      IN CNAME  ssl0.ovh.net.
pop3                      IN CNAME  ssl0.ovh.net.
smtp                      IN CNAME  ssl0.ovh.net.
www                       IN A      213.186.33.5
www                    60 IN TXT    "4|https://github.com/SoftwareWorkersSrl/photoledger.com/blob/master/README.md"
