# SW Infrastructure
#
# node001.meup.it configuration

{ config, pkgs, ...}:

let
  nginx_port = 54321;
  sshd_port = 6922;
in
{
  imports = [
    ./hardware-configuration.nix
  ];

  boot = {
    loader = {
      grub.device = "/dev/vda";
      grub.enable = true;
      grub.version = 2;
      grub.extraConfig = ''
        serial --speed=115200 --unit=0 --word=8 --parity=no --stop=1;
        terminal_input serial;
	      terminal_output serial
	  '';
    };
    kernelParams = [ "console=ttyS0,115200" ];

  };

  environment = {
    systemPackages = with pkgs; [
      curl
      gcc
      git
      screen
      vim
      wget
    ];

    variables = {
      EDITOR = "vim";
    };
  };

  i18n = {
    defaultLocale = "en_US.UTF-8";
  };

  console.keyMap = "it";

  networking = {
    hostName = "node001";
    domain = "meup.it";
    # Allowing containers to reach other virtual machines using NAT
    nat = {
      enable=true;
      internalInterfaces=["ve-+"];
      externalInterface = "ens3";
    };
    hosts = {
      "10.1.2.10" = [ "node201.local" "llng-auth.local" ];
      "192.168.133.163" = [ "node002.local" "swpad.local" "swforum.local" ];
      "192.168.133.225" = [ "node003.local" "pgsql001.local" "swmonitor.local" "pad.narratron.local forum.narratron.local pad.7fiori.local pad.hacklabcormano.local forum.hacklabcormano.local pad.atrent.local pad.a9i.local forum.test.meup.local pad.softwareworkers.local kanboard.softwareworkers.local" ];
    };
    nameservers = [ "213.186.33.99" ];
  };

  # Public IP - static assigned via IP failover from OVH/SoYouStart
  networking.interfaces."ens6" = {
    ipv4.addresses = [ { address = "94.23.70.58"; prefixLength = 32; } ];
  };

  # Static routing for ens3 network
  networking.interfaces."ens3" = {
    ipv4.routes = [ { address = "10.1.2.0"; prefixLength = 24; via = "192.168.133.1"; } ];
  };

  # for an OVH floating IP address to be routed, the default gateway **must** be
  # the one assigned to host machine (not guest)
  # see: https://binaryimpulse.com/2014/08/ovh-ip-failover-vm-configuration/
  networking.defaultGateway = { address = "188.165.234.254"; interface = "ens6"; };

  # Firewall:
  #  - allow http and https ports used by HAProxy
  #  - masquerade containers networks
  networking.firewall = {
    allowedTCPPorts = [ 80 443 sshd_port];
    extraCommands = ''
        iptables -t nat -A POSTROUTING -s 192.168.200.0/24 -o ens6 -j MASQUERADE
      '';
    extraStopCommands = ''
        iptables -t nat -D POSTROUTING -s 192.168.200.0/24 -o ens6 -j MASQUERADE
      '';
    interfaces.ens3 = {
      allowedTCPPorts = [ 10050 10051 ];
    };
  };

  programs = {
    bash = {
      enableCompletion = true;
    };

    ssh = {
      startAgent = true;
    };
  };

  services.ssmtp = {
    enable = true;
    hostName = "submission.meup.it:587";
    useTLS = true;
    useSTARTTLS = true;
    domain = "softwareworkers.it";
    root = "admin@softwareworkers.it";
    authUser = "do.not.reply@softwareworkers.it";
    authPassFile = "/etc/nixos/secrets/defaultMailServerAuthPass";
  };

  services = {
    openssh = {
      enable = true;
      listenAddresses = [ { addr = "0.0.0.0"; port = sshd_port; } ];
      permitRootLogin = "prohibit-password";
      extraConfig = ''
       Match Group agent
         PermitTTY no
         X11Forwarding no
         PermitTunnel no
         GatewayPorts no
         ForceCommand /run/current-system/sw/bin/nologin
      '';
    };
  };

  time.timeZone = "Europe/Rome";

  users = {
    groups = {
      agent = { };
    };
    extraUsers = {
      gbiscuolo = {
        extraGroups = [ "wheel" ];
        isNormalUser = true;
        openssh.authorizedKeys.keys = with import ./ssh-keys.nix; [ gbiscuolo ];
      };

      root = {
        openssh.authorizedKeys.keys = with import ./ssh-keys.nix; [ gbiscuolo ];
      };

      webagent = {
        extraGroups = [ "agent" ];
        isNormalUser = true;
        home = "/var/webagent";
        createHome = true;
        openssh.authorizedKeys.keys = with import ./ssh-keys.nix; [ swws-webagent ];
      };

    };
  };

  system = {
    stateVersion = "19.09";
    autoUpgrade.enable = false;
    autoUpgrade.channel = https://nixos.org/channels/nixos-20.03;
  };

# HAProxy service
  services.haproxy = {
    enable = true;
    config = ''
    # Simple configuration for an HTTP frontend listening on port 80 on all
    # interfaces and forwarding requests to a single backend "servers"
    global
	  daemon
	  log /dev/log local0 info
	  user haproxy
         group haproxy
	  maxconn 256

    mailers swmailers
         mailer smtp1 localhost:25

    defaults
	 mode http
	 timeout connect 5000ms
	 timeout client 50000ms
	 timeout server 50000ms
	 email-alert mailers swmailers
         email-alert from do.not.reply@softwareworkers.it
         email-alert to admin@softwareworkers.it

    frontend http-in
	 bind *:80
	 # Add multiple certs, one for each domain.tld managed by ACME service
	 # WARNING: before enabling HTTP/2 check the DSA 4649-1 is resolved for NixOS
	 bind *:443 ssl crt /var/lib/acme/softwareworkers.it/full.pem crt /var/lib/acme/meup.io/full.pem crt /var/lib/acme/narratron.io/full.pem crt /var/lib/acme/7fiori.it/full.pem crt /var/lib/acme/cloud.hacklabcormano.it/full.pem crt /var/lib/acme/pad.atrent.it/full.pem crt /var/lib/acme/cloud.a9i.it/full.pem crt /var/lib/acme/c18e.it/full.pem alpn http/1.1

	 # service: LetsEncrypt
	 acl letsencrypt-acl path_beg /.well-known/acme-challenge/
	 use_backend letsencrypt-backend if letsencrypt-acl
	 # Redirect HTTP to HTTPS all but LetsEncrypt service
	 redirect scheme https if !{ ssl_fc } !letsencrypt-acl

	 #
	 # meup.io
	 #
	 # [auth|reload].meup.io
	 acl LLNGMeup-acl hdr(host) -i auth.meup.io
	 acl LLNGMeup-acl hdr(host) -i reload.meup.io
	 use_backend LLNGMeup-backend if LLNGMeup-acl
	 # test.meup.io
	 acl MeupTestForum-acl hdr(host) -i forum.test.meup.io
	 use_backend MeupTestForum-backend if MeupTestForum-acl
 	 # web sites
	 acl meup-acl hdr(host) -i doc.meup.io
   acl meup-acl hdr(host) -i beta.meup.io
   acl meup-acl hdr(host) -i testevent.meup.io
   acl meup-acl hdr(host) -i beta.softwareworkers.it
	 use_backend meup-backend if meup-acl

	 #
	 # narratron.io
	 #
	 acl padNarratron-acl hdr(host) -i pad.narratron.io
	 use_backend padNarratron-backend if padNarratron-acl
	 acl cloudNarratron-acl hdr(host) -i cloud.narratron.io
	 use_backend cloudNarratron-backend if cloudNarratron-acl
	 acl forumNarratron-acl hdr(host) -i forum.narratron.io
	 use_backend forumNarratron-backend if forumNarratron-acl

	 #
	 # 7fiori
	 #
	 acl cloud7fiori-acl hdr(host) -i cloud.7fiori.it
	 use_backend cloud7fiori-backend if cloud7fiori-acl
	 acl pad7fiori-acl hdr(host) -i pad.7fiori.it
         use_backend pad7fiori-backend if pad7fiori-acl
	 
	 #
	 # hacklabcormano
	 #
	 acl cloudHacklabcormano-acl hdr(host) -i cloud.hacklabcormano.it
	 use_backend cloudHacklabcormano-backend if cloudHacklabcormano-acl
	 acl padHacklabcormano-acl hdr(host) -i pad.hacklabcormano.it
	 use_backend padHacklabcormano-backend if padHacklabcormano-acl
	 acl forumHacklabcormano-acl hdr(host) -i forum.hacklabcormano.it
         use_backend forumHacklabcormano-backend if forumHacklabcormano-acl

	 # atrent
	 acl padAtrent-acl hdr(host) -i pad.atrent.it
	 use_backend padAtrent-backend if padAtrent-acl

	 # a9i
	 acl cloudA9i-acl hdr(host) -i cloud.a9i.it
	 use_backend cloudA9i-backend if cloudA9i-acl
	 acl cloudA9iv2-acl hdr(host) -i newcloud.a9i.it
	 use_backend cloudA9iv2-backend if cloudA9iv2-acl
	 acl padA9i-acl hdr(host) -i pad.a9i.it
	 use_backend padA9i-backend if padA9i-acl

	 # c18e.it
	 acl c18e-acl hdr(host) -i lists.c18e.it
	 acl c18e-acl hdr(host) -i c18e.it
	 acl c18e-acl hdr(host) -i www.c18e.it 
	 use_backend c18e-backend if c18e-acl
	 
	 #
	 # softwareworkers.it
	 #
	 # site: test.softwareworkers.it
	 acl test-SW-acl hdr(host) -i test.softwareworkers.it
	 use_backend test-SW-backend if test-SW-acl
	 # site: [www.]softwareworkers.it
	 acl softwareworkers-acl hdr(host) -i softwareworkers.it
	 use_backend softwareworkers-backend if softwareworkers-acl
	 # service: xtcloud.softwareworkers.it nextcloud instance
	 acl SWxtcloud-acl hdr(host) -i xtcloud.softwareworkers.it
	 use_backend SWxtcloud-backend if SWxtcloud-acl
	 # service: pad.softwareworkers.it
	 acl padSoftwareworkers-acl hdr(host) -i pad.softwareworkers.it
	 use_backend padSoftwareworkers-backend if padSoftwareworkers-acl
	 # service: forum.softwareworkers.it
	 acl SWforum-acl hdr(host) -i forum.softwareworkers.it
	 use_backend SWforum-backend if SWforum-acl
	 # service: xtboard.softwareworkers.it
	 acl SWkanboard-acl hdr(host) -i xtboard.softwareworkers.it
	 use_backend SWkanboard-backend if SWkanboard-acl
	 # service: monitoring
	 acl SWmonitor-acl hdr(host) -i monitor.softwareworkers.it
	 use_backend SWmonitor-backend if SWmonitor-acl
	 # service: moodle
	 acl moodleSW-acl hdr(host) -i moodle.softwareworkers.it
         use_backend moodleSW-backend if moodleSW-acl
	 # service: conf (external redirect)
	 #acl SWconf-acl hdr(host) -i conf.softwareworkers.it
	 #acl is_root path /
	 #http-request redirect code 301 location https://jitsi.linux.it/SolitaWideocall if is_root and SWconf-acl
	 
    backend letsencrypt-backend
         mode http
         server letsencrypt 127.0.0.1:${toString nginx_port}

    backend LLNGMeup-backend
         mode http
    	 server authMeup llng-auth.local:80 maxconn 32 send-proxy

    backend MeupTestForum-backend
         mode http
    	 server MeupTestForum forum.test.meup.local:8080 maxconn 32 send-proxy

    backend softwareworkers-backend
         mode http
    	 server github_pages softwareworkerssrl.github.io:80 maxconn 128

    backend SWxtcloud-backend
    	 mode http
    	 server SWcloud SWcloud.containers:80 maxconn 32 send-proxy

    backend padSoftwareworkers-backend
         mode http
    	 server padSoftwareworkers pad.softwareworkers.local:3070 maxconn 32

    backend SWforum-backend
         mode http
    	 server SWforum swforum.local:80 maxconn 32 send-proxy

    backend SWkanboard-backend
         mode http
    	 server SWkanboard kanboard.softwareworkers.local:4070 maxconn 32 send-proxy

    backend SWmonitor-backend
         mode http
    	 server SWmonitor swmonitor.local:7878 maxconn 32

    backend moodleSW-backend
         mode http
         server moodleSW SWcloud.containers:8080 maxconn 64

    backend test-SW-backend
         mode http
    	 server SWtest webtest.containers:80 maxconn 32

    backend padNarratron-backend
         mode http
    	 server padNarratron pad.narratron.local:3000 maxconn 64

    backend cloudNarratron-backend
         mode http
    	 server cloudNarratron narratron.containers:80 maxconn 64 send-proxy

    backend forumNarratron-backend
         mode http
    	 server forumNarratron forum.narratron.local:8100 maxconn 600 send-proxy

    backend cloud7fiori-backend
         mode http
         server cloud7fiori 7fiori.containers:80 maxconn 64 send-proxy

    backend pad7fiori-backend
         mode http
    	 server pad7fiori pad.7fiori.local:3020 maxconn 64

    backend cloudHacklabcormano-backend
         mode http
	 server cloudHacklabcormano hackcormano.containers:80 maxconn 64 send-proxy

    backend padHacklabcormano-backend
         mode http
	 server padHacklabcormano pad.hacklabcormano.local:3040 maxconn 64

    backend padAtrent-backend
         mode http
	 server padAtrent pad.atrent.local:3050 maxconn 64

    backend forumHacklabcormano-backend
         mode http
	 server forumHacklabcormano forum.hacklabcormano.local:8070 maxconn 128 send-proxy

    backend cloudA9i-backend
         mode http
	 server cloudA9i a9i.containers:80 maxconn 64 send-proxy

    backend cloudA9iv2-backend
         mode http
	 server cloudA9iv2 a9iv2.containers:80 maxconn 64 send-proxy

    backend padA9i-backend
         mode http
	 server padA9i pad.A9i.local:3060 maxconn 64

    backend c18e-backend
         mode http
	 server c18e c18e.containers:80 maxconn 64 send-proxy

    backend meup-backend
         mode http
         server meup meup.containers:80 maxconn 64 send-proxy

   '';
 };

  # nginx service used for ACME protocol aka LetsEncrypt
  services.nginx.enable = true;
  services.nginx.group = "haproxy";
  services.nginx.virtualHosts = {
    "acme" = {
      listen = [ { addr = "127.0.0.1"; port = nginx_port; } ];
      locations = { "/" = { root = "/var/www/challenges"; }; };
    };
  };

  # zabbix agent
  services.zabbixAgent = {
    enable = true;
    server = "swmonitor.local";
    settings = {
      Hostname = "node001";
    };
  };

  # ACME certificates
  # ATTENTION:
  # 1. for each domain (name) set the A record to HAProxy IP (94.23.70.58)
  # 2. for each extraDomain add a CNAME to node001.meup.it.
  security.acme.acceptTerms = true;
  security.acme.certs = {
    "softwareworkers.it" = {
      extraDomainNames = [
        "moodle.softwareworkers.it"
        "conf.softwareworkers.it"
        "www.softwareworkers.it"
        "xtcloud.softwareworkers.it"
        "a9icloud.softwareworkers.it"
        "pad.softwareworkers.it"
        "forum.softwareworkers.it"
        "monitor.softwareworkers.it"
        "xtboard.softwareworkers.it"
        "beta.softwareworkers.it"
      ];
      webroot = "/var/www/challenges";
      email = "domain.tech@softwareworkers.it";
      group = "haproxy";
      postRun = ''
      systemctl restart haproxy.service
    '';
    };
    "meup.io" = {
      extraDomainNames = [
        "www.meup.io"
        "doc.meup.io"
        "auth.meup.io"
        "reload.meup.io"
        "forum.test.meup.io"
        "beta.meup.io"
        "testevent.meup.io"
      ];
      webroot = "/var/www/challenges";
      email = "domain.tech@softwareworkers.it";
      group = "haproxy";
      postRun = ''
      systemctl restart haproxy.service
    '';
    };
    "narratron.io" = {
      extraDomainNames = [
        "cloud.narratron.io"
        "pad.narratron.io"
        "forum.narratron.io"
      ];
      webroot = "/var/www/challenges";
      email = "domain.tech@softwareworkers.it";
      group = "haproxy";
      postRun = ''
      systemctl restart haproxy.service
    '';
    };
    "7fiori.it" = {
      extraDomainNames = [
        "cloud.7fiori.it"
        "pad.7fiori.it"
      ];
      webroot = "/var/www/challenges";
      email = "domain.tech@softwareworkers.it";
      group = "haproxy";
      postRun = ''
      systemctl restart haproxy.service
    '';
    };
    "cloud.hacklabcormano.it" = {
      extraDomainNames = [
        "pad.hacklabcormano.it"
        "forum.hacklabcormano.it"
      ];
      webroot = "/var/www/challenges";
      directory = "/var/lib/acme/hacklabcormano.it";
      email = "domain.tech@softwareworkers.it";
      group = "haproxy";
      postRun = ''
      systemctl restart haproxy.service
    '';
    };
    "pad.atrent.it" = {
      extraDomainNames = [
      ];
      webroot = "/var/www/challenges";
      directory = "/var/lib/acme/atrent.it";
      email = "domain.tech@softwareworkers.it";
      group = "haproxy";
      postRun = ''
      systemctl restart haproxy.service
    '';
    };

    "cloud.a9i.it" = {
      extraDomainNames = [
        "pad.a9i.it"
        "newcloud.a9i.it"
        "oldcloud.a9i.it"
      ];
      webroot = "/var/www/challenges";
      directory = "/var/lib/acme/a9i.it";
      email = "domain.tech@softwareworkers.it";
      group = "haproxy";
      postRun = ''
      systemctl restart haproxy.service
    '';
    };

    "c18e.it" = {
      extraDomainNames = [
        "lists.c18e.it"
      ];
      webroot = "/var/www/challenges";
      email = "domain.tech@softwareworkers.it";
      group = "haproxy";
      postRun = ''
      systemctl restart haproxy.service
    '';
    };

  };

  # webTest container, with lighttpd daemon
  containers.webTest = {
    autoStart = true;
    privateNetwork = true;
    hostAddress = "192.168.200.10";
    localAddress = "192.168.200.11";
    config = { config, pkgs, ... }: {
      networking = {
        nameservers = [ "213.186.33.99" ];
      };
      networking.firewall = {
	      enable = true;
	      allowedTCPPorts = [ 80 ];
      };
      services.lighttpd = {
	      enable = true;
	      document-root = "/srv/webTest/httpd/";
      };
    };
  };

  # Softwareworkers production nextcloud container
  containers.SWcloud = {
    autoStart = true;
    privateNetwork = true;
    hostAddress = "192.168.200.10";
    localAddress = "192.168.200.12";
    config = { config, pkgs, ... }: {

      system = {
        stateVersion = "19.09";
      };

      networking = {
        hosts = {
          "192.168.133.225" = [ "node003.local" "pgsql001.local" "swmonitor.local" ];
          "94.23.70.58" = [ "xtcloud.softwareworkers.it" ];
        };
        nameservers = [ "213.186.33.99" ];
        firewall = {
	        enable = true;
	        allowedTCPPorts = [ 80 8080 ];
        };
      };

      services.ssmtp = {
        enable = true;
        hostName = "submission.meup.it:587";
        useTLS = true;
        useSTARTTLS = true;
        domain = "softwareworkers.it";
        root = "admin@softwareworkers.it";
        authUser = "do.not.reply@softwareworkers.it";
        authPassFile = "/etc/nixos/secrets/defaultMailServerAuthPass";
      };

      environment = {
        systemPackages = with pkgs; [
          mailutils
        ];
        variables = {
          EDITOR = "nano";
        };
      };

      # WARNING: used **only** for first installation
      # further config (means parameters in config section) must be done via web UI
      # see https://nixos.org/nixos/manual/index.html#module-services-nextcloud-pitfalls-during-upgrade
      services.nextcloud = {
        enable = true;
        logLevel = 0;
        package = pkgs.nextcloud22;
        webfinger = true;
        autoUpdateApps.enable = false;
        hostName = "xtcloud.softwareworkers.it";
        home = "/var/lib/nextcloud/xtcloud.softwareworkers.it";
        https = true;
        maxUploadSize = "512M";
        config = {
          overwriteProtocol = "https";
          dbtype = "pgsql";
          dbhost = "pgsql001.local";
          dbuser = "cloudswcloud";
          dbname = "cloudswcloud";
          dbpassFile = "/etc/nixos/secrets/xtcloud_dbpass";
          dbtableprefix = "xtcloud_";
          adminuser = "admin";
          adminpassFile = "/etc/nixos/secrets/xtcloud_adminpass";
        };
      };

      services.nginx.virtualHosts = {
        "xtcloud.softwareworkers.it" = {
          listen = [
            { addr = "0.0.0.0"; port = 80; extraParameters =  [ "proxy_protocol" ]; }
	          { addr = "[::]"; port = 80; extraParameters =  [ "proxy_protocol" ]; }
          ];
          extraConfig = "set_real_ip_from 192.168.0.0/16;\nreal_ip_header proxy_protocol;";
        };
      };

      # Borg backup jobs
      # rsyncnet = database, NextCloud data and logs on rsync.net
      services.borgbackup.jobs = {
        rsyncnet = {
          extraInitArgs = "--storage-quota 500G";
	        extraArgs = "--remote-path=borg1";
          paths = [ "/var/backup/" "/var/lib/nextcloud/" "/var/log/" ];
          exclude = [ ];
          repo = "14865@ch-s012.rsync.net:borgbackup";
          encryption = {
            mode = "keyfile-blake2";
            passCommand = "cat /etc/nixos/secrets/borgbackup-passphrase";
          };
          compression = "auto,lzma";
          startAt = "daily";
	        prune.keep = {
            within = "1d"; # Keep all archives from the last day
            daily = 14;
            weekly = 8;
            monthly = -1;  # Keep at least one archive for each month
          };
        };
      };
    };
  };

  # Narratron container
  containers.narratron = {
    autoStart = true;
    privateNetwork = true;
    hostAddress = "192.168.200.10";
    localAddress = "192.168.200.13";
    config = { config, pkgs, ... }: {

      system = {
        stateVersion = "19.09";
      };

      networking = {
        hosts = {
          "10.1.2.10" = [ "node201.local" "llng-auth.local" ];
          "192.168.133.163" = [ "node002.local" "swpad.local" "swforum.local" ];
          "192.168.133.225" = [ "node003.local" "pgsql001.local" "swmonitor.local" "pad.narratron.local" ];
        };
        nameservers = [ "213.186.33.99" ];
        firewall = {
	        enable = true;
	        allowedTCPPorts = [ 80 ];
        };
      };

      services.ssmtp = {
        enable = true;
        hostName = "submission.meup.it:587";
        useTLS = true;
        useSTARTTLS = true;
        domain = "softwareworkers.it";
        root = "admin@softwareworkers.it";
        authUser = "do.not.reply@softwareworkers.it";
        authPassFile = "/etc/nixos/secrets/defaultMailServerAuthPass";
      };


      environment = {
        systemPackages = with pkgs; [
          inetutils
	        postgresql_11
        ];
      };

      # WARNING: used **only** for first installation
      # further config (means parameters in config section) must be done via web UI
      # see https://nixos.org/nixos/manual/index.html#module-services-nextcloud-pitfalls-during-upgrade
      services.nextcloud = {
        enable = true;
        package = pkgs.nextcloud22;
        webfinger = true;
        autoUpdateApps.enable = false;
        hostName = "cloud.narratron.io";
        https = true;
        maxUploadSize = "512M";
        config = {
          dbtype = "pgsql";
          dbhost = "pgsql001.local";
	        # WARNING!!! Do **not** use underscores like user_name :-@
          dbuser = "nrrcloud";
          dbname = "nrrcloud";
    	    dbpassFile = "/etc/nixos/secrets/narratron_cloud_dbpass";
          adminuser = "admin";
          adminpassFile = "/etc/nixos/secrets/narratron_cloud_adminpass";
	        overwriteProtocol = "https";
        };
      };

      services.nginx.virtualHosts = {
        "cloud.narratron.io" = {
          listen = [
            { addr = "0.0.0.0"; port = 80; extraParameters =  [ "proxy_protocol" ]; }
  	        { addr = "[::]"; port = 80; extraParameters =  [ "proxy_protocol" ]; }
          ];
          extraConfig = "set_real_ip_from 192.168.0.0/16;\nreal_ip_header proxy_protocol;";
        };
      };


      # services.codimd = {
      #   enable = true;
      #   configuration = {
      #     allowAnonymousEdits = true;
      # 	allowEmailRegister = false;
      # 	debug = false;
      # 	domain = "pad.narratron.io";
      # 	dbURL = "postgres://narratron_cmd:oaQu3oengeimi@pgsql001.local:5432/narratron_cmd";
      # 	email = false;
      # 	host = "localhost";
      # 	useCDN = true;
      #   };
      # };
    };
  };

  # 7fiori container
  containers."7fiori" = {
    autoStart = true;
    privateNetwork = true;
    hostAddress = "192.168.200.10";
    localAddress = "192.168.200.15";
    config = { config, pkgs, ... }: {

      system = {
        stateVersion = "19.09";
      };

      networking = {
        hosts = {
          "10.1.2.10" = [ "node201.local" "llng-auth.local" ];
          "192.168.133.163" = [ "node002.local" "swpad.local" "swforum.local" ];
          "192.168.133.225" = [ "node003.local" "pgsql001.local" "swmonitor.local" ];
        };
        nameservers = [ "213.186.33.99" ];
        firewall = {
	        enable = true;
	        allowedTCPPorts = [ 80 ];
        };
      };

      services.ssmtp = {
        enable = true;
        hostName = "submission.meup.it:587";
        useTLS = true;
        useSTARTTLS = true;
        domain = "softwareworkers.it";
        root = "admin@softwareworkers.it";
        authUser = "do.not.reply@softwareworkers.it";
        authPassFile = "/etc/nixos/secrets/defaultMailServerAuthPass";
      };

      environment = {
        systemPackages = with pkgs; [
          inetutils
	        postgresql_11
        ];
      };

      # WARNING: used **only** for first installation
      # further config (means parameters in config section) must be done via web UI
      # see https://nixos.org/nixos/manual/index.html#module-services-nextcloud-pitfalls-during-upgrade
      # WARINIG: if nextcloud-setup.service fails remove /var/lib/nextcloud/config/config.php and restart the service, ONLY during first installation!
      services.nextcloud = {
        enable = true;
        package = pkgs.nextcloud22;
        webfinger = true;
        autoUpdateApps.enable = false;
        hostName = "cloud.7fiori.it";
        https = true;
        maxUploadSize = "512M";
        config = {
          dbtype = "pgsql";
          dbhost = "pgsql001.local";
    	    # WARNING!!! Do **not** use underscores like user_name :-@
          dbuser = "cloud7fiori";
          dbname = "cloud7fiori";
    	    dbpassFile = "/etc/nixos/secrets/cloud_dbpass";
          adminuser = "admin";
          adminpassFile = "/etc/nixos/secrets/cloud_adminpass";
    	    overwriteProtocol = "https";
        };
      }; # Nextcloud

      services.nginx.virtualHosts = {
        "cloud.7fiori.it" = {
          listen = [
            { addr = "0.0.0.0"; port = 80; extraParameters =  [ "proxy_protocol" ]; }
  	        { addr = "[::]"; port = 80; extraParameters =  [ "proxy_protocol" ]; }
          ];
          extraConfig = "set_real_ip_from 192.168.0.0/16;\nreal_ip_header proxy_protocol;";
        };
      };

    };
  };

  # hacklabcormano container
  containers."hackcormano" = {
    autoStart = true;
    privateNetwork = true;
    hostAddress = "192.168.200.10";
    localAddress = "192.168.200.17";
    config = { config, pkgs, ... }: {

      system = {
        stateVersion = "19.09";
      };

      networking = {
        hosts = {
          "10.1.2.10" = [ "node201.local" "llng-auth.local" ];
          "192.168.133.163" = [ "node002.local" "swpad.local" "swforum.local" ];
          "192.168.133.225" = [ "node003.local" "pgsql001.local" "swmonitor.local" ];
        };
        nameservers = [ "213.186.33.99" ];
        firewall = {
	        enable = true;
	        allowedTCPPorts = [ 80 ];
        };
      };

      services.ssmtp = {
        enable = true;
        hostName = "submission.meup.it:587";
        useTLS = true;
        useSTARTTLS = true;
        domain = "softwareworkers.it";
        root = "admin@softwareworkers.it";
        authUser = "do.not.reply@softwareworkers.it";
        authPassFile = "/etc/nixos/secrets/defaultMailServerAuthPass";
      };

      environment = {
        systemPackages = with pkgs; [
          inetutils
    	    postgresql_11
        ];
      };

      services.nextcloud = {
        enable = true;
        package = pkgs.nextcloud22;
        webfinger = true;
        autoUpdateApps.enable = false;
        hostName = "cloud.hacklabcormano.it";
        https = true;
        maxUploadSize = "512M";
        config = {
          dbtype = "pgsql";
          dbhost = "pgsql001.local";
     	    # WARNING!!! Do **not** use underscores like user_name :-@
          dbuser = "cloudhacklabcormano";
          dbname = "cloudhacklabcormano";
     	    dbpassFile = "/etc/nixos/secrets/cloud_dbpass";
          adminuser = "admin";
          adminpassFile = "/etc/nixos/secrets/cloud_adminpass";
     	    overwriteProtocol = "https";
        };
      }; # nextcloud

      services.nginx.virtualHosts = {
        "cloud.hacklabcormano.it" = {
          listen = [
            { addr = "0.0.0.0"; port = 80; extraParameters =  [ "proxy_protocol" ]; }
  	        { addr = "[::]"; port = 80; extraParameters =  [ "proxy_protocol" ]; }
          ];
          extraConfig = "set_real_ip_from 192.168.0.0/16;\nreal_ip_header proxy_protocol;";
        };
      };


    };
  }; # Container hacklabcormano

  # a9i.it container
  containers."a9i" = {
    autoStart = true;
    privateNetwork = true;
    hostAddress = "192.168.200.10";
    localAddress = "192.168.200.18";
    config = { config, pkgs, ... }: {

      system = {
        stateVersion = "19.09";
      };

      networking = {
        hosts = {
          "10.1.2.10" = [ "node201.local" "llng-auth.local" ];
          "192.168.133.163" = [ "node002.local" "swpad.local" "swforum.local" ];
          "192.168.133.225" = [ "node003.local" "pgsql001.local" "swmonitor.local" ];
        };
        nameservers = [ "213.186.33.99" ];
        firewall = {
	        enable = true;
	        allowedTCPPorts = [ 80 ];
        };
      };

      services.ssmtp = {
        enable = true;
        hostName = "submission.meup.it:587";
        useTLS = true;
        useSTARTTLS = true;
        domain = "softwareworkers.it";
        root = "admin@softwareworkers.it";
        authUser = "do.not.reply@softwareworkers.it";
        authPassFile = "/etc/nixos/secrets/defaultMailServerAuthPass";
      };

      environment = {
        systemPackages = with pkgs; [
          inetutils
    	    postgresql_11
        ];
      };

      services.nextcloud = {
        enable = true;
        package = pkgs.nextcloud22;
        webfinger = true;
        autoUpdateApps.enable = false;
        hostName = "cloud.a9i.it";
        https = true;
        maxUploadSize = "512M";
        config = {
          dbtype = "pgsql";
          dbhost = "pgsql001.local";
     	    # WARNING!!! Do **not** use underscores like user_name :-@
          dbuser = "clouda9i";
          dbname = "clouda9i";
     	    dbpassFile = "/etc/nixos/secrets/cloud_dbpass";
          adminuser = "admin";
          adminpassFile = "/etc/nixos/secrets/cloud_adminpass";
     	    overwriteProtocol = "https";
        };
      }; # nextcloud

      services.nginx.virtualHosts = {
        "cloud.a9i.it" = {
          listen = [
            { addr = "0.0.0.0"; port = 80; extraParameters =  [ "proxy_protocol" ]; }
  	        { addr = "[::]"; port = 80; extraParameters =  [ "proxy_protocol" ]; }
          ];
          extraConfig = "set_real_ip_from 192.168.0.0/16;\nreal_ip_header proxy_protocol;";
        };
      };


    };
  }; # Container a9i.it

  # c18e.it container
  containers."c18e" = {
    autoStart = true;
    privateNetwork = true;
    hostAddress = "192.168.200.10";
    localAddress = "192.168.200.19";
    config = { config, pkgs, ... }:

      # WARNING: siteowner *must* be allowed to send email from this domain!
      let
        OWNER_EMAIL = "siteowner@lists.c18e.it";
        MAILMAN_HOST = "lists.c18e.it";
        WEBSITE_HOST = "c18e.it";
      in

        {

          system = {
            stateVersion = "20.03";
          };

          networking = {
            hosts = {
              "10.1.2.10" = [ "node201.local" "llng-auth.local" ];
              "192.168.133.225" = [ "node003.local" "pgsql001.local" "swmonitor.local" ];
            };
            nameservers = [ "213.186.33.99" ];
            firewall = {
	            enable = true;
	            allowedTCPPorts = [ 80 ];
            };
          };

          environment = {
            systemPackages = with pkgs; [
              fetchmail
	            sqlite
            ];
          };

          # Cron service
          # FIXME /etc/nixos/secrets/fetchmailrc permissions to 0700
          services.cron = {
            enable = true;
            systemCronJobs = [
              "*/5 * * * * root ${pkgs.fetchmail}/bin/fetchmail -f /etc/nixos/secrets/fetchmailrc &> /dev/null"
            ];
          };

          ########################
          # mailman configuration from: https://nixos.wiki/wiki/Mailman

          # postfix delivers via relayhost, authenticated and via TLS
          services.postfix = {
            enable = true;
            relayHost = "submission.meup.it";
            relayPort = 587;
            relayDomains = ["hash:/var/lib/mailman/data/postfix_domains"];
            config = {
              transport_maps = ["hash:/var/lib/mailman/data/postfix_lmtp"];
              local_recipient_maps = ["hash:/var/lib/mailman/data/postfix_lmtp"];
              smtp_use_tls = "yes";
              smtp_sasl_security_options = "noanonymous";
              smtp_sasl_auth_enable = "yes";
              smtp_sender_dependent_authentication = "yes";
              smtp_sasl_password_maps = ["hash:/etc/nixos/secrets/postfix-sasl-passwords"];
            };
          };

          # mailman service
          services.mailman = {
            enable = true;
            serve.enable = true;
            siteOwner = OWNER_EMAIL;
            hyperkitty.enable = true;
            webHosts = [MAILMAN_HOST];
            extraPythonPackages = with pkgs; [
              pythonPackages.psycopg2
            ];
            settings = {
              "database" = {
                class = "mailman.database.postgresql.PostgreSQLDatabase";
                url = "postgres://mailmanc18e:pe3go6ke8wi7@pgsql001.local/mailmanc18e";
              };
            };
            webSettings = {
              DEFAULT_FROM_EMAIL = OWNER_EMAIL;
              SERVER_EMAIL = OWNER_EMAIL;
              ALLOWED_HOSTS = [ "localhost" "127.0.0.1" MAILMAN_HOST ];
              COMPRESS_OFFLINE = true;
              STATIC_ROOT = "/var/lib/mailman-web-static";
              MEDIA_ROOT = "/var/lib/mailman-web/media";
              DATABASES = {
                "default" = {
                  ENGINE = "django.db.backends.postgresql_psycopg2";
                  NAME = "mailmanwebc18e";
                  USER = "mailmanc18e";
                  PASSWORD = "pe3go6ke8wi7";
                  HOST = "pgsql001.local";
                  PORT = "5432";
                };
              };
              LOGGING = {
                version = 1;
                disable_existing_loggers = true;
                handlers.console.class = "logging.StreamHandler";
                loggers.django = {
                  handlers = [ "console" ];
                  level = "INFO";
                };
              };
              HAYSTACK_CONNECTIONS.default = {
                ENGINE = "haystack.backends.whoosh_backend.WhooshEngine";
                PATH = "/var/lib/mailman-web/fulltext-index";
              };
            }; # websettings
          }; # mailman3

          # nginx with virtualhosts
          services.nginx = {
            enable = true;
            recommendedGzipSettings = true;
            recommendedProxySettings = true;
            # mailman service virtualhost
            virtualHosts.${MAILMAN_HOST} = {
              enableACME = false;
              listen = [
                { addr = "0.0.0.0"; port = 80; extraParameters =  [ "proxy_protocol" ]; }
  	            { addr = "[::]"; port = 80; extraParameters =  [ "proxy_protocol" ]; }
              ];
              extraConfig = "set_real_ip_from 192.168.0.0/16;\nreal_ip_header proxy_protocol;";
            }; # MAILMAN_HOST
            # web site
            virtualHosts.${WEBSITE_HOST} = {
              enableACME = false;
              root = "/srv/sites/c18e.it/";
              serverAliases = ["www.${WEBSITE_HOST}" "web.${WEBSITE_HOST}"];
              listen = [
                { addr = "0.0.0.0"; port = 80; extraParameters =  [ "proxy_protocol" ]; }
  	            { addr = "[::]"; port = 80; extraParameters =  [ "proxy_protocol" ]; }
              ];
              extraConfig = "set_real_ip_from 192.168.0.0/16;\nreal_ip_header proxy_protocol;";
            }; # web site
          };
        }; # let
  }; # Container c18e.it

  # meup container
  containers."meup" = {
    autoStart = true;
    privateNetwork = true;
    hostAddress = "192.168.200.10";
    localAddress = "192.168.200.20";
    config = { config, pkgs, ... }:

      let
        OWNER_EMAIL = "admin@softwareworkers.it";
        DOCMEUP_VIRTUALHOST = "doc.meup.io";
        DOCMEUP_ROOT = "doc.meup";
        SWBETA_VIRTUALHOST = "beta.softwareworkers.it";
        SWBETA_ROOT = "beta.swws";
        BETAMEUP_VIRTUALHOST = "beta.meup.io";
        BETAMEUP_ROOT = "beta.meup";
        TEVENT_VIRTUALHOST = "testevent.meup.io";
        TEVENT_ROOT = "testevent.meup";
      in

        {

          system = {
            stateVersion = "20.03";
            # FIXME: in NixOS 22.05 use the option users.extraUsers.<name>.homeMode
            activationScripts = { websites.text =
              ''
              chmod go+rx /srv/sites
              '';
             };
          };

          networking = {
            hosts = {
              "10.1.2.10" = [ "node201.local" "llng-auth.local" ];
              "192.168.133.225" = [ "node003.local" "pgsql001.local" "swmonitor.local" ];
            };
            nameservers = [ "213.186.33.99" ];
            firewall = {
	            enable = true;
	            allowedTCPPorts = [ 80 ];
            };
          };

          services = {
            openssh = {
              enable = true;
              listenAddresses = [ { addr = "0.0.0.0"; port = 22; } ];
              permitRootLogin = "prohibit-password";
              # TODO: set chroot for webagent user
              # extraConfig = ''
              #  Subsystem sftp internal-sftp
              #    Match User webagent
              #    ChrootDirectory /srv/sites/
              # '';
            };
          };

          users = {
            groups = {
              agent = { };
            };
            extraUsers = {
              gbiscuolo = {
                extraGroups = [ "wheel" ];
                isNormalUser = true;
                openssh.authorizedKeys.keys = with import ./ssh-keys.nix; [ gbiscuolo ];
              };

              root = {
                openssh.authorizedKeys.keys = with import ./ssh-keys.nix; [ gbiscuolo ];
              };

              webagent = {
                isNormalUser = true;
                extraGroups = [ "agent" ];
                home = "/srv/sites";
                # FIXME: in NixOS 22.05 uncomment next option
                # homeMode = "755";
                openssh.authorizedKeys.keys = with import ./ssh-keys.nix; [ swws-webagent ];
              };

            };
          };

          # nginx with virtualhosts
          services.nginx = {
            enable = true;
            recommendedGzipSettings = true;
            recommendedProxySettings = true;
            # WARNING: increasing server_names_hash_bucket_size for long server names
            appendHttpConfig = ''
             server_names_hash_bucket_size  64;
            '';
            virtualHosts.default = {
              default = true;
              enableACME = false;
              listen = [
                { addr = "0.0.0.0"; port = 80; extraParameters =  [ "proxy_protocol" ]; }
  	            { addr = "[::]"; port = 80; extraParameters =  [ "proxy_protocol" ]; }
              ];
              extraConfig = "return 444;\nset_real_ip_from 192.168.0.0/16;\nreal_ip_header proxy_protocol;";
            }; # end default virtualhost
            virtualHosts.${DOCMEUP_VIRTUALHOST} = {
              enableACME = false;
              root = "/srv/sites/${DOCMEUP_ROOT}/";
              serverAliases = ["www.${DOCMEUP_VIRTUALHOST}" "web.${DOCMEUP_VIRTUALHOST}"];
              listen = [
                { addr = "0.0.0.0"; port = 80; extraParameters =  [ "proxy_protocol" ]; }
  	            { addr = "[::]"; port = 80; extraParameters =  [ "proxy_protocol" ]; }
              ];
              extraConfig = "set_real_ip_from 192.168.0.0/16;\nreal_ip_header proxy_protocol;";
            }; # end DOCMEUP_VIRTUALHOST
            virtualHosts.${SWBETA_VIRTUALHOST} = {
              enableACME = false;
              root = "/srv/sites/${SWBETA_ROOT}/";
              serverAliases = ["www.${SWBETA_VIRTUALHOST}" "web.${SWBETA_VIRTUALHOST}"];
              listen = [
                { addr = "0.0.0.0"; port = 80; extraParameters =  [ "proxy_protocol" ]; }
  	            { addr = "[::]"; port = 80; extraParameters =  [ "proxy_protocol" ]; }
              ];
              extraConfig = "set_real_ip_from 192.168.0.0/16;\nreal_ip_header proxy_protocol;";
            }; # end SWBETA_VIRTUALHOST
            virtualHosts.${BETAMEUP_VIRTUALHOST} = {
              enableACME = false;
              root = "/srv/sites/${BETAMEUP_ROOT}/";
              serverAliases = ["www.${BETAMEUP_VIRTUALHOST}" "web.${BETAMEUP_VIRTUALHOST}"];
              listen = [
                { addr = "0.0.0.0"; port = 80; extraParameters =  [ "proxy_protocol" ]; }
  	            { addr = "[::]"; port = 80; extraParameters =  [ "proxy_protocol" ]; }
              ];
              extraConfig = "set_real_ip_from 192.168.0.0/16;\nreal_ip_header proxy_protocol;";
            }; # end BETAMEUP_VIRTUALHOST
            virtualHosts.${TEVENT_VIRTUALHOST} = {
              enableACME = false;
              root = "/srv/sites/${TEVENT_ROOT}/";
              serverAliases = ["www.${TEVENT_VIRTUALHOST}" "web.${TEVENT_VIRTUALHOST}"];
              listen = [
                { addr = "0.0.0.0"; port = 80; extraParameters =  [ "proxy_protocol" ]; }
  	            { addr = "[::]"; port = 80; extraParameters =  [ "proxy_protocol" ]; }
              ];
              extraConfig = "set_real_ip_from 192.168.0.0/16;\nreal_ip_header proxy_protocol;";
            }; # end TEVENT_VIRTUALHOST
          };
        }; # let
  }; # Container meup

  # container a9i.it ver. 2
  containers."a9iv2" = {
    autoStart = true;
    privateNetwork = true;
    hostAddress = "192.168.200.10";
    localAddress = "192.168.200.21";
    config = { config, pkgs, ... }: {

      system = {
        stateVersion = "20.03";
      };

      networking = {
        hosts = {
          "10.1.2.10" = [ "node201.local" "llng-auth.local" ];
          "192.168.133.163" = [ "node002.local" "swpad.local" "swforum.local" ];
          "192.168.133.225" = [ "node003.local" "pgsql001.local" "swmonitor.local" ];
        };
        nameservers = [ "213.186.33.99" ];
        firewall = {
	        enable = true;
	        allowedTCPPorts = [ 80 ];
        };
      };

      services.ssmtp = {
        enable = true;
        hostName = "submission.meup.it:587";
        useTLS = true;
        useSTARTTLS = true;
        domain = "softwareworkers.it";
        root = "admin@softwareworkers.it";
        authUser = "do.not.reply@softwareworkers.it";
        authPassFile = "/etc/nixos/secrets/defaultMailServerAuthPass";
      };

      environment = {
        systemPackages = with pkgs; [
          inetutils
       	  postgresql_11
        ];
      };

      services.nextcloud = {
        enable = true;
        logLevel = 0;
        package = pkgs.nextcloud22;
        webfinger = true;
        autoUpdateApps.enable = false;
        hostName = "newcloud.a9i.it";
        https = true;
        maxUploadSize = "512M";
        config = {
          dbtype = "pgsql";
          dbhost = "pgsql001.local";
      	  # WARNING!!! Do **not** use underscores like user_name :-@
          dbuser = "clouda9iv2";
          dbname = "clouda9iv2";
      	  dbpassFile = "/etc/nixos/secrets/cloud_dbpass";
          adminuser = "admin";
          adminpassFile = "/etc/nixos/secrets/cloud_adminpass";
      	  overwriteProtocol = "https";
        };
      }; # nextcloud

      services.nginx.virtualHosts = {
        "newcloud.a9i.it" = {
          listen = [
            { addr = "0.0.0.0"; port = 80; extraParameters =  [ "proxy_protocol" ]; }
    	      { addr = "[::]"; port = 80; extraParameters =  [ "proxy_protocol" ]; }
          ];
          extraConfig = "set_real_ip_from 192.168.0.0/16;\nreal_ip_header proxy_protocol;";
        };
      };
    };
  }; # Container a9i.it ver. 2

}
