# Note per il deployment

FIXME: fare un piccolo Makefile per automatizzare il deployment e la manutenzione dei file di questa cartella

## Configurazione del nodo remoto

Per il deployment della configurazione occorre:

- copiare configuration.nix in /etc/nixos/ sul nodo remoto
- copiare ./secrets/SWcloud/* in /var/lib/containers/SWcloud/etc/nixos/secrets/
- copiare ./secrets/host/* in /etc/nixos/secrets
- eseguire ``sudo nixos-rebuild test`` sul nodo remoto
- se tutto OK ``sudo nixos-rebuild switch`` sul nodo remoto

## Gestione dei secrets (parametri passwordFile)

I file usati per i parametri di tipo `passwordFile` (es. `adminpassFile`) devono essere copiati sul nodo **prima** che i servizi che utilizzano tali parametri vengano istanziati, altrimenti non saranno disponibili in fase di `rebuild` della derivation.

Quanto sopra esposto implica che i *nuovi container* devono essere dichiarati (senza servizi inclusi) *prima* dei servizi che utilizzeranno i file di tipo `passwordFile` nella propria configurazione, in questo modo sarà possibile copiare i file *secrets* nell'apposita cartella `/var/lib/containers/<nome_container>`, dentro la root del container.
