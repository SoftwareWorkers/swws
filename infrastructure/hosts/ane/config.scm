;; Very basic Guix System
;; 2 disks in BTRFS RAID1 configuration
;; BIOS boot scheme
(use-modules (gnu))
(use-service-modules admin dns linux networking ssh)
(use-package-modules admin certs ssh)

;; -------------------------------------
;; Groups and users definitions
(define (sysadmin name full-name)
  (user-account
   (name name)
   (comment full-name)
   (group "users")
   (supplementary-groups '("wheel" "kvm"))
   (home-directory (string-append "/home/" name))))

(define %accounts
  (list (sysadmin "g" "Giovanni Biscuolo")))

;; -----------------------------------
;; DNS definitions

(define-zone-entries meup-dns.entries
  ("@"  ""  "IN"  "NS" "ns") ;; primary authoritative DNS
  ("ns" ""  "IN"  "A"  "162.55.88.253"))

(define-zone-entries meup-mail.entries
  ("@"               "" "IN" "MX"  "10 mx1.meup.it.")
  ("@"               "" "IN" "TXT" "\"v=spf1 mx a:0.a.mx.0xe1.net -all\"")
  ("_dmarc"          "" "IN" "TXT" "\"v=DMARC1; p=reject; adkim=r; aspf=r; sp=reject\""))

(define-zone-entries meup-dkim.entries
  ("dkim._domainkey" "" "IN" "TXT" "\"k=rsa; t=s; p=MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEA0dOJdpy0VPq+/iOzAb6/MvsWp/xqqbi5qvFCibt7kp7uCMe3vOgcQdc6hH2YPvjfEw4Og0m67gRsmo/lWbI+ASc6jquf+CLjsiRHlWxUAqQWmb2/BAtkH0NW7Zbg3CnOqUjYUMIVtZloMQwGk/1gYu0JlJ+Q6JiKHvy9JPPscMlr7kCy+9r+TTFZ9tEUQVMm\" \"eHlUwXDjHnr29Lhy/3xX4Z835cSP+X/niFPDoEqtxFDPB5xcVYRAAC5HnXHTUbO/TG+pP+IiDVVZ4WZgfi3qOevg+/Zxy3zQE1YzYh2oIZHBTFsPnQqDCdX6AIXbzgKx7ynmVW7kBRXJEvUILL3z9wIDAQAB\""))

(define-zone-entries meup.entries
  ("@"                "" "IN" "A"     "192.30.252.153")
  ("mx1"              "" "IN" "A"     "188.165.234.93") ;; MX record defined in meup-mail.entries
  ("_imaps._tcp"      "" "IN" "SRV"   "0 4 993 imap.softwareworkers.it.")
  ("_submission._tcp" "" "IN" "SRV"   "0 4 587 submission.softwareworkers.it.")
  ("admin"            "" "IN" "A"     "188.165.234.93")
  ("demo"             "" "IN" "MX"    "1 mx1.meup.it.")
  ("imap"             "" "IN" "A"     "188.165.234.93")
  ("imap2"            "" "IN" "A"     "188.165.234.93")
  ("mail"             "" "IN" "A"     "188.165.234.93")
  ("node001"          "" "IN" "A"     "94.23.70.58")
  ("proxy"            "" "IN" "CNAME" "node001")
  ("submission"       "" "IN" "A"     "188.165.234.93")
  ("test"             "" "IN" "MX"    "1 aspmx.l.google.com."))

(define meup-zone
   (knot-zone-configuration
    (domain "meup.it")
    (dnssec-policy "default")
    (serial-policy (quote unixtime))
    (zone (zone-file
           (origin "meup.it")
	   (serial (current-time))
           (entries (append
		     meup-dns.entries
		     meup-mail.entries
		     meup-dkim.entries
		     meup.entries))))))

(define-zone-entries swws-dkim.entries
  ("dkim._domainkey" "" "IN" "TXT" "\"v=DKIM1;k=rsa; t=s; p=MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEA14hRLF7x9QYycPeLQJvKfNvK9T4Rea5KNaXBsA4NY0C0MUfRkKwNUasTIpWJg9/BaHcL662MbLbHWF5AfxFohJBMAPSWhHcZ2lixoBhx7htPASRplp0k6KKgyCoVoxl9GJpXcWyUITvMsbR9sgKoIkyZsGODFYLqDJ3RVlonXNwSbGFvNOGioLeL1BcVxsQf\" \"R8UaQSgGxMAFvaLHzp1t5J3LY5r1ACusaDMPKQPhLif75RE/qoblZykBo/2K5YpekPmIyDXKV5ac8Zcx1L4rIknfa4If2+XzjEEUq9t+lIeYzYDJ0+bk9T0KtY/uKSHM46fTfdYtxHKjZcv663JP+QIDAQAB\""))

(define-zone-entries swws.entries
  ("@"          "" "IN" "A"     "94.23.70.58")
  ("@"          "" "IN" "TXT"   "\"google-site-verification=auDsOeEM0fvtZvz7IfntMqeAfBjgOEqeWsieq2GWank\"")
  ("a9icloud"   "" "IN" "CNAME" "proxy.meup.it.")
  ("api"        "" "IN" "TXT"   "\"test api\"")
  ("beta"       "" "IN" "CNAME" "proxy.meup.it.")
  ("conf"       "" "IN" "CNAME" "proxy.meup.it.")
  ("forum"      "" "IN" "CNAME" "proxy.meup.it.")
  ("fosdem2017" "" "IN" "A"     "192.30.252.153")
  ("git"        "" "IN" "A"     "91.134.2.184")
  ("kb"         "" "IN" "TXT"   "\"1|https://gitlab.com/softwareworkers\"")
  ("monitor"    "" "IN" "CNAME" "proxy.meup.it.")
  ("moodle"     "" "IN" "CNAME" "proxy.meup.it.")
  ("null"       "" "IN" "CNAME" "proxy.meup.it.")
  ("pad"        "" "IN" "CNAME" "proxy.meup.it.")
  ("test"       "" "IN" "CNAME" "proxy.meup.it.")
  ("www"        "" "IN" "A"     "94.23.70.58")
  ("www"        "" "IN" "TXT"   "\"3|welcome\"")
  ("www"        "" "IN" "TXT"   "\"l|it\"")
  ("xtboard"    "" "IN" "CNAME" "proxy.meup.it.")
  ("xtcloud"    "" "IN" "CNAME" "proxy.meup.it."))

(define swws-zone
   (knot-zone-configuration
    (domain "softwareworkers.it")
    (dnssec-policy "default")
    (serial-policy (quote unixtime))
    (zone (zone-file
           (origin "softwareworkers.it")
	   (serial (current-time))
           (entries (append
		     meup-dns.entries
		     meup-mail.entries
		     swws-dkim.entries
		     swws.entries))))))

;; ------------------------------------
;; operating-system
(operating-system
  (locale "en_US.utf8")
  (timezone "Europe/Rome")
  (keyboard-layout (keyboard-layout "us"))
  (host-name "ane")

  (bootloader (bootloader-configuration
	       (bootloader grub-bootloader)
	       (targets (list "/dev/nvme0n1" "/dev/nvme1n1"))
               (terminal-inputs '(serial console))
	       (terminal-outputs '(serial console))))

  (file-systems (append
		 (list (file-system
			(mount-point "/")
			(device (uuid "e30c650b-2014-4a5b-ad5f-01fc778af5ff"))
			(type "btrfs")
			(options "compress=zstd")))
		 %base-file-systems))
   
  (swap-devices
   (list (swap-space (target (uuid "c869da7f-3063-42ba-bbef-2932b51e57c6")))
         (swap-space (target (uuid "35080c58-0a73-48c3-8b8f-cae8569b313f")))))

  (issue
   ;; Default contents for /etc/issue.
   "\
This a Guix system.  Welcome.\n")

  (users (append %accounts %base-user-accounts))
  
  (sudoers-file
   (plain-file "sudoers" "\
root ALL=(ALL) ALL
%wheel ALL=(ALL) ALL\n"))

  ;; Globally-installed packages.
  (packages (cons nss-certs %base-packages))

  (services
   (append %base-services
           (list
            (service static-networking-service-type
        	     (list (static-networking
        		    (addresses (list (network-address
        				      (device "eno1")
        				      (value "162.55.88.253/24"))))
        		    (routes (list (network-route
        				   (destination "default")
        				   (gateway "162.55.88.193"))))
        		    (name-servers '("185.12.64.1"
        				    "185.12.64.1")))))
	    (service knot-service-type
                          (knot-configuration
                           (zones (list
				   meup-zone
				   swws-zone))))
        
            (service openssh-service-type
        	     (openssh-configuration
        	      (port-number 22)
                      (password-authentication? #f)
                      (permit-root-login 'prohibit-password)
                      (extra-content "ListenAddress 0.0.0.0")
        	      (authorized-keys
        	       `(("g" ,(plain-file "g.pub" "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAICqpr0unFxPo2PnQTmmO2dIUEECsCL3vVvjhk5Dx80Yb g@xelera.eu"))
                          ("root" ,(plain-file "g.pub" "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAICqpr0unFxPo2PnQTmmO2dIUEECsCL3vVvjhk5Dx80Yb g@xelera.eu"))))))))))
        
