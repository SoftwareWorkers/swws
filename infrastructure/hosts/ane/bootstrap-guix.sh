#!/bin/bash

# Install Guix on a dedicated Hetzner server via rescue mode
# Host: softwareworkers.rescue.ane (defined in ~/.ssh/config)

# This is specific for one host, since many variables depends on its hardware caracteristic
# Heavily inspired by: https://guix.gnu.org/en/cookbook/en/guix-cookbook.html#Running-Guix-on-a-Kimsufi-Server

###########
# Variables

# TODO: transform this in array TARGET_DISKS[TARGET_NUMDISKS]
TARGET_NUMDISKS=2
TARGET_DISK1="/dev/nvme0n1"
TARGET_DISK2="/dev/nvme1n1"
TARGET_PARTITION_PREFIX="p"
TARGET_SWAP_SIZE="16GB"

TARGET_HOSTNAME="ane"

# User and pub key
TARGET_USERNAME="g"
TARGET_USERGECOS="Giovanni Biscuolo"
TARGET_USERKEY="ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAICqpr0unFxPo2PnQTmmO2dIUEECsCL3vVvjhk5Dx80Yb g@xelera.eu"

# Networking
TARGET_NIC="eno1"
TARGET_IPV4_ADDR="162.55.88.253/24"
TARGET_IPV4_GW="162.55.88.193"
TARGET_IPV4_DNS1="185.12.64.1"
TARGET_IPV4_DNS2="185.12.64.1"

# IPV6_ADDR="2a01:4f8:271:185a::/64"
# IPV6_GW="fe80::1"
# IPV6_DNS1="2a01:4ff:ff00::add:1"
# IPV6_DNS2="2a01:4ff:ff00::add:2"

# Basic OS configuration name
BASE_OS="/root/bootstrap-config.scm"

# Target OS mount point
MOUNTPOINT="/mnt/guix"

# ---------------------------------------------------------------------
# Install Guix and prepare the target system

# Install guix from Binary Installation in GNU Guix: 
echo "### START - Installing Guix binary"
wget https://git.savannah.gnu.org/cgit/guix.git/plain/etc/guix-install.sh
chmod +x guix-install.sh
./guix-install.sh
hash guix
guix pull
echo "### END - Installing Guix binary"

# Wipe the disks
# TODO: use the array TARGET_DISKS[]
echo "### START - Wiping disks"
wipefs -a ${TARGET_DISK1}
wipefs -a ${TARGET_DISK2}
echo "### STOP - Wiping disks"

# Partition the disks

## Disk 1
echo "### START - Partitioning disks"
parted ${TARGET_DISK1} --align=opt -s -m -- mklabel gpt
# BIOS grub system partition
parted ${TARGET_DISK1} --align=opt -s -m -- \
       mkpart grub 1MiB 5MiB \
       name 1 grub-1 \
       set 1 bios_grub on
# partition p2 will be swap
parted ${TARGET_DISK1} --align=opt -s -m -- \
       mkpart primary linux-swap 5MiB ${TARGET_SWAP_SIZE} \
       name 2 swap-1
# partition p3 will be BTRFS device
parted ${TARGET_DISK1} --align=opt -s -m -- \
       mkpart primary btrfs ${TARGET_SWAP_SIZE} 100% \
       name 3 BTRFS-1

## Disk 2
parted ${TARGET_DISK2} --align=opt -s -m -- mklabel gpt
# BIOS grub system partition
parted ${TARGET_DISK2} --align=opt -s -m -- \
       mkpart grub 1MiB 5MiB \
       name 1 grub-1 \
       set 1 bios_grub on
# partition p2 will be swap
parted ${TARGET_DISK2} --align=opt -s -m -- \
       mkpart primary linux-swap 5MiB ${TARGET_SWAP_SIZE} \
       name 2 swap-1
# partition p3 will be BTRFS device
parted ${TARGET_DISK2} --align=opt -s -m -- \
       mkpart primary btrfs ${TARGET_SWAP_SIZE} 100% \
       name 3 BTRFS-1
echo "### END - Partitioning disks"

# Make swap on second partitions and turn them on
echo "### START - Making swap"
mkswap ${TARGET_DISK1}${TARGET_PARTITION_PREFIX}2
swapon ${TARGET_DISK1}${TARGET_PARTITION_PREFIX}2
mkswap ${TARGET_DISK2}${TARGET_PARTITION_PREFIX}2
swapon ${TARGET_DISK2}${TARGET_PARTITION_PREFIX}2
echo "### END - Making swap"

# Create BTRFS filesystem
echo "### START - Making BTRFS flesystem and subvolumes"
mkfs.btrfs -f -d raid1 -m raid1 ${TARGET_DISK1}${TARGET_PARTITION_PREFIX}3 ${TARGET_DISK2}${TARGET_PARTITION_PREFIX}3

# Mount the target Guix System root
mkdir -p ${MOUNTPOINT}
mount -o compress=zstd ${TARGET_DISK1}${TARGET_PARTITION_PREFIX}3 ${MOUNTPOINT}
systemctl daemon-reload

# Create subvolumes
btrfs subvolume create ${MOUNTPOINT}/var
btrfs subvolume create ${MOUNTPOINT}/home
btrfs subvolume create ${MOUNTPOINT}/srv
btrfs subvolume create ${MOUNTPOINT}/root
btrfs subvolume create ${MOUNTPOINT}/gnu

##########################
# Bootstrap configuration
cat > ${BASE_OS} << EOF
;; Very basic Guix System
;; 2 disks in BTRFS RAID1 configuration
;; BIOS boot scheme

(use-modules (gnu) (guix gexp))
(use-modules (gnu packages admin))
(use-service-modules admin networking ssh linux)
(use-package-modules ssh certs)

;; Definitions
(define (sysadmin name full-name)
  (user-account
   (name name)
   (comment full-name)
   (group "users")
   (supplementary-groups '("wheel" "kvm"))
   (home-directory (string-append "/home/" name))))

(define %accounts
  (list (sysadmin "${TARGET_USERNAME}" "${TARGET_USERGECOS}")))

;; operating-system
(operating-system
  (locale "en_US.utf8")
  (timezone "Europe/Rome")
  (keyboard-layout (keyboard-layout "us"))
  (host-name "${TARGET_HOSTNAME}")

  (bootloader (bootloader-configuration
	       (bootloader grub-bootloader)
	       (targets (list "${TARGET_DISK1}" "${TARGET_DISK2}"))
               (terminal-inputs '(serial console))
	       (terminal-outputs '(serial console))))

  (file-systems (append
		 (list (file-system
			(mount-point "/")
			(device (uuid "`blkid -o value -s UUID ${TARGET_DISK1}${TARGET_PARTITION_PREFIX}3`"))
			(type "btrfs")
			(options "compress=zstd")))
		 %base-file-systems))
   
  (swap-devices
   (list (swap-space (target (uuid "`blkid -o value -s UUID ${TARGET_DISK1}${TARGET_PARTITION_PREFIX}2`")))
         (swap-space (target (uuid "`blkid -o value -s UUID ${TARGET_DISK2}${TARGET_PARTITION_PREFIX}2`")))))

  (issue
   ;; Default contents for /etc/issue.
   "\\
This a Guix system.  Welcome.\n")

  (users (append %accounts %base-user-accounts))
  
  (sudoers-file
   (plain-file "sudoers" "\\
root ALL=(ALL) ALL
%wheel ALL=(ALL) ALL\n"))

  ;; Globally-installed packages.
  (packages (cons nss-certs %base-packages))

  (services
   (append %base-services
           (list
            (service static-networking-service-type
        	     (list (static-networking
        		    (addresses (list (network-address
        				      (device "${TARGET_NIC}")
        				      (value "${TARGET_IPV4_ADDR}"))))
        		    (routes (list (network-route
        				   (destination "default")
        				   (gateway "${TARGET_IPV4_GW}"))))
        		    (name-servers '("${TARGET_IPV4_DNS1}"
        				    "${TARGET_IPV4_DNS2}")))))
        
            (service unattended-upgrade-service-type)

            (service openssh-service-type
        	     (openssh-configuration
        	      (port-number 22)
                      (password-authentication? #f)
                      (permit-root-login 'prohibit-password)
                      (extra-content "ListenAddress 0.0.0.0")
        	      (authorized-keys
        	       \`(("${TARGET_USERNAME}" ,(plain-file "${TARGET_USERNAME}.pub" "${TARGET_USERKEY}"))
                          ("root" ,(plain-file "${TARGET_USERNAME}.pub" "${TARGET_USERKEY}"))))))))))
        
EOF

# ---------------------------------------------------------------------
# Install Guix in target filesystem
echo "### START - Installing Guix on ${MOUNTPOINT}"
guix system init ${BASE_OS} ${MOUNTPOINT}
echo "### END - Installing Guix on ${MOUNTPOINT}"

# ---------------------------------------------------------------------
# End game
echo ""
echo "### DONE! - Target system in ${MOUNTPOINT} is ready..."
echo ""
echo "Please remember to copy ${BASE_OS} to a safe remote location"
echo ""
echo "...and reboot to start your new Guix System!  Bye."
