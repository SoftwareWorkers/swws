;; Very basic Guix System
;; 2 disks in BTRFS RAID1 configuration
;; BIOS boot scheme

(use-modules (gnu) (guix gexp))
(use-modules (gnu packages admin))
(use-service-modules admin networking ssh linux)
(use-package-modules ssh certs)

;; Definitions
(define (sysadmin name full-name)
  (user-account
   (name name)
   (comment full-name)
   (group "users")
   (supplementary-groups '("wheel" "kvm"))
   (home-directory (string-append "/home/" name))))

(define %accounts
  (list (sysadmin "g" "Giovanni Biscuolo")))

;; operating-system
(operating-system
  (locale "en_US.utf8")
  (timezone "Europe/Rome")
  (keyboard-layout (keyboard-layout "us"))
  (host-name "ane")

  (bootloader (bootloader-configuration
	       (bootloader grub-bootloader)
	       (targets (list "/dev/nvme0n1" "/dev/nvme1n1"))
               (terminal-inputs '(serial console))
	       (terminal-outputs '(serial console))))

  (file-systems (append
		 (list (file-system
			(mount-point "/")
			(device (uuid "e30c650b-2014-4a5b-ad5f-01fc778af5ff"))
			(type "btrfs")
			(options "compress=zstd")))
		 %base-file-systems))
   
  (swap-devices
   (list (swap-space (target (uuid "c869da7f-3063-42ba-bbef-2932b51e57c6")))
         (swap-space (target (uuid "35080c58-0a73-48c3-8b8f-cae8569b313f")))))

  (issue
   ;; Default contents for /etc/issue.
   "\
This a Guix system.  Welcome.\n")

  (users (append %accounts %base-user-accounts))
  
  (sudoers-file
   (plain-file "sudoers" "\
root ALL=(ALL) ALL
%wheel ALL=(ALL) ALL\n"))

  ;; Globally-installed packages.
  (packages (cons nss-certs %base-packages))

  (services
   (append %base-services
           (list
            (service static-networking-service-type
        	     (list (static-networking
        		    (addresses (list (network-address
        				      (device "eno1")
        				      (value "162.55.88.253/24"))))
        		    (routes (list (network-route
        				   (destination "default")
        				   (gateway "162.55.88.193"))))
        		    (name-servers '("185.12.64.1"
        				    "185.12.64.1")))))
        
            (service unattended-upgrade-service-type)

            (service openssh-service-type
        	     (openssh-configuration
        	      (port-number 22)
                      (password-authentication? #f)
                      (permit-root-login 'prohibit-password)
                      (extra-content "ListenAddress 0.0.0.0")
        	      (authorized-keys
        	       `(("g" ,(plain-file "g.pub" "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAICqpr0unFxPo2PnQTmmO2dIUEECsCL3vVvjhk5Dx80Yb g@xelera.eu"))
                          ("root" ,(plain-file "g.pub" "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAICqpr0unFxPo2PnQTmmO2dIUEECsCL3vVvjhk5Dx80Yb g@xelera.eu"))))))))))
        
