# Configuration of node003.meup.it

This host is a Guix System, see the [configuration file](./config.scm) to see the details about installed Guix services.

## Configuration sync

Use `make sync` in this folder to deploy to the remote node.  The remote `node softwareworkers.node003` must be defined in your `~/.ssh/config`.

## Docker

For some services - expecially web services - still not available in Guix we use Docker containers. To instantiate the containers we use the standard [docker compose](https://docs.docker.com/compose/compose-file/) as configured in [docker-compose.yml](./config/docker-compose.yml).

### Docker for Discourse

Currently there is still no standard way (i.e. Docker Compose) to instantiate Discourse containers.  Upstream distributes a custom tool, called `launcher`, used to bootstrap one or more containers for Discourse services.

The official [Docker Discourse guide](https://github.com/discourse/discourse_docker/blob/master/README.md) explains how to configure and instantiate new Discourse containers via `launcher`.

We use a custom (forked from [upstream](https://github.com/discourse/discourse_docker.git)) [Discourse Docker container](git@gitlab.com:softwareworkers/dockerfiles/discourse.git) as [git submodule](./config/dockerfiles/discourse_docker) of this repository. The `sw-develop` branch holds all our customizations, we periodically rebase it on upstream master to upgrade our Discourse Docker "machinery".

Containers are defined in special YAML files stored in a [containers directory](./config/dockerfiles/discourse_docker_containers) that is part of this repo and to make them available to the `laucher` official script we *symlink* them in the [git submodule containers dir](./config/dockerfiles/discourse_docker/containers).

**Volumes** for persistent container data *must* be created in a domain dedicated subdirectory `/var/lib/discourse/<FQDN>` (on `node003`).
