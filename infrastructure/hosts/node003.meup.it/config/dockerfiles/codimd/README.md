# CodiMD application #

This folder contains [Alpine dockerfile](https://github.com/codimd/container/tree/master/alpine) _and_ [configuration resources](https://github.com/codimd/container/tree/master/resources) needed to build a CodiMD docker image.
