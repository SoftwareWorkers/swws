;; node003 system configuration

;; Guix modules dependencies
(use-modules (gnu))
(use-service-modules networking ssh mcron databases monitoring web desktop dbus docker)
(use-package-modules certs databases monitoring docker rsync version-control)

;; Definitions
(define gc-job
  ;; Run 'guix gc' at 3AM every day.
  #~(job '(next-hour '(3)) "guix gc -F 50G"))

;; OS declararion
(operating-system
  (locale "en_US.utf8")
  (timezone "Europe/Rome")
  (keyboard-layout
   (keyboard-layout "it" "nodeadkeys"))
  (kernel-arguments '("console=ttyS0,115200"))
  (bootloader
    (bootloader-configuration
      (bootloader grub-bootloader)
      (target "/dev/vda")
      (terminal-inputs '(serial))
      (terminal-outputs '(serial))
      (keyboard-layout keyboard-layout)))
  (file-systems
    (cons* (file-system
             (mount-point "/")
             (device
               (uuid "9862e534-946d-4323-b7ce-9937661bdb7d"
                     'btrfs))
             (type "btrfs"))
           %base-file-systems))
  (swap-devices '("/dev/vdb"))
  (host-name "node003")
  (hosts-file (plain-file "hosts"
			  "
127.0.0.1 localhost node003 pgsql001.local
::1       localhost node003
  "))

  (users (cons* (user-account
                  (name "gbiscuolo")
                  (comment "Giovanni Biscuolo")
                  (group "users")
                  (home-directory "/home/gbiscuolo")
                  (supplementary-groups
                    '("wheel" "docker" "netdev" "audio" "video")))
                %base-user-accounts))
  (packages
    (cons* nss-certs postgresql docker-cli docker-compose rsync git %base-packages))

  (services
    (append
     (list (service openssh-service-type
                    (openssh-configuration
                     (port-number 22)
		     (extra-content "ListenAddress 0.0.0.0")
                     (authorized-keys
                       `(("gbiscuolo" ,(local-file "keys/ssh/gbiscuolo.pub"))))))

	   (service dhcp-client-service-type)

	   (service mcron-service-type
                     (mcron-configuration
                      (jobs (list gc-job))))

	   (service ntp-service-type) ;; clock **must** be in sysc

	   (elogind-service) ;; needed by docker-service
	   (dbus-service) ;; needed by docker-service
           (service docker-service-type)

	   ;; For the Zabbix database.  It was created by manually
	   ;; following the instructions here:
	   ;; https://www.zabbix.com/documentation/4.2/manual/appendix/install/db_scripts

	   (service postgresql-service-type
                    (postgresql-configuration
                     (config-file
                      (postgresql-config-file
                       (hba-file
			(plain-file "pg_hba.conf"
	                            "
local	all	all	                  peer
host    all     all     127.0.0.1/32      md5
host	all	all     192.168.0.0/16    md5
host    all     all     172.16.0.0/12     md5
host	all	all	::1/128 	  md5
"))
                     (extra-config
                      '(("listen_addresses" "'*'")))))))

	   ;; Monitoring

	   (service zabbix-agent-service-type)

           (service zabbix-server-service-type
		    (zabbix-server-configuration
		     (include-files '("/root/secrets/zabbix-server-dbpass"))
		     (log-type "file")))

           (service zabbix-front-end-service-type
                    (zabbix-front-end-configuration
                     (nginx (list
                             (nginx-server-configuration
                              (root #~(string-append #$zabbix-server:front-end "/share/zabbix/php"))
                              (listen '("7878"))
			      (index '("index.php"))
			      (locations
			       (let ((php-location (nginx-php-location)))
				 (list (nginx-location-configuration
					(inherit php-location)
					(body (append (nginx-location-configuration-body php-location)
						      (list "
fastcgi_param PHP_VALUE \"post_max_size = 16M
                          max_execution_time = 300\";
"))))))))))
                       (db-secret-file "/root/secrets/zabbix-front-end-dbpass"))))     
     %base-services)))
