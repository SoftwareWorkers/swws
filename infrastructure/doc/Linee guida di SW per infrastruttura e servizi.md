---
title: Linee guida di SW per infrastruttura e servizi
description: Criteri di gestione e implementazione
langs: it-it
robots: noindex, nofollow 

---

###### tags: `SOFTWARE WORKERS` `SW` `infrastruttura` `SSO` `SSO` `LemonLDAP::NG`


In questo messaggio mi limiterò ad illustrare i principi e i criteri
tecnici "infrastrutturali" elaborati ad oggi sulla base della politica
generale condivisa in SW, che sintetizzerei (a modo mio) in:

1. SW fornisce servizi in hosting o on-premises ai clienti,
l'infrastruttura e i servizi stessi devono garantire sicurezza a ogni
livello dello "stack", a partire dal "bare metal" [1]

2. da 1. ne deriva che l'intera infrastruttura di SW e i relativi
servizi sono basati solo su software libero

3. SW non profila i propri clienti ala "surveillance captalism"

4. SW non intende usare metodi di lock-in dei clienti, anzi a regime
fornirà loro le risorse necessarie per eventualmente riprodurre
l'infrastruttura e i servizi altrove (questo criterio aggiuntivo è
ancora in _bozza_)


Veniamo ora alle considerazioni in merito all'infrastruttura.

# Infrastructure stack

Il principio fondamentale adottato per la gestione della propria
infrastruttura (e di quelle gestite on-premises per conto dei clienti)
da parte di SW è quello di tenere strettamente sotto controllo l'intera
gestione della "Software Supply Chain".

Il concetto è illustrato molto bene da questo articolo
https://web.archive.org/web/20170810041438/http://blogs.tedneward.com/post/developer-scm/,
che riassumerei in questo paragrafo:

--8<---------------cut here---------------start------------->8---
All of these are the kinds of questions that any manufacturing company
has had to wrestle with, under the larger term “Supply Chain
Management”. If you currently run a software development department, and
you currently use libraries that are developed out-of-house (which is to
say, everybody), then you owe it to your customers and consumers and
operations staff and executive management to read up on this subject and
find some ideas for how to manage your software supply chain.
--8<---------------cut here---------------end--------------->8---

Alla luce di questo principio, SW ha deciso di utilizzare uno stack
misto nella propria infrastruttuta, valutando per ciascuno strumento
nello stack la propria adeguatezza a tenere sotto controllo la "Supply
Chain" del software utilizzato nell'infrastruttura e per l'erogazione
dei servizi-

## Guix https://guix.gnu.org/

Ogni volta che è disponibile un servizio in Guix System, SW si deve
implementare quello.

Guix è il package manager, Guix System è il sistema operativo basato su
Guix e configurabile in modo dichiarativo attraverso uno speciale
Embedded Domain Specific Language che *estende* il linguaggio Guile
Scheme.

Per la descrizione delle caratteristiche principali di Guix,
trovo questo un interessante executive summary:
https://github.com/malcook/sce/blob/master/MakingTheCase.org

### Coinvolgimento nella comunità Guix

Considerato l'alto valore strategico di Guix System per SW, nel medio
periodo uno degli obiettivi di SW è quello di essere parte attiva
all'interno della comunità Guix contribuendo con codice per la
definizione di nuovi pacchetti ma soprattutto di nuovi servizi [2],
dando priorità a quelli strategici per la nostra offerta.

### Infrastructure as code, code as data

Tutto ciò che nell'infrastruttura è definito in Guile Scheme può essere
trattato sia come codice, quindi implementato via Guix System, che
come dati, quindi elaborato per diventare per esempio: documentazione
dell'infrastruttura, template per l'istanziazione automatica.

A regime SW vorrebbe utilizzare questa caratteristica della
configurazione di Guix System per sviluppare una interfaccia di
self-service attraverso la quale i clienti non solo possano istanziare
in autonomia i servizi, ma anche (ri)configurarli, attraverso una
compinazione di template per ciascun servizio e macro per la generazione
del codice. 

Un interessante articolo in merito è "Lisp for the Web"
http://www.adamtornhill.com/articles/lispweb.htm, che usa Common Lisp
per gli esempi ma può essere implementato in qualsiasi dialetto Lisp moderno.

## NixOS https://nixos.org/

Il secondo sistema operativo nello stack di SW è NixOS, il sistema
operativo basato su Nix con una configurazione di tipo dichiarativo [3].

Nix possiede le caratteristiche di: configurazione dichiarativa,
roll-back delle istanze, build dei pacchetti in un processo isolato
dall'ambiente utente.

Le caratteristiche principali di NixOS sono descritte qui:
https://nixos.org/nixos/about.html
 
### Criticità di Nix 

Rispetto a Guix System, NixOS utilizza un DSL funzionale sviluppato
ad-hoc, non estende altri linguaggi esistenti: questo lo rende meno
versatile rispetto al EDSL utilizzato da Guix System.

L'approccio nella creazione e inclusione dei pacchetti in Nix a volte è
un po' sbrigativo e per aggirare alcuni problemi non esitano a includere
binari precompilati anzichè fare il build da sorgente; questo approccio
è illustrato bene in questo articolo "Let's Package jQuery: A Javascript
Packaging Dystopian Novella" [4], cito questo estratto:

--8<---------------cut here---------------start------------->8---
And let's face it, "fuck it, I'm out" seems to be the mantra of web
application packaging these days. Our deployment and build setups have
gotten so complicated that I doubt anyone really has a decent
understanding of what is going on, really.
--8<---------------cut here---------------end--------------->8---

Per questi motivi SW non ritiene di investire nello sviluppo di nuovi
pacchetti o servizi non già disponibili in NixOS.

## Debian

Ove non siano disponibili servizi in Guix System o NixOS, SW utilizza i
servizi disponibili in Debian (stable), possibilmente installati in un
container LXC definito e gestito via libvirt/virsh per una
configurazione il più possibile dichiarativa (in questo caso XML di
libvirt).

### Difetti di Debian

In Debian la configurazione (solitamente in /etc) è _stateful_, ovvero
fa parte integrante dello stato della macchina e non è per nulla
semplice adottare un sistema di configurazione _stateless_.

La totalità dei sistemi di configuration management disponibili, da
ansible a puppet, _non_ risolvono questo problema (punto da sviluppare
in futuro).

Essendo l'intero sistema operativo stateful, l'unico modo per garantire
la rirpoducibilità dell'implementazione è quello di effettuare snapshot
periodici (backup) dell'intero sistema. Il fatto che il sistema sia un
container rende più semplice la procedura. Utilizzare btrfs per le root
dei container rende ancora più semplice effettuare snapshots ed
eventualmente effettuare il roll-back a versioni precedenti, in caso di
problemi durante un upgrade di sistema.

## Docker

Ove non siano disponibili servizi per i sistemi operativi ufficialmente
supportati da SW, vengono utilizzati i "container Docker" (in questo
contesto si intende immagini di applicazioni create per mezzo di un
Dockerfile), messi solitamente a disposizione upstream come mezzo
ufficiale di distribuzione o - ove ragionevole da un punto di vista
della sicurezza - da terze parti che rielaborano i Dockerfile iniziali.

Nell'adottare queste immagini di applicazioni SW stabilisce questi
ulteriori criteri per tenere sotto controllo la sicurezza e la gestione
dei container:

1. tutti i servizi devono essere implementati attraverso Docker compose
(https://docs.docker.com/compose/) con relativo file di configurazione
docker-compose.yml; eventuali deroghe a questa regola possono essere
date solo a fronte documentata impossibilità di utilizzo dell'immagine
con Docker compose e un piano di integrazione dell'app in Docker
compose deve essere improntato e implementato in tempi ragionevoli

2. tutte le immagini utilizzate devono essere compilate in locale
(ovvero all'interno dell'infrastruttura SW) per mezzo di un Dockerfile
valutato e approvato dal responsabile IT di SW; eventuali deroghe a
questa regola possono essere date solo a fronte di una documentata
impossibilità di ricompilare localmente le immagini e un piano di
compilazione locale deve essere improntato e implementato **con
urgenza**

### Problemi con Docker

Docker, come sistema di istanziazione di container, non ha assolutamente
nessun problema di sicurezza e non pone nessun particolare allarme il
suo utilizzo dell'infrastruttura SW.

Il fondamentale (e troppo spesso sottovalutato) problema di Docker sta
nell'utilizzo dei Dockerfiles, che purtroppo in moltissimi casi sono mal
progettati e installano nelle immagini software potenzialmente insicuro,
a volte direttamente in formato binario. L'utilizzo massiccio della
direttiva FROM, molto diffuso, rende l'analisi del build delle immagini
un'operazione estremamente complessa e dispendiosa, che in SW
preferiremmo non essere costretti a fare per poterci concentrare su cose
più produttive.

Anche dal punto di vista legale l'utilizzo di Dockerfiles con molteplici
overlay rende la situazione più complicata rispetto all'adozione dei
sistemi elencati sopra.

In un articolo intitolato "Containers and license compliance" [5] il
responsabile open source di VMware illustra bene sia i problemi di
sicurezza che quelli legali dovuto ad un uso "spregiudicato" (piuttosto
diffuso) dei Dockerfiles. Questi i punti fondamentali della sua analisi:

1. People do "incredibly dumb stuff" in their Dockerfiles, including adding
new repositories with higher priorities than the standard distribution
repositories, then doing an update. That means the standard packages
might be replaced with others from elsewhere. Once again, that is a
security nightmare, but it may also mean that there is no source code
available and/or that the license information is missing. This is not
something he made up, he said, if you look at the Docker repositories,
you will see this kind of thing all over; many will just copy their
Dockerfiles from elsewhere.

2. There is a "rabbit hole" that you need to follow, Dockerfile to
Dockerfile, to figure out what you are actually shipping. He has done a
search of official Docker images and did not find a single one that
follows compliance best practices. All of the Dockerfiles grab other
Dockerfiles—on and on.

3. Containers need to be built starting from a base that has known-good
package versions, corresponding source code, and licenses. The
anti-pattern of installing stuff from random internet locations needs to
be avoided. And software developers need to be trained about the
pitfalls of the container build systems, which should not be hard, but
is.

Tutte le attività sopra elencate sono piuttosto dispendiose da
effettuare internamente in SW, per questo motivo per ciascun servizio
implementato attraverso un Dockerfile SW deve progettare e implementare
una soluzione alternativa che eviti quelle incombenze.

# Reproducible builds, non binari prefconfezionati.

SW adotta solo software che rispettano i principi definiti da
Reproducible Builds [6], questo significa che evita qualsiasi binario
precompilato che non provenga da una fonte riproducibile (es. i
substitutes di Guix o di Nix) e che possa quindi essere riprodotto e
verificato localmente anche da terze parti.

## I problemi del build non riproducibile

Il 26 Marzo 2019 è stata pubblicata
nel repository ufficiale RubyGems (https://rubygems.org/) la versione
3.2.0.3 della libreria bootstrap-sass, versione che conteneva una
backdoor che consentiva l'esecuzione remota di codice sul server.

Il nocciolo della questione è che il codice upstream di bootstrap-sass
non ha *mai* incluso la backdoor, quella backdoor è stata inserita da un
attaccante che è riuscito a rubare le credenziali RubyGem di uno degli
sviluppatori upstream e le ha usate per caricare la versione già
"pacchettizzata" della versione con la backdoor. In altre parole il
repository RubyGem _non_ si occupa di fare il build (la
pacchettizzazione) del codice upstream, ma solo di distribuire la
versione già pacchettizzata.

In un articolo intitolato "Subversion of bootstrap-sass" [7] David
Wheeler elenca alcuni strumenti per evitare simili situazioni [8], tra
cui il fondamentale (a nostro giudizio) è:

--8<---------------cut here---------------start------------->8---

Require a reproducible build. The attackers subverted the distributed
package on RubyGems without first posting the corresponding source code
in its official source code repository on GitHub. That’s a big red
flag. I believe package repositories should verify that code distributed
can be reproducibly regenerated from its putative source. Such an
approach would have prevented this attack, and also made the previous
“event-stream” incident far more visible than it was. They don't need to
change their inputs; accept a build, and check that the regenerating the
build will produce the same thing.

--8<---------------cut here---------------end--------------->8---

SW ritiene che simili problemi possano riguardare anche altri sistemi di
distribuzione analoghi a RubyGems (npmjs, pypi, ecc) e che non è detto
che questi tipi di attacchi siano scoperti in tempi brevi come nel caso
di bootstrap-sass, soprattutto quando si tratta di librerie o pacchetti
meno famosi.




[1] per quanto ragionevolmente possibile considerato lo stato dell'arte,
vedi ad es. il problema con Intel Management Engine 

[2] https://guix.gnu.org/manual/devel/en/html_node/Services.html

[3] Guix si ispira a Nix e in un certo senso ne è l'evoluzione.

[4] https://web.archive.org/web/20180528141816/http:/dustycloud.org/blog/javascript-packaging-dystopia/

[5] https://web.archive.org/web/20180914155209/https:/lwn.net/Articles/752982/

[6] https://reproducible-builds.org/docs/definition/

[7] https://dwheeler.com/essays/bootstrap-sass-subversion.html

[8] interessante tutta la serie https://dwheeler.com/essays/learning-from-disaster.html