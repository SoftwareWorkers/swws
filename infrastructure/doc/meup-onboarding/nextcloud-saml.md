# Nextcloud - SAML configuration

1. login as `admin` using the *direct* login method (aka bypassing SAML auth): https://cloud.example.com/login?direct=1

1. install "SSO & SAML authentication" additional application (find it in section "Integration" or search in the search bar)

1. go to "Settings|Admin|SAML" and select "Use integrated SAML auth"

TODO: study howto setup SAML using env variables

1. enable "Use SAML auth for the Nextcloud desktop clients (requires user re-authentication)"

1. `Attribuite to map UID to`: `uid`

1. `Optional diplay name...`: `meup.io SSO`

1. `X.509 Certificate...`: copy `secrets/containers/<container>/cloud_saml.pem` content in this field

1. `Private key of the Service provider`: copy `secrets/containers/<container>/cloud_saml.key` content in this field

1. `Identifier of the IdP`: `https://auth.meup.io/saml/metadata`

1. `URL Target of the IdP where the SP will send...`: `https://auth.meup.io/saml/singleSignOn`

1. `URL Location of the Idp ... SLO request`: `https://auth.meup.io/saml/proxySingleLogout`

1. `Public X.509 certificate of the IdP`:

```
-----BEGIN CERTIFICATE-----
MIIGJTCCBA2gAwIBAgIUaX2vDwV4JSjNFJs+wuWKOD0vTQIwDQYJKoZIhvcNAQEL
BQAwgaExCzAJBgNVBAYTAklUMQswCQYDVQQIDAJNSTEOMAwGA1UEBwwFTWlsYW4x
HDAaBgNVBAoME1NvZnR3YXJld29ya2VycyBzcmwxFDASBgNVBAsMC0ludGVybmFs
IENBMRUwEwYDVQQDDAxhdXRoLm1ldXAuaW8xKjAoBgkqhkiG9w0BCQEWG3N5c2Fk
bWluQHNvZnR3YXJld29ya2Vycy5pdDAeFw0xOTExMjIwOTIyMzJaFw0yOTExMTkw
OTIyMzJaMIGhMQswCQYDVQQGEwJJVDELMAkGA1UECAwCTUkxDjAMBgNVBAcMBU1p
bGFuMRwwGgYDVQQKDBNTb2Z0d2FyZXdvcmtlcnMgc3JsMRQwEgYDVQQLDAtJbnRl
cm5hbCBDQTEVMBMGA1UEAwwMYXV0aC5tZXVwLmlvMSowKAYJKoZIhvcNAQkBFhtz
eXNhZG1pbkBzb2Z0d2FyZXdvcmtlcnMuaXQwggIiMA0GCSqGSIb3DQEBAQUAA4IC
DwAwggIKAoICAQC69qFuXMJb5AU7ofkxEj4qYJJexHe0i43LSN/50Zy7h0EWW6Mv
pIs1ZV5ol6wJiczXsbxFeg6fUwXpGbUYlJQMK3EF5oxI9UNxFUI7NBhY/CoezV8x
h5E0Vtl4Xen9v/oDgMhXQiQpzugA/+gyHRLCdPcyv0YyAhHU1IBdsQ0cRoO/FWsT
+IGZs7ZII5GjebkE5pR07sj6o+gx0I6zX3zpHkSFreIJievVq0YsdVQW9lBe82U7
SlszxPB9FIoIlpOvxH8K4fF4ih8ANIdyfebZPyQZAAS+/YbWQxdOpqmB9gBr/acn
pG8+d0uhGAfeXw2tgzSlfGXDAXkWiZhPpAsWoT9/61y3JB6VgE+XBhIQBYPV6tqt
SGS1BmHrkVASpnUIlwcGQYS4xf39j9kNmCImbVMVFW0w49LCRCXoOvk3gw/BCcUA
qOnVoQ/ZhwWwoHhCD/3bK38abLFWwXusrnVbYLVcm0nUndQOiB6uO6XNrsoqAklV
P0jb+liZ5aNTCzHrT8VREaBjZ79pW/nfmLD6xgRaNUZ4fXhUAc5aGgrqz/AFDUmm
iPuQadO8wPEpMzx1EhvhQsk5tE79v2aR+QNJ95hmuTlp3XvOiD6X5gwgPHNBTD6P
HOZg1xO4FP3jHJ0JgOz3zfduHb+W/Yoxm849aLBDM0U06aM9oUS3IDBiqwIDAQAB
o1MwUTAdBgNVHQ4EFgQUj8Nwaqx0rZwKEoD07Ur75sJIiCwwHwYDVR0jBBgwFoAU
j8Nwaqx0rZwKEoD07Ur75sJIiCwwDwYDVR0TAQH/BAUwAwEB/zANBgkqhkiG9w0B
AQsFAAOCAgEAs35tHWWdBfY0vV0X1ikbERWFFdKW/bzegwskY3wTkuuxFe7LL3x6
Rw3oHPujvbCQkEokXvDE0yKLGLCNvmd5l+3+TRQEv0Cv/RNxOgRIlcGwI3Qsf67k
EV9jdpqnD+usKZcqw1JNT0K4gm8pZlj4hBsPFXzgUpAOTSfRjfYdBFyXG1GXSNb2
sNUVLmnDXfTXskt19lUVyWCGcuiqbLn89qQrc3MWJ81+b2wc7S601ZJshcJI5Noq
h2bSsmCBjoIf48Ezlc+NVGio9SjgnMH6Sv6ZIkQZtkvLyLWVwc9w639KQdet+ygI
ZyEin9bkWOJDsxzAI9E28YjfmXJn3R96qZd7pJgewm8NlnG/y3KBgq9KhD8E17ji
Y9Mp1SrTDiaMsNY1xdhsWAh4u8Xgu9cWLxJr/2/33eVeGVFzEa+II178YN+2aHib
dSkSNMm/wNSf+EO+fp0uGy0KNbgX3n2bwWaKEq3liXmAzZi4bgHkjS6WXmNMTQBT
SZa9Yh1Qtfy1RxxUIxZLvLitxX1O+STIVtrxIS9cs3a77Jz4G+s7n7IN4hrveiJS
gg9RpA1H3rBltSNoGn0ww7BPz5IiGvxYJgMHXIoQ2AMpnVpuD6owdClLZ0CMAsM1
DpKBXSi/gWSt2pYjzmxt1Jd7S9glBT8uSIO6f2MfbTK7bpuWytojzig=
-----END CERTIFICATE-----
```

1. `Attribute to map displayname`: `cn`

1. `Attribute to map email address`: `mail`

1. Security settings, enable:

  1. Indicates that the nameID of the <samlp:logoutRequest> sent by this SP will be encrypted.

  1. Indicates whether the <samlp:AuthnRequest> messages sent by this SP will be signed. [Metadata of the SP will offer this info]

  1. Indicates whether the <samlp:logoutRequest> messages sent by this SP will be signed.

  1. Indicates whether the <samlp:logoutResponse> messages sent by this SP will be signed.

1. Download metadata XML and save it in `secrets/containers/<container>/cloud_SAML_metadata.xml`

