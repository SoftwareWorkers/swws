# LDAP - Add new group record for customers

1. add a new group using this LDIF template:

```
dn: cn=<shortdomain>,ou=groups,o=websso,dc=example,dc=tld
changetype: add
objectClass: top
objectClass: groupOfNames
cn: <shortdomain>
description: <description>
member: uid=gbiscuolo,ou=users,o=websso,dc=example,dc=tld
```

or this ldapvi template:

```
add cn=<shortdomain>,ou=groups,o=websso,dc=example,dc=tld
objectClass: top
objectClass: groupOfNames
cn: <shortdomain>
description: <description>
member: uid=gbiscuolo,ou=users,o=websso,dc=example,dc=tld
```

