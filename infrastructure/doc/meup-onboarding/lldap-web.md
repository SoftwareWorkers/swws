# LemonLDAP::NG - Add services via manager

New services must be added to our LemonLDAP::NG SSO management service.

## Connection to manager interface

For security reasons, connection to LemonLDAP::NG manager web interface is allowed **only** from localhost.

To be able to connect to the web interface you must:

1. set this record in your local `/etc/hosts` file:

```
# Lemonldap on node201.meup.it (via ssh tunnel)
127.0.0.1       manager.meup.io
```

1. open an ssh tunnel to `node201` this way:

```
ssh -L 9876:manager.meup.io:8080 -C -N softwareworkers.node201
```

1. open the [Manager web interface](http://manager.meup.io:9876/#/confs/latest) with a web browser.

## Add new SP for the service

1. Add a new "SAML Service Provider" named `<shortname>-<SP>` (e.g.: `narratron-cloud` (*WARNING*: SP name cannot start with a digit) with this values:

  1. Metadata: load from SP URL
  
  1. Exported attributes:
  
     a. for Nextcloud, CodiMD: `[cn -> cn, mandatory]`; `[mail -> mail, mandatory]`; `[uid -> uid, mandatory]`

	 b. for Discourse: `[mail -> email, mandatory]`; `[cn -> screenName, mandatory]`; `[ uid -> name, mandatory]`; `[ givenName -> first_name, mandatory]`; `[ sn -> last_name, mandatory]`

  1. Options | Security | Access Rule: `$groups =~ /\b<group>\b/` (replace group)

1. Add "Virtual host"

   1. Set default access rule to `$groups =~ /\b<group>\b/`
   1. Set "Options|Type" to `CDA`

1. Add "New application" in "General Parameters|Portal|Menu|Categories...|Your Applications"
