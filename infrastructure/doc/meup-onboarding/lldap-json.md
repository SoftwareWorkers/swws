# LemonLDAP::NG - Add services via json config editing

**WARNING** still a WIP, see FIXME

New services can be added to our LemonLDAP::NG SSO management service by directly editing `/etc/lemonldap-ng/conf/lmConf-1.json` on `node201`.

## Add new Application

1. Add a record in section `"applicationList"|"0006-cat"`:

```
            "<NNNN>-app": {
                "options": {
                    "description": "<description>",
                    "display": "auto",
                    "logo": "network.png",
                    "name": "<name>",
                    "uri": "https://<FQDN>"
                },
                "type": "application"
            },
```

1. update `"cfgDate"` using today's date and time in epoch (use `date +%s` in a terminal)

1. update `"cfgLog"` with a short description of the added service

1. add ampty `"exportedHeaders"` list like:

```
"<FQDN>": {},
```

1. add `"locationRules"` record for the service, using this template:

```
        "URI": {
            "default": "$groups =~ /\\b<group>\\b/"
        },
```

1. add `"post"` record, using this template:

```
"URI": null,
```

1. add `"samlSPMetaDataExportedAttributes"` record:

```
        "<SP name>": {
            "cn": "1;cn;;",
            "mail": "1;mail;;",
            "uid": "1;uid;;"
        },

```

1. add `"samlSPMetaDataOptions"` record:

```
        "<shortname>-cloud": {
            "samlSPMetaDataOptionsCheckSLOMessageSignature": 1,
            "samlSPMetaDataOptionsCheckSSOMessageSignature": 1,
            "samlSPMetaDataOptionsEnableIDPInitiatedURL": 0,
            "samlSPMetaDataOptionsEncryptionMode": "none",
            "samlSPMetaDataOptionsForceUTF8": 1,
            "samlSPMetaDataOptionsNameIDFormat": "",
            "samlSPMetaDataOptionsNotOnOrAfterTimeout": 72000,
            "samlSPMetaDataOptionsOneTimeUse": 0,
            "samlSPMetaDataOptionsRule": "$groups =~ /\\b<group>\\b/",
            "samlSPMetaDataOptionsSessionNotOnOrAfterTimeout": 72000,
            "samlSPMetaDataOptionsSignSLOMessage": -1,
            "samlSPMetaDataOptionsSignSSOMessage": 1
        },
```

1. add `"samlSPMetaDataXML"` record:

FIXME: XML metadata **must** be escaped to be valid json... still have to find a way to "manually" do it

```
        "<shortname>-cloud": {
            "samlSPMetaDataXML": "<XML metadata from SP config"
        },

```

1. Add `"vhostOptions"` record:

```
        "<FQDN>": {
            "vhostHttps": -1,
            "vhostMaintenance": 0,
            "vhostPort": -1,
            "vhostType": "CDA"
        },

```

1. **Validate** Json: `jq '.' lmConf-1.json`

