# Documenting *MeUp! onboarding

This document is still a work in progress.

## Terms and definitions

1. `<shortdomain>` is the [second level domain](https://en.wikipedia.org/wiki/Second-level_domain) *excluding* all [reserved ones](https://en.wikipedia.org/wiki/Second-level_domain#Country-code_second-level_domains)

1. `<Shortdomain>` is the [Camel Case](https://en.wikipedia.org/wiki/Camel_case] version of `<shortdomain>`.

## Registration of new domains.

TODO

## Domain management.

We can provide our services both for *full managed domains* or *customer managed domains*.

FIXME: define and document full and customer managed domains in our public facing documentation.

### Full managed domains.

For a *full managed domain* we must get all [required contacts](https://en.wikipedia.org/wiki/Domain_name#Technical_requirements_and_process):

* Owner: customer provided email for this role.

* Administrative Contact: same as owner (default on Gandi.net)

* Technical Contact: domain.tech@softwareworkers.it (dedicated alias)

* Billing Contact: domain.billing@softwareworkers.it (dedicated alias)

For Software Workers *owned* domains we have defined this additional contancts (dedicated aliases):

* domain.owner@softwareworkers.it

* domain.admin@softwareworkers.it

### Customer managed domanins

For a *customer managed domain* customer is responsible of DNS configuration, all we have to do is provide customer with one DNS record for each service we provide.

TODO: document DNS records customers must add.

## ACME service for X.509 certificates

### Full managed domains.

1. Replace the top A record in the DNS, using this one:

```
@ 10800 IN A 94.23.70.58
```

Where `94.23.70.58` is the public IP of our HAProxy host `proxy.meup.it.`.

1. Add a new section `security.acme.certs` of [node001 configuration](./hosts/node001.meup.it/configuration.nix) using this template:

```
  "<FQDN>" = {
    extraDomains = {
    };
    webroot = "/var/www/challenges";
    email = "domain.tech@softwareworkers.it";
    user = "haproxy";
    group = "haproxy";
    postRun = ''
      systemctl reload haproxy.service
    '';
  };
```

### Customer managed domanins

1. Add a new section `security.acme.certs` of [node001 configuration](./hosts/node001.meup.it/configuration.nix) using this template:

```
  "<service>.<FQDN>" = {
    extraDomains = {
    };
    webroot = "/var/www/challenges";
    directory = "/var/lib/acme/<FQDN>";
    email = "domain.tech@softwareworkers.it";
    user = "haproxy";
    group = "haproxy";
    postRun = ''
      systemctl reload haproxy.service
    '';
  };
```

Where `<service>` is the name of the service to manage.

### Finalization of certificate creation or update

1. sync and apply the updated configuration to `node001`

1. create certificate on `node001`: `sudo systemctl restart acme-<FQDN>`

1. If still not present, add the `full`pem certificate to the `bind` directive of HAProxy

### ACME certificates troubleshhoting

If for any reason you change the configuration of one `security.acme.certs` and it fails or one certificate renewal stops working, try removing the related subdirectory in `/var/lib/acme` and restart the related `acme-<FQDN>` service.

## Services in NixOS containers

Some services are available on NixOS, we run them in a customer dedicated container on `node001` (NixOS).

### create customer container

For each customer we define a new container in [node001 configuration](./hosts/node001.meup.it/configuration.nix).

1. add a new container, `<container_name>` **lenght must be less than 12 characters**, use a human readeable shorter version of `<shortname>` if needed

```
# <shortname> container
containers."<container_name>" = {
  autoStart = true;
  privateNetwork = true;
  hostAddress = "192.168.200.10";
  localAddress = "192.168.200.<next_free>";
  config = { config, pkgs, ... }: {
    networking = {
      hosts = {
         "10.1.2.10" = [ "node201.local" "llng-auth.local" ];
         "192.168.133.163" = [ "node002.local" "swpad.local" "swforum.local" ];
         "192.168.133.225" = [ "node003.local" "pgsql001.local" "swmonitor.local" ];
    };
     nameservers = [ "213.186.33.99" ];
     firewall = {
	 enable = true;
	 allowedTCPPorts = [ 80 ];
     };
     defaultMailServer = {
       directDelivery = true;
       hostName = "submission.meup.it:587";
       useTLS = true;
       useSTARTTLS = true;
       domain = "softwareworkers.it";
       root = "admin@softwareworkers.it";
       authUser = "do.not.reply@softwareworkers.it";
       authPassFile = "/etc/nixos/secrets/defaultMailServerAuthPass";
     };
    };
  };
};
```

1. sync and create container on `node001` applying configuration

1. on `node001` set permissions of `/etc/nixos`:

```
sudo chmod -R g+w /var/lib/containers/<container_name>/etc/nixos
sudo chgrp -R wheel /var/lib/containers/<container_name>/etc/nixos
```

1. (if not already present) add a new [node001 Makefile rule](hosts/node001.meup.it/Makefile) to sync container secrets with `node001` container dir:

```
rsync $(RSYNC_OPTS) ./secrets/containers/<shortname>/ softwareworkers.node001:/var/lib/containers/<container_name>/etc/nixos/secrets
```

### cloud: Nextcloud service

1. add this record to the DNS: `cloud 1800 IN CNAME proxy.meup.it.`

1. create secrets directory (if not present): `hosts/node001.meup.it/secrets/containers/<shortdomain>`

1. create `hosts/node001.meup.it/secrets/containers/<shortdomain>/cloud_adminpass` file, adding a sigle line with a password (use a tool like `pwgen` for strong random password)

1. create `hosts/node001.meup.it/secrets/containers/<shortdomain>/cloud_dbpass` file, adding a sigle line with a password (use a tool like `pwgen` for strong random password)

1. sync container secrets via `make sync`

1. create the user and database on `node003` (via ssh session):

The new created user *must* not be `superuser`, *must* not be allowed to create new databases and *must* not be allowed to create new roles.

```
sudo -u postgres -s createuser --interactive -P cloud<shortdomain>
sudo -u postgres -s createdb -O cloud<shortdomain> cloud<shortdomain> "Nextcloud database for <shortdomain> (<customer>)"
```

1. in `security.acme.certs` in [node001 configuration](./hosts/node001.meup.it/configuration.nix) add a new `extraDomains` record like this `"cloud.<FQDN>" = null;` to the main certificate.

1. add service inside the customer dedicated container in [node001 configuration](./hosts/node001.meup.it/configuration.nix)

```
    environment = {
      systemPackages = with pkgs; [
        inetutils
    	postgresql_11
      ];
    };

    services.nextcloud = {
      enable = true;
	  webfinger = true;
      autoUpdateApps.enable = true;
      nginx.enable = true;
      hostName = "cloud.<FQDN>";
      https = true;
      maxUploadSize = "512M";
      config = {
	     overwriteProtocol = "https";
         dbtype = "pgsql";
         dbhost = "pgsql001.local";
     	 # WARNING!!! Do **not** use underscores like user_name :-@
         dbuser = "cloud<shortdomain>";
         dbname = "cloud<shortdomain>";
     	 dbpassFile = "/etc/nixos/secrets/cloud_dbpass";
         adminuser = "admin";
         adminpassFile = "/etc/nixos/secrets/cloud_adminpass";
      };
    }; # nextcloud

    services.nginx.virtualHosts = {
      "cloud.<FQDN>" = {
        listen = [
          { addr = "0.0.0.0"; port = 80; extraParameters =  [ "proxy_protocol" ]; }
  	      { addr = "[::]"; port = 80; extraParameters =  [ "proxy_protocol" ]; }
        ];
        extraConfig = "set_real_ip_from 192.168.0.0/16;\nreal_ip_header proxy_protocol;";
      };
    };

```

1. Add service to HAProxy *frontend* config

```
acl cloud<Shortdomain>-acl hdr(host) -i cloud.<FQDN>
use_backend cloud<Shortdomain>-backend if cloud<Shortdomain>-acl
```

1. Add service as HAProxy *backend*:

```
backend cloud<Shortdomain>-backend
     mode http
	 server cloud<Shortdomain> <container_name>.containers:80 maxconn 64 send-proxy
```

1. Rebuild `node001` configuration: `make sync` then ssh to `node001` and `sudo nixos-rebuild test` then `sudo nixos-rebuild switch`

1. renew ACME certificates for the domain: `sudo systemctl start acme-<FQDN>`

1. test succesful installation: via web browser go to configured URL

1. on your local machine generate X.509 certificate and key for this instance (in `secrets/containers/<container>/` folder

```
openssl req -nodes -x509 -sha256 -newkey rsa:4096 -keyout "cloud_saml.key" -out "cloud_saml.pem" -days 99999
```

1. configure SAML SP via web interface, see [Nextcloud - SAML configuration](./meup-onboarding/nextcloud-saml.md)

1. add needed LDAP records, see [LDAP - Add new group record for customers](./meup-onboarding/ldap-add.md)

1. add the service SSO via web management interface, see [LemonLDAP::NG - Add services via manager](./meup-onboarding/lldap-web.md) for details; Nextcloud metadata URL is: `https://<FQDN>/apps/user_saml/saml/metadata?idp=1`

1. add proper database indexes:

   a. sudo nixos-container run <container> -- nextcloud-occ db:add-missing-indices
   c. sudo nixos-container run <container> -- nextcloud-occ db:convert-filecache-bigint

#### Troubleshooting

1. first Nextcloud setup of Nix (NixOS ver 18.09) installation scripts fails, in this case on `node001` do:

```
sudo rm /var/lib/containers/<container_name>/var/lib/nextcloud/config/config.php
sudo nixos-container run <container_name> -- systemctl start nextcloud-setup.service
```

### LMS: Moodle service

This guide has been compiled following the [Moodle 3.9 Installation Quick Guide](https://docs.moodle.org/39/en/Installation_quick_guide).

1. add this record to the DNS: `moodle 1800 IN CNAME proxy.meup.it.`

1. create secrets directory (if not present): `hosts/node001.meup.it/secrets/containers/<shortdomain>`

1. create `hosts/node001.meup.it/secrets/containers/<shortdomain>/moodle_dbpass` file, adding a sigle line with a password (use a tool like `pwgen` for strong random password)

1. sync container secrets via `make sync`

1. create the user and database on `node003` (via ssh session):

The new created user *must* not be `superuser`, *must* not be allowed to create new databases and *must* not be allowed to create new roles.

```
sudo -u postgres -s createuser --interactive -P moodle<shortdomain>
sudo -u postgres -s createdb -O moodle<shortdomain> moodle<shortdomain> "Moodle database for <shortdomain> (<customer>)"
```

1. in `security.acme.certs` in [node001 configuration](./hosts/node001.meup.it/configuration.nix) add a new `extraDomains` record like this `"moodle.<FQDN>" = null;` to the main certificate.

1. add service inside the customer dedicated container in [node001 configuration](./hosts/node001.meup.it/configuration.nix)

```
    services.moodle = {
      enable = true;
	  initialPassword = "<use pwgen>";
	  extraConfig = "$CFG->disableupdatenotifications = true;\n";
	  virtualhost = {
	   hostName = "moodle.<FQDN>";
	   adminAddr = "sysadmin@softwareworkers.it";
	  };
      database = {
         createLocally = false;
         type = "pgsql";
         host = "pgsql001.local";
         # WARNING!!! Do **not** use underscores like user_name :-@
         user = "moodle<shortdomain>";
         name = "moodle<shortdomain>";
         passwordFile = "/etc/nixos/secrets/moodle_dbpass";
      };
    }; # moodle

    services.nginx.virtualHosts = {
      "moodle.<FQDN>" = {
        listen = [
          { addr = "0.0.0.0"; port = 80; extraParameters =  [ "proxy_protocol" ]; }
  	      { addr = "[::]"; port = 80; extraParameters =  [ "proxy_protocol" ]; }
        ];
        extraConfig = "set_real_ip_from 192.168.0.0/16;\nreal_ip_header proxy_protocol;";
      };
    };

```

1. Add service to HAProxy *frontend* config

```
acl moodle<Shortdomain>-acl hdr(host) -i moodle.<FQDN>
use_backend moodle<Shortdomain>-backend if moodle<Shortdomain>-acl
```

1. Add service as HAProxy *backend*:

```
backend moodle<Shortdomain>-backend
     mode http
     server moodle<Shortdomain> <container_name>.containers:80 maxconn 64 send-proxy
```

1. Rebuild `node001` configuration: `make sync` then ssh to `node001` and `sudo nixos-rebuild test` then `sudo nixos-rebuild switch`

1. renew ACME certificates for the domain: `sudo systemctl start acme-<FQDN>`

1. test succesful installation: via web browser go to configured URL

FIXME begin

1. on your local machine generate X.509 certificate and key for this instance (in `secrets/containers/<container>/` folder

```
openssl req -nodes -x509 -sha256 -newkey rsa:4096 -keyout "cloud_saml.key" -out "cloud_saml.pem" -days 99999
```

1. configure SAML SP via web interface, see [Nextcloud - SAML configuration](./meup-onboarding/nextcloud-saml.md)

1. add needed LDAP records, see [LDAP - Add new group record for customers](./meup-onboarding/ldap-add.md)

1. add the service SSO via web management interface, see [LemonLDAP::NG - Add services via manager](./meup-onboarding/lldap-web.md) for details; Nextcloud metadata URL is: `https://<FQDN>/apps/user_saml/saml/metadata?idp=1`

FIXME end

### Mailman

This guide has been compiled following the [Configuring Mailman](https://docs.mailman3.org/projects/mailman/en/latest/src/mailman/config/docs/config.html#) **and** [Mailman Web](https://mailman-web.readthedocs.io/en/latest/index.html) documentation.

#### Email configuration

Mailman needs an already configured email server with POP3 or IMAP for incoming email and SMTP services. 

In the email server we should reserve a full subdomain named `lists.FQDN` for all the mailing lists we want manage via Mailman.

On the email server create the following mailbox (or aliase) reserved to the mailman site owner:

```
siteowner@lists.<FQDN>"
```

For **each** mailing list named `<listname>`, please create the following mailboxes:

```
<listname>@lists.<FQDN>
<listname>-bounces@lists.<FQDN>
<listname>-confirm@lists.<FQDN>
<listname>-join@lists.<FQDN>
<listname>-leave@lists.<FQDN>
<listname>-owner@lists.<FQDN>
<listname>-request@lists.<FQDN>
<listname>-subscribe@lists.<FQDN>
<listname>-unsubscribe@lists.<FQDN>
```

Obviously, you must provide the password to us to be able to access the mailboxes and to use the SMTP service.

#### Service configuration

1. add this record to the DNS: `lists 1800 IN CNAME proxy.meup.it.`

1. create secrets directory (if not present): `hosts/node001.meup.it/secrets/containers/<shortdomain>`

1. create `hosts/node001.meup.it/secrets/containers/<shortdomain>/fetchmailrc` FIXME

1. create `hosts/node001.meup.it/secrets/containers/<shortdomain>/postfix-sasl-passwords` FIXME

1. sync container secrets via `make sync`

1. create the users and databases on `node003` (via ssh session):

The new created user *must* not be `superuser`, *must* not be allowed to create new databases and *must* not be allowed to create new roles.

```
sudo -u postgres -s createuser --interactive -P mailman<shortdomain>
sudo -u postgres -s createdb -O mailman<shortdomain> mailman<shortdomain> "Mailman database for <shortdomain> (<customer>)"
sudo -u postgres -s createdb -O mailman<shortdomain> mailmanweb<shortdomain> "Mailman web service database for <shortdomain> (<customer>)"
```

1. in `security.acme.certs` in [node001 configuration](./hosts/node001.meup.it/configuration.nix) add a new `extraDomains` record like this `"moodle.<FQDN>" = null;` to the main certificate.

1. add service inside the customer dedicated container in [node001 configuration](./hosts/node001.meup.it/configuration.nix)

```
    services.moodle = {
      enable = true;
	  initialPassword = "<use pwgen>";
	  extraConfig = "$CFG->disableupdatenotifications = true;\n";
	  virtualhost = {
	   hostName = "moodle.<FQDN>";
	   adminAddr = "sysadmin@softwareworkers.it";
	  };
      database = {
         createLocally = false;
         type = "pgsql";
         host = "pgsql001.local";
         # WARNING!!! Do **not** use underscores like user_name :-@
         user = "moodle<shortdomain>";
         name = "moodle<shortdomain>";
         passwordFile = "/etc/nixos/secrets/moodle_dbpass";
      };
    }; # moodle

    services.nginx.virtualHosts = {
      "moodle.<FQDN>" = {
        listen = [
          { addr = "0.0.0.0"; port = 80; extraParameters =  [ "proxy_protocol" ]; }
  	      { addr = "[::]"; port = 80; extraParameters =  [ "proxy_protocol" ]; }
        ];
        extraConfig = "set_real_ip_from 192.168.0.0/16;\nreal_ip_header proxy_protocol;";
      };
    };

```

1. Add service to HAProxy *frontend* config

```
acl moodle<Shortdomain>-acl hdr(host) -i moodle.<FQDN>
use_backend moodle<Shortdomain>-backend if moodle<Shortdomain>-acl
```

1. Add service as HAProxy *backend*:

```
backend moodle<Shortdomain>-backend
     mode http
     server moodle<Shortdomain> <container_name>.containers:80 maxconn 64 send-proxy
```

1. Rebuild `node001` configuration: `make sync` then ssh to `node001` and `sudo nixos-rebuild test` then `sudo nixos-rebuild switch`

1. renew ACME certificates for the domain: `sudo systemctl start acme-<FQDN>`

1. test succesful installation: via web browser go to configured URL


## Services in Docker containers

Some services - specially web services - still not available in NixOS or in Guix System are deployed using Docker containers.

`dockerd` service and related tools are installed on `node003` Guix System.

To instantiate the containers we use the standard [docker compose](https://docs.docker.com/compose/compose-file/) tool, configured in [docker-compose.yml](./hosts/node003.meup.it/config/docker-compose.yml).

### pad: CodiMD service

1. configure parameters in `hosts/node003.meup.it/config/dockerfiles/secrets/codimd-<shortname>.env`:

```
CMD_DOMAIN=pad.<shortname>.it
CMD_SESSION_SECRET=<random secret (use 'pwgen 33')>
CMD_IMAGE_UPLOAD_TYPE=filesystem
CMD_DB_URL=postgres://cmd_<shortname>:<password>@pgsql001.local:5432/cmd_<shortname>
CMD_SAML_ISSUER=https://pad.<shortname>.it/saml
CMD_SAML_IDPSSOURL=https://auth.meup.io/saml/singleSignOn
CMD_SAML_IDPCERT=/mnt/saml.pem
CMD_SAML_ATTRIBUTE_ID=uid
CMD_SAML_ATTRIBUTE_USERNAME=cn
CMD_SAML_ATTRIBUTE_EMAIL=mail
```

`CMD_DB_URL` is formatted this way: `<databasetype>://<username>:<password>@<hostname>/<database>`

1. create the database on `psql.local` (database host aka `node003`)

```
sudo -u postgres -s createuser --interactive -P cmd_<shortname>
sudo -u postgres -s createdb -O cmd_<shortname> cmd_<shortname> "CodiMD database for <Shortname> (<customer>)"
```

1. add this subsection to the `services` section in [docker-compose.yml](./hosts/node003.meup.it/config/docker-compose.yml)

```
  <shortname>_codimd:
    container_name: <shortname>_codimd
    build:
      context: ./dockerfiles/codimd
      dockerfile: codimd-alpine
      args:
        - "VERSION=master"
        - "CODIMD_REPOSITORY=https://github.com/codimd/server.git"
    image: codimd-alpine:master
    volumes:
      - <shortname>_codimd_uploads:/codimd/public/uploads
      - ./dockerfiles/secrets/mnt:/mnt/:ro
    env_file:
      - dockerfiles/codimd/codimd-default.env
      - dockerfiles/secrets/codimd-<shortname>.env
    ports:
      - target: 3000
        published: <ext_port>
    extra_hosts:
      - "pgsql001.local:192.168.133.225"
    restart: always
    labels:
      it.<shortname>.description: "Pad di <Shortname> (<customer>)"
      it.<shortname>.department: "MeUp! Infrastructure"
```

1. add volume to `volumes` section in [docker-compose.yml](./hosts/node003.meup.it/config/docker-compose.yml):

```
  <shortname>_codimd_uploads:
    name: <shortname>_codimd_uploads
```

1. syncronize config with `node003`: `make sync`

1. check configuration: `docker-compose config`

1. apply configuration: `docker-compose up -d --remove-orphans`

1. add this record to the DNS: `pad 1800 IN CNAME proxy.meup.it.`

1. if a dedicated section in `security.acme.certs` in [node001 configuration](./hosts/node001.meup.it/configuration.nix) for the FQDN is not defined, define it; if a section is already defined, just add a new `extraDomains` record like this `"pad.<FQDN>" = null;`

1. add a new entry in `networking.hosts` (on `node001`): "192.168.133.225" = [ "node003.local" "pgsql001.local" "swmonitor.local" <...> pad.`<shortname>`.local" ];

1. Add service to HAProxy *frontend* config

```
acl pad<Shortdomain>-acl hdr(host) -i pad.<FQDN>
use_backend pad<Shortdomain>-backend if pad<Shortdomain>-acl
```

1. Add service as HAProxy *backend*:

```
backend pad<Shortdomain>-backend
     mode http
	 server pad<Shortdomain> pad.<shortname>.local:<ext_port> maxconn 64
```

1. Rebuild `node001` configuration: `make sync` then ssh to `node001` and `sudo nixos-rebuild test` then `sudo nixos-rebuild switch`

1. renew ACME certificates for the domain: `sudo systemctl start acme-<FQDN>`

1. add needed LDAP records, see [LDAP - Add new group record for customers](./meup-onboarding/ldap-add.md)

1. add the service SSO via web management interface, see [LemonLDAP::NG - Add services via manager](./meup-onboarding/lldap-web.md) for details; CodiMD SAML metadata URL is `https://{{FQDN}}/auth/saml/metadata`

### forum. Discourse service

1. create the user and database on `node003` (via ssh session):

The new created user *must* not be `superuser`, *must* not be allowed to create new databases and *must* not be allowed to create new roles.

The new created database *must* have the following extensions: `hstore`, `pg_trgm`; they *must* be installed before Discourse instantiation.

```
sudo -u postgres -s createuser --interactive -P discourse_<shortdomain>
sudo -u postgres -s createdb -O discourse_<shortdomain> discourse_<shortdomain> "Discourse database for <shortdomain> (<customer>)"
sudo -u postgres -s psql discourse_<shortdomain> -c 'CREATE EXTENSION IF NOT EXISTS hstore;'
sudo -u postgres -s psql discourse_<shortdomain> -c 'CREATE EXTENSION IF NOT EXISTS pg_trgm;'
```

1. in `security.acme.certs` in [node001 configuration](./hosts/node001.meup.it/configuration.nix) add a new `extraDomains` record like this `"forum.<FQDN>" = null;` to the main certificate.

1. on your local machine generate X.509 certificate and key for this instance and copy them in `config/dockerfiles/secrets/` folder

```
openssl req -nodes -x509 -sha256 -newkey rsa:4096 -keyout "<FQDN>_saml.key" -out "<FQDN>_saml.pem" -days 99999
```

1. add a new YAML container declaration in [docker containres directory](hosts/node003.meup.it/config/dockerfiles/discourse_docker_containers/); in filename `<FQDN>.yml` (will be also used as container name):

```
templates:
  - "templates/redis.template.yml"
  - "templates/web.template.yml"
  - "templates/web.ratelimited.template.yml"
  # Nginx configs for HAProxy compatibility
  - "templates/web.proxyprotocol.template.yml"
expose:
  - "<external_port>:80"
params:
  db_default_text_search_config: "pg_catalog.english"
  db_shared_buffers: "256MB"
  version: v2.4.3
env:
  DISCOURSE_HOSTNAME: "<FQDN>"
  DISCOURSE_FORCE_HTTPS: true
  DOCKER_USE_HOSTNAME: true
  # Language and locale
  LANG: "it_IT.UTF-8"
  DISCOURSE_DEFAULT_LOCALE: "it"
  # Unicorn config
  UNICORN_WORKERS: 4

  ## List of comma delimited emails that will be made admin and developer
  ## on initial signup example 'user1@example.com,user2@example.com'
  DISCOURSE_DEVELOPER_EMAILS: '<user1 SAML email>,<user2 SAML email>'

  ## The SMTP mail server used to validate new accounts and send notifications
  # WARNING the char '#' in SMTP password can cause problems!
  DISCOURSE_SMTP_ADDRESS: "<SMTP FQDN>"
  DISCOURSE_SMTP_PORT: "<SMTP port>"
  DISCOURSE_SMTP_USER_NAME: "<SMTP username>"
  DISCOURSE_SMTP_PASSWORD: "<SMTP password>"

  ## PostgreSQL database configurarion
  #DISCOURSE_DB_HOST: pgsql001.local # We have to find a way to define hosts
  DISCOURSE_DB_HOST: "192.168.133.225"
  DISCOURSE_DB_NAME: "discourse_<shortname>"
  DISCOURSE_DB_USERNAME: "discourse_<shortname>"
  DISCOURSE_DB_PASSWORD: "<DB passwd>"

  ## SAML IDP and "static" settings
  DISCOURSE_SAML_DEBUG_AUTH: false
  DISCOURSE_SAML_LOG_AUTH: true
  DISCOURSE_SAML_AUTO_CREATE_ACCOUNT: true
  DISCOURSE_SAML_ATTRIBUTE_STATEMENTS: "{:name => ['screenName']}"
  DISCOURSE_SAML_SYNC_EMAIL: true
  DISCOURSE_SAML_FULL_SCREEN_LOGIN: true
  DISCOURSE_SAML_TITLE: "Auth MeUp!"
  DISCOURSE_SAML_TARGET_URL: "https://auth.meup.io/saml/singleSignOn"
  DISCOURSE_SAML_SLO_TARGET_URL: "https://auth.meup.io/saml/proxySingleLogout"
  DISCOURSE_SAML_AUTHN_REQUESTS_SIGNED: true
  DISCOURSE_SAML_CERT: "-----BEGIN CERTIFICATE-----
MIIGJTCCBA2gAwIBAgIUaX2vDwV4JSjNFJs+wuWKOD0vTQIwDQYJKoZIhvcNAQEL
BQAwgaExCzAJBgNVBAYTAklUMQswCQYDVQQIDAJNSTEOMAwGA1UEBwwFTWlsYW4x
HDAaBgNVBAoME1NvZnR3YXJld29ya2VycyBzcmwxFDASBgNVBAsMC0ludGVybmFs
IENBMRUwEwYDVQQDDAxhdXRoLm1ldXAuaW8xKjAoBgkqhkiG9w0BCQEWG3N5c2Fk
bWluQHNvZnR3YXJld29ya2Vycy5pdDAeFw0xOTExMjIwOTIyMzJaFw0yOTExMTkw
OTIyMzJaMIGhMQswCQYDVQQGEwJJVDELMAkGA1UECAwCTUkxDjAMBgNVBAcMBU1p
bGFuMRwwGgYDVQQKDBNTb2Z0d2FyZXdvcmtlcnMgc3JsMRQwEgYDVQQLDAtJbnRl
cm5hbCBDQTEVMBMGA1UEAwwMYXV0aC5tZXVwLmlvMSowKAYJKoZIhvcNAQkBFhtz
eXNhZG1pbkBzb2Z0d2FyZXdvcmtlcnMuaXQwggIiMA0GCSqGSIb3DQEBAQUAA4IC
DwAwggIKAoICAQC69qFuXMJb5AU7ofkxEj4qYJJexHe0i43LSN/50Zy7h0EWW6Mv
pIs1ZV5ol6wJiczXsbxFeg6fUwXpGbUYlJQMK3EF5oxI9UNxFUI7NBhY/CoezV8x
h5E0Vtl4Xen9v/oDgMhXQiQpzugA/+gyHRLCdPcyv0YyAhHU1IBdsQ0cRoO/FWsT
+IGZs7ZII5GjebkE5pR07sj6o+gx0I6zX3zpHkSFreIJievVq0YsdVQW9lBe82U7
SlszxPB9FIoIlpOvxH8K4fF4ih8ANIdyfebZPyQZAAS+/YbWQxdOpqmB9gBr/acn
pG8+d0uhGAfeXw2tgzSlfGXDAXkWiZhPpAsWoT9/61y3JB6VgE+XBhIQBYPV6tqt
SGS1BmHrkVASpnUIlwcGQYS4xf39j9kNmCImbVMVFW0w49LCRCXoOvk3gw/BCcUA
qOnVoQ/ZhwWwoHhCD/3bK38abLFWwXusrnVbYLVcm0nUndQOiB6uO6XNrsoqAklV
P0jb+liZ5aNTCzHrT8VREaBjZ79pW/nfmLD6xgRaNUZ4fXhUAc5aGgrqz/AFDUmm
iPuQadO8wPEpMzx1EhvhQsk5tE79v2aR+QNJ95hmuTlp3XvOiD6X5gwgPHNBTD6P
HOZg1xO4FP3jHJ0JgOz3zfduHb+W/Yoxm849aLBDM0U06aM9oUS3IDBiqwIDAQAB
o1MwUTAdBgNVHQ4EFgQUj8Nwaqx0rZwKEoD07Ur75sJIiCwwHwYDVR0jBBgwFoAU
j8Nwaqx0rZwKEoD07Ur75sJIiCwwDwYDVR0TAQH/BAUwAwEB/zANBgkqhkiG9w0B
AQsFAAOCAgEAs35tHWWdBfY0vV0X1ikbERWFFdKW/bzegwskY3wTkuuxFe7LL3x6
Rw3oHPujvbCQkEokXvDE0yKLGLCNvmd5l+3+TRQEv0Cv/RNxOgRIlcGwI3Qsf67k
EV9jdpqnD+usKZcqw1JNT0K4gm8pZlj4hBsPFXzgUpAOTSfRjfYdBFyXG1GXSNb2
sNUVLmnDXfTXskt19lUVyWCGcuiqbLn89qQrc3MWJ81+b2wc7S601ZJshcJI5Noq
h2bSsmCBjoIf48Ezlc+NVGio9SjgnMH6Sv6ZIkQZtkvLyLWVwc9w639KQdet+ygI
ZyEin9bkWOJDsxzAI9E28YjfmXJn3R96qZd7pJgewm8NlnG/y3KBgq9KhD8E17ji
Y9Mp1SrTDiaMsNY1xdhsWAh4u8Xgu9cWLxJr/2/33eVeGVFzEa+II178YN+2aHib
dSkSNMm/wNSf+EO+fp0uGy0KNbgX3n2bwWaKEq3liXmAzZi4bgHkjS6WXmNMTQBT
SZa9Yh1Qtfy1RxxUIxZLvLitxX1O+STIVtrxIS9cs3a77Jz4G+s7n7IN4hrveiJS
gg9RpA1H3rBltSNoGn0ww7BPz5IiGvxYJgMHXIoQ2AMpnVpuD6owdClLZ0CMAsM1
DpKBXSi/gWSt2pYjzmxt1Jd7S9glBT8uSIO6f2MfbTK7bpuWytojzig=
-----END CERTIFICATE-----"
  ## SAML SP settings: CUSTOMIZE
  DISCOURSE_SAML_SP_CERTIFICATE: "<copy from <FQDN>_saml.pem>
  "
  DISCOURSE_SAML_SP_PRIVATE_KEY: "<copy from <FQDN>_saml.key>
  "

volumes:
  - volume:
      host: /var/lib/discourse/<FQDN>/
      guest: /shared
  - volume:
      host: /var/lib/discourse/<FQDN>/var-log
      guest: /var/log

## Plugins go here
## see https://meta.discourse.org/t/19157 for details
hooks:
  after_code:
    - exec:
        cd: $home/plugins
        cmd:
          - git clone https://github.com/discourse/discourse-saml.git
```

You can find [poor documentation on SAML parameters](https://github.com/discourse/discourse-saml/blob/master/README.md) or better read [the SAML auth code](https://github.com/discourse/discourse-saml/blob/master/lib/saml_authenticator.rb), Discourse uses the [SAML Omniauth Ruby lib](https://github.com/omniauth/omniauth-saml).

1. Symlink container configuration in `<root>/dockerfiles/discourse_docker/containers`

1. Syncronize config with `node003`: `make sync`

1. Add host alias `forum.<shortname>.local` to `hosts` on `node001`

1. Add service to HAProxy *frontend* config

```
acl forum<Shortdomain>-acl hdr(host) -i forum.<FQDN>
use_backend forum<Shortdomain>-backend if forum<Shortdomain>-acl
```

1. Add service as HAProxy *backend*:

```
backend forum<Shortdomain>-backend
     mode http
	 server forum<Shortdomain> forum.<shortname>.local:<ext_port> maxconn 64 send-proxy
```

1. Rebuild `node001` configuration: `make sync` then ssh to `node001` and `sudo nixos-rebuild test` then `sudo nixos-rebuild switch`

1. renew ACME certificates for the domain: `sudo systemctl start acme-<FQDN>`

1. Instantiate Docker container on `node003`: `@node003 ~/maintenance/dockerfiles/discourse_docker$ time ./launcher rebuild <container>`

1. create an admin account from console, on `node003`:

   a. enter the container: `docker container exec -it <container> /bin/bash`

   b. create admin account: `rake admin:create`

1. add needed LDAP records, see [LDAP - Add new group record for customers](./meup-onboarding/ldap-add.md)

1. add the service SSO via web management interface, see [LemonLDAP::NG - Add services via manager](./meup-onboarding/lldap-web.md) for details; Discourse metadata URL is `https://{{FQDN}}/auth/saml/metadata`

#### Troubleshooting

To watch live what is happening, see logs in container: `tail -f shared/log/rails/production.log`.  Log files are also available in `/var/lib/discourse/<FQDN>/log/`.
