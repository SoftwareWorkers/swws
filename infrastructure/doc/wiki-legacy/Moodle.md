Moodle is not present today in [NixOS](https://nixos.org/nixos/packages.html#moodle) nor in [Debian](https://packages.debian.org/search?keywords=moodle&searchon=names&suite=all&section=all).  
There is a Docker version https://github.com/docker-scripts/moodle  

We'll use this way:  https://docs.moodle.org/37/en/Installing_Moodle  

installation on Debian burst (because we need php 7.3)
https://cdimage.debian.org/cdimage/buster_di_rc1/amd64/iso-cd/debian-buster-DI-rc1-amd64-netinst.iso  

`apt install git apache2 postgresql php php-intl php-curl php-zip php-pgsql php-xml php-gd php-mbstring php-xmlrpc php-soap  `

`cd /var/www/html/  `
git clone -b MOODLE_{{Version3}}_STABLE git://git.moodle.org/moodle.git --single-branch   
if in trouble see https://docs.moodle.org/37/en/Git_for_Administrators  
or try `git clone -b MOODLE_37_STABLE git://git.moodle.org/moodle.git --single-branch  `

`chown -R root /var/www/html/moodle  `  
`chmod -R 0755 /var/www/html/moodle  `  
`find /var/www/html/moodle -type f -exec chmod 0644 {} \;  `  
`mkdir /var/www/moodledata  `  
`chmod a+w /var/www/moodledata  `  


# create database 


`su - postgres  `  
`psql`  


postgres=# `CREATE USER moodleuser WITH PASSWORD 'yourpassword';  `  
to change the password `ALTER ROLE moodleuser WITH PASSWORD 'secret';`  

postgres=# `CREATE DATABASE moodle WITH OWNER moodleuser;  `  

FOR LOCALIZATION ONLY: postgres=# `CREATE DATABASE moodle WITH OWNER moodleuser ENCODING 'UTF8' LC_COLLATE='it_IT.utf8' LC_CTYPE='it_IT.utf8' TEMPLATE=template0;  `  

exit  `\q`  

`vi /etc/postgresql/11/main/pg_hba.conf  `  
add this as first line  
`local moodle moodleuser password`  

`vi  /var/www/html/moodle/config.php  `  

if it doesn't exist copy it  
`cp /var/www/html/moodle/config-dist.php /var/www/html/moodle/config.php  `  

verify  
dbtype pgsql  
dbhost localhost  
dbname moodle  
dbuser moodleuser  
dbpass password  
dbsocket true  
wwwroot http://site.xx/moodle  
dataroot /var/www/moodledata/  

# restart web & DB server
`systemctl restart postgresql  `  
`systemctl restart apache2  `  


go to http://yourwebserver.com/moodle and follow the instructions:  
or run: `php /var/www/html/moodle/admin/cli/install.php  `  
port=5432  
unix socket=/var/run/postgresql  

# cron
`crontab -u www-data -e`  
`* * * * * /usr/bin/php  /path/to/moodle/admin/cli/cron.php >/dev/null`  

# email
set parameter here:  
http://server.xx/moodle/admin/category.php?category=email  
http://server.xx/moodle/admin/settings.php?section=messageinbound_mailsettings  
and test it:  
http://server.xx/moodle/admin/testoutgoingmailconf.php  

# SSO
https://docs.moodle.org/37/en/Shibboleth  


# Reference:  
https://docs.moodle.org/37/en/Installing_Moodle_on_Debian_based_distributions (out of date)  
https://docs.moodle.org/37/en/Installing_Moodle  
https://docs.moodle.org/37/en/PostgreSQL  
https://docs.moodle.org/37/en/Cron  
https://docs.moodle.org/37/en/Messaging_settings  

