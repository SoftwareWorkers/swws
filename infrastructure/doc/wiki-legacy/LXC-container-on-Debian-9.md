# LXC container on Debian 9 #

apt-get install lxc libvirt0 libpam-cgroup libpam-cgfs bridge-utils sudo  

mkdir .config/lxc  

cp /etc/lxc/default.conf .config/lxc/  

less /etc/subuid  

less /etc/subgid  

vi .config/lxc/default.conf  

append this 2 lines:  

lxc.idmap = u 0 100000 65536  

lxc.idmap = g 0 100000 65536  

adduser lxc_user sudo (usermod -a -G sudo lxc_user)  


#### “host-shared bridge” ####

sudo vi /etc/lxc/lxc-usernet  

append this line:  

your-username veth lxcbr0 10  

sudo vi /etc/network/interfaces  

auto br0  

iface br0 inet dhcp  

bridge_ports ens3  #?? eth0  

bridge_fd 0  

bridge_maxwait 0  

auto lxcbr0  

iface lxcbr0 inet dhcp  

bridge_ports ens3   #?? eth0  

bridge_fd 0  

bridge_maxwait 0  

systemctl restart networking.service (/etc/init.d/networking restart) #riavvia la rete  

### container ###

sudo lxc-create -t download -n container_name  

answer: debian, stretch, amd64  

sudo lxc-ls  
sudo lxc-info -n container_name  
sudo lxc-start --name container_name -d  

sudo lxc-attach -n container_name #to login the first time into container because root has no password  
	passwd #set the root password  
ctrl-a ctrl-a ctrl-a q # to exit  


vi /var/lib/lxc/containername/config #config rete  
append this line:  
##Network  
lxc.network.type = veth  
lxc.network.flags = up  

#that's the interface defined above in host's interfaces file  
lxc.network.link = br0  

#name of network device inside the container,  
#defaults to eth0, you could choose a name freely  
#lxc.network.name = lxcnet0   

#lxc.network.hwaddr = 00:FF:AA:00:00:01  

#the ip may be set to 0.0.0.0/24 or skip this line  
#if you like to use a dhcp client inside the container  
#lxc.network.ipv4 = 192.168.1.110/24  

#define a gateway to have access to the internet  
#lxc.network.ipv4.gateway = 192.168.1.1  

vi /etc/network/interfaces  
auto eth0  
iface eth0 inet dhcp  
#iface eth0 inet static  
#address <container IP here, e.g. 192.168.1.110>  
#all other settings like those for the host  

systemctl restart networking.service  
update-initramfs -u   #per la rete??????????  



sudo lxc-console -n container_name #to login into container  
apt install openssh-server #to login with ssh  
ctrl-a ctrl-a ctrl-a q # to exit  

lxc-stop -n container_name  
lxc-checkconfig  
lxc-destroy -n container_name  

### reference ###
https://wiki.debian.org/LXC  
https://wiki.debian.org/LXC/SimpleBridge  
https://linuxcontainers.org/lxc/getting-started/  