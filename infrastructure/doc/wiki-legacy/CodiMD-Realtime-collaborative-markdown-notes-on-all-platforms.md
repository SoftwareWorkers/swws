# CodiMD - Realtime collaborative markdown notes on all platforms. #

# Docker installation #

Debian 9 on SWhost:
```
su -  
apt install git curl apt-transport-https docker-compose  
curl -fsSL https://download.docker.com/linux/debian/gpg | apt-key add -  
vi /etc/apt/source.list  
add this line: deb https://download.docker.com/linux/debian stretch stable  
apt update  
apt install docker-ce docker-ce-cli containerd.io  

mkdir -p /home/user/docker  
cd /home/user/docker  
git clone https://github.com/codimd/container.git codimd-container  
cd codimd-container  
vi docker-compose.yml  
change version from 3 to 2  
uncomment the lines  
build:
  context: .  
  dockerfile: debian/Dockerfile  
  args:  
    - "VERSION=master"  
    - "CODIMD_REPOSITORY=https://github.com/codimd/server.git"  

change *dockerfile: debian/Dockerfile* in *dockerfile: alpine/Dockerfile*  

comment this line  
#    image: quay.io/codimd/server:1.3.2  

change hackmdpass in two lines  
POSTGRES_PASSWORD=***  
CMD_DB_URL=postgres://hackmd:*****  

insert under enviroment:  
CMD_ALLOW_ANONYMOUS=false  
CMD_ALLOW_EMAIL_REGISTER=false  

docker-compose up -d  
```
# manage users #
```
docker ps -a  
docker exec -it containerID /bin/sh  
bin/manage_users  --add pippo@xyz.com  
```

# test #
browse  http://localhost:3000  


# useful commands: #

```
docker-compose stop  
docker-compose rm  
docker volume ls  
docker volume rm xyz  
docker ps -a  
docker stop  
docker rm  
docker container logs -f  
docker container stats  

docker --version  
Docker version 18.09.5, build e8ff056dbc  

docker-compose --version  
docker-compose version 1.8.0, build unknown  

docker version  
Client:  
 Version:           18.09.5  
 API version:       1.39  
 Go version:        go1.10.8  
 Git commit:        e8ff056dbc  
 Built:             Thu Apr 11 04:44:28 2019  
 OS/Arch:           linux/amd64  
 Experimental:      false  

Server: Docker Engine - Community  
 Engine:  
  Version:          18.09.5  
  API version:      1.39 (minimum version 1.12)  
  Go version:       go1.10.8  
  Git commit:       e8ff056  
  Built:            Thu Apr 11 04:10:53 2019  
  OS/Arch:          linux/amd64  
  Experimental:     false  

docker ps  
CONTAINER ID        IMAGE                                COMMAND                  CREATED             STATUS              PORTS                    NAMES  
a165731a2e19        quay.io/codimd/server:1.3.2-alpine   "/usr/local/bin/dock…"   2 days ago          Up About an hour    0.0.0.0:3000->3000/tcp   codimdcontainer_app_1  
9b22c58b568b        postgres:9.6-alpine                  "docker-entrypoint.s…"   2 days ago          Up About an hour    5432/tcp                 codimdcontainer_database_1  

ps aux | grep docker  
root      3094  0.0  3.3 694744 66172 ?        Ssl  10:48   0:01 /usr/bin/dockerd -H fd:// --containerd=/run/containerd/containerd.sock  
root      3258  0.0  0.1  12996  3660 ?        Sl   10:48   0:00 /usr/bin/docker-proxy -proto tcp -host-ip 0.0.0.0 -host-port 3000 -container-ip 172.18.0.2 -container-port 3000  
root      3265  0.0  0.2  10728  5656 ?        Sl   10:48   0:00 containerd-shim -namespace moby -workdir /var/lib/containerd/io.containerd.runtime.v1.linux  /moby/a165731a2e196351613fa0750a1203422c1450b9f0514927cb27459629f46ca7 -address /run/containerd/containerd.sock -containerd-binary /usr/bin/containerd -runtime-root /var/run/docker/runtime-runc  
root      3288  0.0  0.2   9320  5016 ?        Sl   10:48   0:00 containerd-shim -namespace moby -workdir /var/lib/containerd/io.containerd.runtime.v1.linux  /moby/9b22c58b568bf5663772d3f718c7c04d1442d65212ba94c05810fc39cc5cf158 -address /run/containerd/containerd.sock -containerd-binary /usr/bin/containerd -runtime-root /var/run/docker/runtime-runc  

ps aux | grep codimd  
root      3493  0.0  1.8 608400 37932 ?        Sl   10:48   0:00 /usr/local/bin/node /codimd/lib/workers/dmpWorker.js  

```


note that database is located here: /var/lib/docker/volumes/codimd_container_database/_data  

reference:  
https://github.com/codimd/container  
https://docs.docker.com/install/linux/docker-ce/debian/  




----------------------------------------------
----------------------------------------------
----------------------------------------------
----------------------------------------------
----------------------------------------------
----------------------------------------------
this was an attempt to manual install (failed)
----------------------------------------------
prepare a Debian host with a LXC container  https://gitlab.com/softwareworkers/infrastructure/wikis/LXC-container-on-Debian-9  

node.js version in Debian stretch is 4.8 so we need a backports to have the >6.x version required  
config backports  https://www.debian.org/doc/manuals/debian-reference/ch02.it.html#_updates_and_backports  

apt install -t stretch-backports nodejs npm yarn  
apt install sqlite libssl-dev wget zip git

mkdir /opt/codimd  
cd /opt/codimd  
wget https://github.com/codimd/server/archive/master.zip  
unzip master.zip  
cd server-master  
bin/setup  

# errore, come lo risolvo? #
root@codi:/opt/codimd/server-master# bin/setup  
copy config files  
install packages  
Usage: yarn [options]  

yarn: error: no such option: --pure-lockfile  
root@codi:/opt/codimd/server-master#   

# yarn è in conflitto con cmdtest #
https://github.com/yarnpkg/yarn/issues/2821#issuecomment-284181365  

# installare YARN da testing:  https://packages.debian.org/buster/yarnpkg #

vi /etc/source.list  
deb http://deb.debian.org/debian/ testing main  
apt update  
apt install -t testing yarnpkg  
mv /usr/bin/yarnpkg /usr/bin/yarn  


# rilancio il setup #
bin/setup  

Error: https://registry.yarnpkg.com/js-sequence-diagrams/-/js-sequence-diagrams-1000000.0.6.tgz: Request failed "404 Not Found"  

from package.json remove de line:  
"js-sequence-diagrams": "^1000000.0.6",  

bin/setup 

modify this file:  
config.json  
.sequelizerc  
  
with this info:  https://github.com/codimd/server/blob/master/docs/configuration-config-file.md  
  


###reference:###

https://github.com/codimd/server/blob/master/docs/setup/manual-setup.md  
https://www.debian.org/doc/manuals/debian-reference/ch02.it.html#_updates_and_backports  




######################################################################################################  
    Download a release and unzip or clone into a directory  
    Enter the directory and type bin/setup, which will install npm dependencies and create configs. The setup script is written in Bash, you would need bash as a prerequisite.  
    Setup the configs, see more below  
    Setup environment variables which will overwrite the configs  
    Build front-end bundle by npm run build (use npm run dev if you are in development)  
    Modify the file named .sequelizerc, change the value of the variable url with your db connection string For example: postgres://username:password@localhost:5432/codimd  
    Run node_modules/.bin/sequelize db:migrate, this step will migrate your db to the latest schema
    Run the server as you like (node, forever, pm2)  

