# how to install GUIX on Debian9 in virtual machine

## step by step commands


wget https://alpha.gnu.org/gnu/guix/guix-binary-0.16.0.x86_64-linux.tar.xz

wget https://alpha.gnu.org/gnu/guix/guix-binary-0.16.0.x86_64-linux.tar.xz.sig

gpg --verify guix-binary-0.16.0.x86_64-linux.tar.xz.sig


gpg --keyserver pool.sks-keyservers.net --recv-keys 3CE464558A84FDC69DB40CFB090B11993D9AEBB5

su -

cd /tmp

tar --warning=no-timestamp -xf guix-binary-0.16.0.system.tar.xz

mv var/guix /var/ && mv gnu /

mkdir -p ~root/.config/guix

ln -sf /var/guix/profiles/per-user/root/current-guix  ~root/.config/guix/current

GUIX_PROFILE="`echo ~root`/.config/guix/current" ; source $GUIX_PROFILE/etc/profile

cp ~root/.config/guix/current/lib/systemd/system/guix-daemon.service /etc/systemd/system/

systemctl start guix-daemon && systemctl enable guix-daemon

mkdir -p /usr/local/bin

cd /usr/local/bin

ln -s /var/guix/profiles/per-user/root/current-guix/bin/guix

mkdir -p /usr/local/share/info

cd /usr/local/share/info

for i in /var/guix/profiles/per-user/root/current-guix/share/info/* ;
  do ln -s $i ; done


## create users and set locale

guix archive --authorize < ~root/.config/guix/current/share/guix/ci.guix.info.pub

https://www.gnu.org/software/guix/manual/en/html_node/Binary-Installation.html#Binary-Installation

groupadd --system guixbuild

for i in `seq -w 1 10`;

  do

    useradd -g guixbuild -G guixbuild           \

            -d /var/empty -s `which nologin`    \

            -c "Guix build user $i" --system    \

            guixbuilder$i;

  done

https://www.gnu.org/software/guix/manual/en/html_node/Build-Environment-Setup.html#Build-Environment-Setup



guix package -i glibc-locales

export GUIX_LOCPATH=$HOME/.guix-profile/lib/locale

https://www.gnu.org/software/guix/manual/en/html_node/Application-Setup.html#Application-Setup


## change tu a normal user 

export PATH="/home/normaluser/.guix-profile/bin${PATH:+:}$PATH"

## verify the installation installing and running the hello program

guix package -i hello

hello

## you will see "Hello, world!" as a result, congratulation!!



