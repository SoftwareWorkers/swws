how to create web pages on Gitlab in a fast and simple manner
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

install Hugo on your pc using the installer of your distribution or follow the instruction on https://gohugo.io/getting-started/installing/#install-hugo-from-tarball

on your pc
~~~~~~~~~~

hugo new site website

cd website/themes

$ git clone https://github.com/hotzeplotz/osprey.git

vi ~/website/config.toml change the default field

[source, toml]
----
baseURL = "https://www.example.com"
languageCode = "en-US"
title = "something title"
theme = "osprey"

[Params]
  tagline = "write some text"
  author = "name surname"

[[menu.main]]
  name = "About"
  url  = "/#about"
  weight = 1
[[menu.main]]
  name = "Roadmap"
  url  = "/#features"
  weight = 2
----

hugo new about.md

vi content/about.md

hugo new features.md

vi content/features.md

hugo

hugo server

to see the result browse http://localhost:1313

on Gitlab
~~~~~~~~~

create a new project called website an inside it create the directory public

create a new file .gitlab-ci.yml

[source, yaml]
----
image: monachus/hugo

variables:
  GIT_SUBMODULE_STRATEGY: recursive

pages:
  script:
  - hugo
  artifacts:
    paths:
    - public
  only:
  - master
----

to see it running go to CI/CD > Pipelines


on your pc
~~~~~~~~~~

cd public

git add .

git commit -m update

git push origin master


reference
~~~~~~~~~

https://gitlab.com/help/user/project/pages/index.md

https://gohugo.io/hosting-and-deployment/hosting-on-gitlab/

https://www.networknt.com/tool/hugo-docs/


