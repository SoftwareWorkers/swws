phpList is not present today (june 2019 - version 3.4.2) in [NixOS](https://nixos.org/nixos/packages.html#phplist) nor in [Debian](https://packages.debian.org/search?keywords=phplist&searchon=names&suite=all&section=all).
There is a [docker](https://hub.docker.com/u/phplist).

We install with https://www.phplist.org/manual/ch028_installation.xhtml  
on Debian Stretch (minimal)  


```
apt install mysql-server apache2 libapache2-mod-php php php-mysql php-xml php-imap   
 
mysql_secure_installation  
enter  
y  
secret  
secret  
y  
y  
y  
y  
```

`mysql -u root -p`
```
CREATE DATABASE phplistdb;  
CREATE USER phplist IDENTIFIED BY 'secret';  
GRANT USAGE ON *.* TO 'phplist'@localhost IDENTIFIED BY 'secret';
GRANT ALL privileges ON phplistdb.* TO 'phplist'@localhost;
FLUSH PRIVILEGES;
\q
```

# Download and unzip version 3.4.2 from sourceforge  
```
wget https://sourceforge.net/projects/phplist/files/phplist/3.4.2/phplist-3.4.2.tgz/download  
tar -xzvf download  
```

# copy the /lists folder to /var/www/html  
```
cp -R phplist-3.4.2/public_html/lists /var/www/html  
cp phplist-3.4.2/public_html/index.html /var/www/html/
```  

# config /var/www/html/lists/config/config.php  
`vi /var/www/html/lists/config/config.php`  

```
$databse_host = localhost  
$databse_name = phplistdb  
$databse_user = phplist  
$databse_password = secret  
```

```
mv /var/www/html/lists/index.html /var/www/html/lists/index.htmlOLD  
mkdir /var/www/html/images  
systemctl restart apache2  
systemctl restart mysql  
```

Open your web browser and go to `http://mywebsite.sal.local/lists/admin`  
click on `initialize database`  and fill the information required  

wait 5 minuts  
click on `do not subscribe`  
click on `continue with setup`  
login with `admin`  
click on `verify settings`  

in  `/var/www/html/lists/config/config.php`  
change `"Test",1`  to  `"Test",0`  

on `http://mywebsite.sal.local/lists/admin/?page=configure` modify  
*  Website address  
*  Domain Name of your server (for email)  


to subscribe to the newsletter go to `http://mywebsite.sal.local/lists/`


Reference:  
https://www.phplist.org/manual/ch028_installation.xhtml  
http://www.daniloaz.com/en/how-to-create-a-user-in-mysql-mariadb-and-grant-permissions-on-a-specific-database/  
https://resources.phplist.com/  
https://resources.phplist.com/system/start  



______________________________________________________
# Postgresql version  (not finished)

`apt install postgresql apache2 php-pgsql`  

Download and unzip version 3.4.2 from https://sourceforge.net/projects/phplist/files/phplist/3.4.2/phplist-3.4.2.tgz/download  
copy the /lists folder to /var/www/html  


`su - postgres`  
`psql`

postgres=# `CREATE USER phplist WITH PASSWORD 'secret';`  
to change the password `ALTER ROLE phplist WITH PASSWORD 'secret';`

postgres=# `CREATE DATABASE phplistdb WITH OWNER phplist;`

exit `\q`

`vi /etc/postgresql/9.6/main/pg_hba.conf`  
add this as first line  
`local phplistdb phplist password`  

`systemctl restart postgresql`  
`systemctl restart apache2`


config /var/www/html/lists/config/config.php  
```
$databse_host = localhost  
$databse_name = phplistdb  
$databse_user = phplist  
$databse_password = secret  
```
______________________________________________________

