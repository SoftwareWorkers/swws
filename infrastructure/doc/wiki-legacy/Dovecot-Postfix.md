DRAFT-DRAFT-DRAFT-DRAFT-DRAFT-DRAFT-DRAFT-DRAFT-DRAFT-DRAFT-DRAFT  

Install Dovecot-imapd, Dovecot-core e Postfix on Debian9 with apt  

modify /etc/hosts
insert  
127.0.1.1 imap.sw.local  imap  
127.0.1.1 smtp.sw.local  smtp  

modify /etc/dovecot/conf.d/10-auth.conf  
#disable_plaintext_auth = yes --> no and uncomment  
#auth_ssl_require_client_cert = yes --> no and uncomment  

in Postfix configuration popup answer locale, sw.local  

configure Evolution
143 IMAP nessuna cifratura, password

25 STARTTLS il server richiede autenticazione = si
