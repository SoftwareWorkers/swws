# Specifications for desktop pc to be used in SW

The following are ideal specifications. Actual purchases may differ due to several reason (cost, sourcing, maturity, ability of people to effectively use products).

Some specs are in trade-off relations: for example a small form factor in **ergonomic area** is not compatible with the possibility of expansions in **functionality area**.


## Ergonomic requirements
* for some ergonomics best practices consult https://www.policlinico.mi.it/centri-di-riferimento/35/centro-di-riferimento-per-l-ergonomia-della-postura-e-del-movimento-e-per-le-allergologie-ambientali-e-occupazionali[Scientific research in posture] (in Italian)
* low noise emission (see https://silent.se/pc/[The Silent PC] for further details)
* small form factor (https://en.wikipedia.org/wiki/Small_form_factor[Wikipedia article])
* mechanical keyboard (https://www.daskeyboard.com/daskeyboard-4-ultimate/[just for badass typists])
* no mouse (https://i3wm.org/[i3 makes them useless]): strongly deprecated as it violates SW efficiency standard
* a shared https://en.wikipedia.org/wiki/Graphics_tablet[graphics tablet] available to be installed on demand for graphic tasks
* two monitors
* webcam
* headset with microphone

## Functional requirements
* capability for execute at least two concurrent virtual machines
* case with slots for plug SSD both internal and external


## Technical requirements
* a secure cryptoprocessor (likely a TPM 2.0). More on https://trustedcomputinggroup.org/wp-content/uploads/How_to_Use_TPM_Whitepaper_20090302_Final_3_.pdf[How to Use the TPM] 
* usb 2.0 and 3.0
* Video card with three output interfaces: VGA, DVI and HDMI
* avoid Intel Management Engine for the sake of https://www.eff.org/deeplinks/2017/05/intels-management-engine-security-hazard-and-users-need-way-disable-it[security]
* [in the future] Plenty of PCIe slots for connecting https://www.howtogeek.com/238253/what-is-a-pcie-ssd-and-do-you-need-one/[PCIe SSDs]


