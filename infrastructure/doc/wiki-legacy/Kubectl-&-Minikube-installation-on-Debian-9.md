DRAFT!!!!!

minimum requisites: 2 CPU, 2048MB RAM, 20GB disk

`
su -

apt-get update

apt-get install -y apt-transport-https

apt-get install -y curl

curl -s https://packages.cloud.google.com/apt/doc/apt-key.gpg | apt-key add -

echo "deb https://apt.kubernetes.io/ kubernetes-xenial main" | tee -a /etc/apt/sources.list.d/kubernetes.list

apt-get update

apt-get install -y kubectl

curl -Lo minikube https://storage.googleapis.com/minikube/releases/latest/minikube-linux-amd64

chmod +x minikube

cp minikube /usr/local/bin

rm minikube


curl -fsSL https://download.docker.com/linux/debian/gpg | apt-key add -

apt-key fingerprint 0EBFCD88

add-apt-repository \
   "deb [arch=amd64] https://download.docker.com/linux/debian \
   $(lsb_release -cs) \
   stable"

apt-get update

apt-get install docker-ce docker-ce-cli containerd.io

docker info

docker --version

docker run hello-world

docker image ls

docker container ls

minikube start --vm-driver=none --cpus=1 --memory=1024


kubectl cluster-info

kubectl cluster-info dump


`

reference:

https://kubernetes.io/docs/tasks/tools/install-kubectl/

https://kubernetes.io/docs/tasks/tools/install-minikube/

https://linuxhint.com/install-minikube-ubuntu/

https://docs.docker.com/install/linux/docker-ce/debian/

https://docs.docker.com/install/linux/linux-postinstall/

