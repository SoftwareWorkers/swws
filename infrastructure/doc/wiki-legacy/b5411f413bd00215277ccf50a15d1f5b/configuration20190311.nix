# Edit this configuration file to define what should be installed on
# your system.  Help is available in the configuration.nix(5) man page
# and in the NixOS manual (accessible by running ‘nixos-help’).

{ config, pkgs, ... }:

{
  imports =
    [ # Include the results of the hardware scan.
      ./hardware-configuration.nix
    ];

  boot.loader.grub.enable = true;
  boot.loader.grub.version = 2;
  boot.loader.grub.device = "/dev/sda"; # or "nodev" for efi only

  networking.hostName = "nixosTEST"; # Define your hostname.

  i18n.consoleFont = "Lat2-Terminus16";
  i18n.consoleKeyMap = "it";
  i18n.defaultLocale = "it_IT.UTF-8";

  time.timeZone = "Europe/Rome";

  services.openssh.enable = true;

  # Open ports in the firewall.
  # networking.firewall.allowedTCPPorts = [ 22 80 443 ];
  # networking.firewall.allowedUDPPorts = [ ... ];
  # Or disable the firewall altogether.
   networking.firewall.enable = false;

  # Define a user account. Don't forget to set a password with ‘passwd’.
   users.users.salvatore = {
     isNormalUser = true;
     uid = 1000;
   };

environment.systemPackages = [ pkgs.php pkgs.postgresql100 pkgs.nginx
pkgs.nextcloud
];

services.postgresql.enable = true;

services.nextcloud.enable = true;
services.nextcloud.hostName = "nixosTEST";
services.nextcloud.home = "/var/www/nextcloud";
services.nextcloud.config.adminpassFile = "/etc/secret";

services.nginx.enable = true;

  system.stateVersion = "18.09";

}

