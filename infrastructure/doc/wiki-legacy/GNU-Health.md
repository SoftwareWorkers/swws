# GNU Health server run on Tryton
you can install with Docker, Tryton Debian Apt Repository or with the bash script.  
Tryton version 5.2 has been released on May 2019 https://discuss.tryton.org/t/tryton-release-5-2/1294  

## Docker installation version 3.0
3 years old instructions:  
https://github.com/mbehrle/docker-gnuhealth-demo

## Tryton Debian Apt Repository installation version 3.8

```
apt install curl

curl -o /etc/apt/trusted.gpg.d/debian.tryton.org-archive.gpg http://debian.tryton.org/debian/debian.tryton.org-archive.gpg

echo "deb http://debian.tryton.org/debian/ stretch-3.8 main" >> /etc/apt/sources.list

curl -o /etc/apt/preferences.d/debian.tryton.org.pref http://debian.tryton.org/debian/debian.tryton.org.pref

apt-get update && apt-get dist-upgrade

apt-get install tryton-server -t stretch-3.8

apt-get install tryton-client -t stretch-3.8

apt-get install tryton-modules-health tryton-modules-health-archives tryton-modules-health-history

import a test DB:  
https://en.wikibooks.org/wiki/GNU_Health/The_Demo_database#Local_Demo_Database  

from GUI launch Tryton client with this credential:  
Database: healthdemo
Username: admin
Password: gnusolidario

```

https://tryton-team.pages.debian.net/gnuhealth.html  
https://tryton-team.pages.debian.net/  
https://blends.debian.org/med/tasks/his#tryton-modules-health  


# bash script installation version 3.4
using Debian Stretch, Postgresql, Gunicorn, Flask, Tryton, ...

```
wget http://ftp.gnu.org/gnu/health/gnuhealth-setup-latest.tar.gz  
./gnuhealth-setup install  

```

## GNU Health client
http://tryton.alioth.debian.org/gnuhealth.html  
https://packages.debian.org/stretch/tryton-client  


## additional modules
History: Reports for patient demographics and medical history.  
Archives: Functionality to track legacy or paper-based patient health records.  

## Reference  
https://en.wikibooks.org/wiki/GNU_Health  
http://health.gnu.org  
https://en.wikibooks.org/wiki/GNU_Health/Installation  
https://tryton-team.pages.debian.net/mirror.html  
https://en.wikipedia.org/wiki/Tryton  

https://www.youtube.com/watch?v=ivGwM9y6YOk  
