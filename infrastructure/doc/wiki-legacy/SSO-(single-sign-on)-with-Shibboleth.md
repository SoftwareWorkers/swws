comandi per configurazione:  
shib-keygen  
shib-metagen

verificare su `/etc/shibboleth/shibboleth2.xml` che siano configurati:

<ApplicationDefaults entityID="https://sp.example.org/shibboleth"

<SSO entityID="https://idp.example.org/idp/shibboleth"

discoveryProtocol="SAMLDS" discoveryURL="[https://ds.example.org/DS/WAYF">](https://ds.example.org/DS/WAYF%22%3E)

uri="http://example.org/federation-metadata.xml"

<Errors supportContact="root@localhost"


LDAP:  
https://wiki.shibboleth.net/confluence/display/IDP30/LDAPAuthnConfiguration  
https://wiki.shibboleth.net/confluence/display/IDP30/PasswordAuthnConfiguration  

Nixos:  
https://nixos.org/nixos/packages.html#shibboleth  
https://nixos.org/nixos/options.html#shibboleth  

Debian:
https://www.shibboleth.net  
https://wiki.shibboleth.net/confluence/display/CONCEPT/Home  

Discourse:  
https://meta.discourse.org/c/dev/sso  
https://meta.discourse.org/t/shibboleth-saml-sso-working-implementation-for-higher-ed/62605  
impostazioni SSO su Discourse: https://forum.softwareworkers.it/admin/site_settings/category/login  

Nextcloud:  
https://help.nextcloud.com/search?q=shibboleth  
https://help.nextcloud.com/t/shibboleth-authentication/47392  


CodiMD:  


Postfix-Dovecot:  


SPID:  
https://github.com/italia/spid-sp-shibboleth/tree/master/metadata  
https://github.com/italia/spid-shibboleth-proxy-docker  


Security:  
https://wiki.shibboleth.net/confluence/display/SP3/SecurityAdvisories  


