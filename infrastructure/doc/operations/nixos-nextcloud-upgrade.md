# Tech notes on Nextcloud upgrades on NixOS

## Automatic database upgrade 

Automatic database upgrade was not triggered when upgrading from ver. 17 to 18 and from v. 18 to 19, I had to start this service:

    nixos-container run <container> -- systemctl start nextcloud-setup.service


## Adding missing database stuff

After every upgrade admins usually have to add some database missing stuff with:

    nixos-container run <container> -- nextcloud-occ db:add-missing-columns
    nixos-container run <container> -- nextcloud-occ db:add-missing-indices

## Add missing primary keys

After upgrade to 20.0.3 adding primary keys on big tables could take some time so they were not added automatically. 

    nixos-container run <container> -- nextcloud-occ db:add-missing-primary-keys
	
## Conversion to bigint for columns

After upgrade to 20.0.3 database are missing a conversion to big int. Due to the fact that changing column types on big tables could take some time they were not changed automatically.  This instance must be in **mainenance mode**.

    nixos-container run <container> -- nextcloud-occ maintenance:mode --on
    nixos-container run <container> -- nextcloud-occ db:convert-filecache-bigint
	nixos-container run <container> -- nextcloud-occ maintenance:mode --off
