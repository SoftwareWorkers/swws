# Migrate CodiMD data to another instance

TODO: translate to english and refactor as needed

## Migrazione database e files

- backup database sorgente
- creazione database destinazione
- FIXME se l'owner cambia, modificare l'owner nelle clausole ALTER ... OWNER 
- ripristino dati in database destinazione

- copiare i file presenti nel container Docker sorgente, cartella /codimd/public/uploads, nella corrispondente cartella sul container destinazione

## Change owner

- dalla tabella "Users" ricavare l'elenco utenti

```
                 id                  |                                    profile                                                           |      
           email                 
--------------------------------------+------------------------------------------------------------------------------------------------------+------
---------------------------------
4dc9083b-7b9b-4b0e-97b7-e574edd12aff |                                                                                                      | salvatore.risuglia@softwareworkers.it
6287aa82-4785-4362-8ab7-2a8f98abedaf |                                                                                                      | giovanni.biscuolo@softwareworkers.it
457e9fbe-2b39-4f7f-9ec6-ac31196c5d9c |                                                                                                      | me@atrent.it
1e93d4ed-0baa-48a2-abd0-067f3eccc701 |                                                                                                      | andrea.rossi@softwareworkers.it
36c74da9-797a-407a-8549-12fc486acee9 | ":"SAML-gbiscuolo","username":"Giovanni BISCUOLO","emails":["giovanni.biscuolo@softwareworkers.it"]} | 
9b459ece-e2b7-4491-838a-4d12b6be645f | {:"SAML-arossi","username":"Andrea ROSSI","emails":["andrea.rossi@softwareworkers.it"]}              |
```

- modificare ownership

Esempio chown:

``` SQL
UPDATE "Authors" SET "userId" = '36c74da9-797a-407a-8549-12fc486acee9' WHERE "userId" = '4dc9083b-7b9b-4b0e-97b7-e574edd12aff';
UPDATE "Notes" SET "ownerId" = '36c74da9-797a-407a-8549-12fc486acee9' WHERE "ownerId" = '4dc9083b-7b9b-4b0e-97b7-e574edd12aff';
```

- ricavare gli URL per recuperare il pad via web

```
SELECT title,shortid FROM "Notes" WHERE "ownerId" = '9b459ece-e2b7-4491-838a-4d12b6be645f';
```
