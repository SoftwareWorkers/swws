# Software Workers infrastructure

**WARNING**: this is still a work in progress as we are factoring the entire infrastructure configuration management.

This repository stores all the infrastructure documentation and configuration of Software Workers IT services, both internal that for customers of the *MeUp!* hosting service.

## Secrets management

Secrets stored in this repository are managed with the [git-crypt](https://www.agwa.name/projects/git-crypt/) tool, that extends `git` with encryption features.

Specify files to encrypt by creating a [.gitattributes](./.gitattributes) file in the repository, like this:

```
secretfile filter=git-crypt diff=git-crypt
*.key filter=git-crypt diff=git-crypt
secretdir/** filter=git-crypt diff=git-crypt
```

Like a .gitignore file, it can match wildcards and should be checked into the repository.

**WARNING**: make sure you don't accidentally encrypt the `.gitattributes` file itself or other git files like `.gitignore` or `.gitmodules`. 

Make sure your `.gitattributes` rules are in place **before you add sensitive files**, or those files won't be encrypted!

See `man git-crypt` for details on how to list or add GPG keys allowed to access secrets in this repository. Authorized GPG keys are stored in `.git-crypt/keys/default/0/` (**do not** manually add/remove keys).

## Configuration of `legacy` systems

With `legacy` systems we intend hosts configured via [Filesystem Hierarchy Standard](https://en.wikipedia.org/wiki/Filesystem_Hierarchy_Standard) classic `/etc` directory, contrasted to the declarative configuration systems of NixOS and Guix System or to the configuration of Docker containers via `docker-compose` configuration file.

All `legacy` systems `/etc` directory **must** be managed with the `etckeeper` tool and added as `submodules` in this repository.  This is an example of git submodule for `node201` (remote must be accessed as user `root`):

```
[submodule "hosts/node201.meup.it"]
	path = hosts/node201.meup.it
	url = ssh://root.softwareworkers.node201:/etc
```

TODO: document standard host names to be defined in local `.ssh/config`, e.g. `root.softwareworkers.node201`
