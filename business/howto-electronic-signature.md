# How I signed a document using a smartcard and Libreoffice

This document describes a practical method to setup and use free foftware to be able to sign, with a X.509 certificate provided on a medium (tipically a smartcard), using one of the European officially recognized [advanced electronic signature](https://www.bit4id.com/it/) standards: [XAdES](https://en.wikipedia.org/wiki/XAdES) for XML, [PAsES](https://en.wikipedia.org/wiki/PAdES) for PDF or [CAdES](https://en.wikipedia.org/wiki/CAdES_(computing)).

This is my personal experience with an italian provided smartcard and my smartcard reader, anyway this should be valid with other hardware, see below for details.

## Software prerequisites

1. opensc (for smartcard or token management)

2. Mozilla Firefox (for NSS management)

3. Libreoffice (to sign documents)

FIXME

## The certificate medium

Vendors sells different kind of medium for the certificates and some (many?) of them requires the installation of non-free software: if you buy a smartcard you are free to choose one of the supported reader, if you buy an USB token you have to be **very lucky** to find it is  compatible with the available free software stack.

I was able to get a smartcard, namely `Carta Nazionale dei Servizi` provided by a local chapter of `Camera di Commercio`,  containing a private X.509 certificate that can be used to sign documents.  That certificate is issued by [Infocert](https://www.firma.infocert.it/), one of the italian officially recognised certification authorities.

### The smartcard reader (or USB token)

The CCID project provides a list of [supported readers or tokens](https://ccid.apdu.fr/ccid/supported.html) you can choose from.

I'm using a [Dell smart card reader keyboard](https://ccid.apdu.fr/ccid/supported.html#0x413C0x2101): I had to **disable the pinpad** to be able to use it with all the software I tested.

#### Disable the smartcard pinpad reader

If you have issues with the PIN input phase, try to add the following code in the `app default` section of your `/etc/opensc/opensc.conf` (on Debian, on other distributions may be a different path):

```
  reader_driver pcsc {
    enable_pinpad = false;
  }
```

## Certificate management

### Import root CA certificates

FIXME

1. download https://www.firma.infocert.it/installazione/#certificati

2. import dowloaded certs via Mozilla Firefox (Preferences | Security...)

### Enable your certificate 

1. 

FIXME

## Use smartcard with Chromium

Add the PKCS#11 module from OpenSC to the Chromium NSS module. **Quit Chromium** if necessary and run:

```
$ modutil -dbdir sql:~/.pki/nssdb/ -add "opensc" -libfile  /usr/lib/pkcs11/opensc-pkcs11.so
```

## Sign documents with Libreoffice

### Configuration

Select the NSS database: use Mozilla Firefox profile (usually `default`) you used to import and enable your certificate.

FIXME

