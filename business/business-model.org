#+LANGUAGE: en
#+TITLE: Software Workers - Business Model
#+DATE: 2022-05-01
#+SUBTITLE: Our business model based on Cost Driven analisys and Business Model Canvas 
#+DESCRIPTION: Our business model based on Cost Driven analisys and Business Model Canvas
#+KEYWORDS: business model marketing
#+AUTHOR: Andrea Rossi, Giovanni Biscuolo
#+EMAIL: g@xelera.eu
#+LATEX_CLASS_OPTIONS: [a4paper]

#+NAME: abstract
#+BEGIN_ABSTRACT
Software Workers, the reproducible open open-source company.
#+END_ABSTRACT

* Business analisys 

** TODO Chi siamo

FIXME: Non mettere questa sezione sta male ma non abbiamo grandi contenuti: peggio la toppa del buco?

*** Tratti distintivi

- Openness

- Esperienza (ma non curriculum impressionante)

** Cosa facciamo

*** Tratti distintivi

- riproducibilità (quindi trasperenza)
  - riproducibilità organizzativa
    - business model
    - struttura
    - servizi per il clienti
    - processi (servizi interni)
  - riproducibilità infrastrutturale
    - data centre
    - hardware
    - software
- sovranità (?) del cliente
  - no customer profiling
  - no lock-in
  - elementi di "local first" ("cloud second")
    - keepassx salvato in cloud
    - email salvata in locale
    - sync dei file creati in locale, non il contrario
- minimalismo (sia sulle interfacce che nello stack software)
  - riduzione superficie attacco
  - riduzione del costi di gestione
- no bullshit terms of use
- trasparenza dei costi
  - struttura offerta chiara
  - efficace mappatura tra bisogni e offerta
  - offerta modello che sia di riferimento per comparare costi di altre offerte

*** TODO Glossario

Sviluppare un glossario che definisca i termini "tecnici" che usiamo nelle nostre comunicazioni.

*** TODO FAQ

Sviluppare una FAQ per fornire esempi (query by example) per gli "elementi" dei tratti distintivi per i quali ne vale la pena, come ad esempio casi d'uso dell'approccio "local first".

** Core competencies

(derivate dal software libero)

Tagline: Sappiamo mettere online le applicazioni in modo sicuro e tenerle aggiornate

- istanziare infrastrutture IT sicure economiche e in tempi rapidi: infrastructure as code, no lock-in, no costs per data volume or user volume

- mantenere aggiornate e sicure le infrastrutture IT

- controllo supply-chain del software

- capacità di compartecipazione alla customer value creation
1. settori industriali: meccanica, medicale
2. settore servizi: FIXME

- fornitura infrastrutture su public o private cloud

** Rischi non amministrati

Tagline: non governare il proprio sistema informativo inteso come non averlo adeguato e non saperlo gestire.

- perdita accidentale della continuità di servizio dell'infrastruttura IT: guasto hardware, aggiornamento non riuscrito

- perdita accidentale dei dati: errore umano, guasto hardware parziale (disco dati)

- attacco informatico 

1. interruzione temporanea del servizio
2. perdita di dati (es. ransomware)

- accesso non autorizzato a documenti e dati sensibili o segreti industriali

- carenza di riservatezza dei dati: privacy e segreto industriale

- perdita del know-how aziendale (es. per turn-over)

- vulnerabilità del software (mancati aggiornamenti)

- rimanere indietro tecnologicamente, essere "inadeguato"

* Business model

We use a customized [[https://en.wikipedia.org/wiki/Business_Model_Canvas][Business Model Canvas]] template, giving our business model more structure  and representing the content using mind-map diagrams.

** Business segments.

Business segments are group of services with a common Value Proposition that we offer to a group of Customer Profiles to help them getting their job done with as less pains and as much gains as possible.

*** MeUp!

FIXME: Free Software as a Service

**** Partnerships, activities and resources

[[https://www.strategyzer.com/business-model-canvas/key-partnerships][Key Partnerships]] is the network of most important suppliers and partners that make our business model work.

#+begin_src plantuml :file output/images/BM.MeUp.Partnerships.svg
@startuml

: /' BM.MeUp.Partnerships'/
{{mindmap
title [MeUp!] Partnerships

,* Partners and suppliers

,** Hosting providers
,*** OVH
,*** Hetzner

,** Coworking providers
,*** Regus

left side

,** Academy

,** Software distributions
,*** Guix
,*** Nix
,*** Debian

}}
;

@enduml
#+end_src

[[https://www.strategyzer.com/business-model-canvas/key-activities][Key Activities]] are the most important things we must do to make our business model work.

#+begin_src plantuml :file output/images/BM.MeUp.Activities.svg
@startuml

: /' BM.MeUp.Activities'/
{{mindmap
title [MeUp!] Activities

,* Activities

,** Infrastructure
,*** Setup
,*** Operations
,*** Data security
,*** Access security

,** Communication
}}
;

@enduml
#+end_src

[[https://www.strategyzer.com/business-model-canvas/key-resources][Key Resources]] are the most important assets required to make our business model work.

#+begin_src plantuml :file output/images/BM.MeUp.Resources.svg
@startuml

: /' BM.MeUp.Resources'/
{{mindmap
title [MeUp!] Resources

,* Resources

,** Hosting infrastructure (leased)

,** Coworking space (leased)

,** Personal working space
,*** Internet connection
,*** Personal computer (approved OS?)

}}
;

@enduml
#+end_src

**** Customer segments

# "Customer segment" è la classe di cui le "Personas" sono istanze
# see also: https://www.interaction-design.org/literature/topics/personas

[[https://www.strategyzer.com/business-model-canvas/customer-segments][Customer Segments]] are the different groups of people or organizations we aim to reach and serve. This includes persons who might not generate revenues, but which are necessary for the business model to work.

For each Customer Segment we define a [[https://www.strategyzer.com/business-model-canvas/value-propositions][Value Proposition]], that is the bundle of our services that create value for a specific Customer Segment. Value Propositions consists of two elements:

- **Customer Profile** defining [[https://strategyzer.uservoice.com/knowledgebase/articles/1194400-how-do-i-add-customer-jobs-to-my-value-proposition][Customer Jobs]], [[https://strategyzer.uservoice.com/knowledgebase/articles/1194394-how-do-i-add-customer-gains-to-my-value-propositio][Gains]] and [[https://strategyzer.uservoice.com/knowledgebase/articles/1194406-how-do-i-add-customer-pains-to-my-value-propositio][Pains]]
- **Value Map** defining our [[https://strategyzer.uservoice.com/knowledgebase/articles/1194409-how-do-i-add-products-services-to-my-value-propo][Services]] helping customers getting their jobs done, [[https://strategyzer.uservoice.com/knowledgebase/articles/1194391-how-do-i-add-gain-creators-to-my-value-proposition][Gain Creators]] defining how our services create customer gains and [[https://strategyzer.uservoice.com/knowledgebase/articles/1194412-how-do-i-add-pain-relievers-to-my-value-propositio][Pain Relievers]] describing how our services alleviate specific customer pains.

For each Customer Segment we also define:

- [[https://www.strategyzer.com/business-model-canvas/customer-relationships][Customer Relationships]] describing the types of relationships our company establishes with specific Customer Segments.
- [[https://www.strategyzer.com/business-model-canvas/channels][Channels]] describing how we communicate with and reach our Customer Segments to deliver our Value Proposition.

***** Wannabe Hacker 

Studenti "nerd" degli ultimi tre anni di scuole superiori ad orientamento tecnico/scientifico, matricole di ingegneria e di informatica.

**Unique selling proposition**: mentorship

#+begin_src plantuml :file output/images/BM.MeUp.WH.ValueProposition.CustomerProfile.svg
@startuml

: /'MeUp.Wannabe Hacker.ValueProposition.Customer Profile'/
{{mindmap
title [MeUp!] Wannabe Hacker - Customer Profile

,* Customer profile

,** Gains
,*** Avere una sandbox per sperimentare
,*** Promuovere la propria identità/sito

,** Pains
,*** Being blacklisted (domain or server IP address)
,*** Being perceived as weird by friends and colleagues
,*** Dipendere dalle scelte tecnologiche dei fornitori
,*** Esporsi a lock-in di varia natura (tecnica, commerciale, relazionale)
,*** Loosing compatibility and interoperability with mainstream ecosystem
,*** Mancanza di controllo del processo (integrazione degli strumenti)
,*** Providers ceasing operations
,*** Spendere troppo

left side

,** Jobs
,*** Acquistare i servizi necessari \n//supp. j. - buyer//
,*** Adattare i servizi acquistati alle nuove esigenze \n//supp. j. - co-creator//
,*** Archiviare file //functional j.//
,*** Avere un dominio proprio //social j.//
,*** Being part of hackers community //emotional j.//
,*** Comunicare via email //functional j.//
,*** Differenziarsi //social j.//
,*** Disdire i servizi non più necessari //supp. j. - transferrer//
,*** Essere percepito come competente //social j.//
,*** Gestire gli impegni //functional j.//
,*** Gestire i contatti //functional j.//
,*** Imparare a gestire infrastruttura sistemistica //supp. j. - co-creator//
,*** Regain control (ownership?) on their computing agency //emotional j.//
,*** Ribellarsi a Big Tech //emotional j.//
,*** Rinnovare i servizi acquistati //support - buyer//
,*** Sincronizzare i dati su vari dispositivi //functional j.//

}}
;

@enduml
#+end_src

#+begin_src plantuml :file output/images/BM.MeUp.WH.ValueProposition.ValueMep.svg
@startuml

: /'MeUp.Wannabe Hacker.ValueProposition.Value Map'/
{{mindmap
title [MeUp!] Wannabe Hacker - Value Map

,* Value Map

,** Gain Creator
,*** Accesso a consulenza e formazione
,*** Possibilità di riprodurre in self-hosting le applicazioni
,*** Tutela della riservatezza dei dati

,** Pain Relievers
,*** Continuità del servizio (SLA)
,*** Sicurezza dei dati (backup)
,*** Tutoring applicativo
,*** Utilizzo di standard aperti

left side

,** Services
,*** Cloud storage for offsite backup
,*** Contact and Calendar Management\n//CalDAV and CardDAV//
,*** DNS management
,*** File hosting\n//with sync and sharing functionality//
,*** Mail
,*** Mentorship
,*** Microblogging service

}}
;

@enduml
#+end_src

#+begin_src plantuml :file output/images/BM.MeUp.WH.Customer Relationships.svg
@startuml

: /'MeUp.Wannabe Hacker.Customer Relationships'/
{{mindmap
title [MeUp!] Customer Relatonships

,* Customer Relationships
/'** Transactional'/
,** Long-term
,*** Communities
/'
 ' ** Personal assistance
 ' ** Dedicated personal assistance
 '/
}}
;

@enduml
#+end_src

#+begin_src plantuml :file output/images/BM.MeUp.WH.Channels.svg
@startuml

: /'MeUp.Wannabe Hacker.Channels'/
{{mindmap
title [MeUp!] Channels

,* Channels

,** Owned Direct
,*** Our website

,** Partner Indirect
,*** Skuola.net
}}
;

@enduml
#+end_src

***** TODO Methodic Hacker

FIXME: definition

**Unique selling proposition**: delegation

#+begin_src plantuml :file output/images/BM.MeUp.MH.ValueProposition.CustomerProfile.svg
@startuml

: /'MeUp.Methodic Hacker.ValueProposition.Customer Profile'/
{{mindmap
title [MeUp!] Methodic Hacker - Customer Profile

,* Customer profile

,** Gains

,** Pains

left side

,** Jobs

}}
;

@enduml
#+end_src

#+begin_src plantuml :file output/images/BM.MeUp.MH.ValueProposition.ValueMep.svg
@startuml

: /'MeUp.Methodic Hacker.ValueProposition.Value Map'/
{{mindmap
title [MeUp!] Methodic Hacker - Value Map

,* Value Map

,** Gain Creator

,** Pain Relievers

left side

,** Services

}}
;

@enduml
#+end_src

#+begin_src plantuml :file output/images/BM.MeUp.MH.Customer Relationships.svg
@startuml

: /'MeUp.Methodic Hacker.Customer Relationships'/
{{mindmap
title [MeUp!] Methodic Hacker - Customer Relationships

,* Customer Relationships
}}
;

@enduml
#+end_src

#+begin_src plantuml :file output/images/BM.MeUp.MH.Channels.svg
@startuml

: /'MeUp.Methodic Hacker.Channels'/
{{mindmap
title [MeUp!] Methodic Hacker - Channels

,* Channels
}}
;

@enduml
#+end_src

***** TODO Mainstream Professional

FIXME: definition

**Unique selling proposition**: business continuity.

#+begin_src plantuml :file output/images/BM.MeUp.MP.ValueProposition.CustomerProfile.svg
@startuml

: /'MeUp.Mainstream Professional.ValueProposition.Customer Profile'/
{{mindmap
title [MeUp!] Mainstream Professional - Customer Profile

,* Customer profile

,** Gains

,** Pains

left side

,** Jobs

}}
;

@enduml
#+end_src

#+begin_src plantuml :file output/images/BM.MeUp.MP.ValueProposition.ValueMep.svg
@startuml

: /'MeUp.Mainstream Professional.ValueProposition.Value Map'/
{{mindmap
title [MeUp!] Mainstream Professional - Value Map

,* Value Map

,** Gain Creator

,** Pain Relievers

left side

,** Services

}}
;

@enduml
#+end_src

#+begin_src plantuml :file output/images/BM.MeUp.MP.Customer Relationships.svg
@startuml

: /'MeUp.Mainstream Professional.Customer Relationships'/
{{mindmap
title [MeUp!] Mainstream Professional - Customer Relationships

,* Customer Relationships
}}
;

@enduml
#+end_src

#+begin_src plantuml :file output/images/BM.MeUp.MP.Channels.svg
@startuml

: /'MeUp.Mainstream Professional.Channels'/
{{mindmap
title [MeUp!] Mainstream Professional - Channels

,* Channels
}}
;

@enduml
#+end_src

*** TODO Xelera

Free Software Private Cloud

FIXME: **criticità** [[https://www.xelera.io/][Xelera Software suite (xelera.io)]] è già registrato e usato per lo stesso settore; **ipotesi nome** (tech.meup?)

* TODO Organization

We organize our team in terms of /functional areas/, which is responsible to develop, document and operate the business processes assigned to it (FIXME: documentare quali processi per ogni area funzionale), adopting the principles of Total Quality Management in all aspects of the process management.  Each functional area is responsible to collaborate with all other areas in defining and operating of business processes according to their competences.

Each funcional area is composed grouping people with the same responsabilities, skills and access rights to documents and data needed for the business processes managed by the funcional area.

There are two kinds of functional areas:

- general: common to all Software Workers organization
- per product line: functions whose knowledge and skills are (at least in part) specific to each product line

#+begin_src plantuml :file SW-org_chart.svg
@startwbs

<style>
wbsDiagram {
	RoundCorner 10
	BackgroundColor Silver
	node {
		MinimumWidth 150
		MaximumWidth 150
	}
	.MeUpStyle * {
		BackgroundColor SkyBlue
		RoundCorner 10
	}
}
</style>

,* SW S.r.l.
,** Reception /'coda RT: reception; email: info@softwareworkers.it; alias: hello'/
,** Management /'coda RT: management; email: management@softwareworkers.it'/
,*** Smarketing
,** Accounting /'email: accounting@softwareworkers.it'/
,***> Accounting attivo
,***< Accounting passivo
,*** Bilancio e finanze
,** HR /'(email: hr@softwareworkers.it; alias: jobs, stages)'/
,*** Stages (e.g. PCTO)
,** Legal and Compliance /'email: legal@softwareworkers.it; alias: dpo; refs: https://it.wikipedia.org/wiki/Compliance_normativa'/
,***> Sicurezza lavoro
,***< Data protection (GDPR)
,** Procurement /'aka: Purchases; email: procurement@softwareworkers.it'/
' functional areas for each product lines: meup, xelera (in the future: narratron, edu)
,** MeUp! <<MeUpStyle>>
,***> Sales /'email: sales@<product line> '/
,***< Marketing /'email: marketing@<product line>'/
,***> Technical support /'email: support@<product line>'/
,****> Deployment and decommissioning
,****< Customer support
' disabled: ** Research & Development

@endwbs
#+end_src

** TODO Role based communication workflow

We use an RT queue for each /functional area/, ma associamo indirizzi email "esterni" solo per le **top level** functional areas.  E' compito dei responsabili delle **top level** functional area associare la coda adeguata.

* Version information

This file was generated from {{{input-file}}}, git commit call_git-commit() in call_git-branch() branch.
